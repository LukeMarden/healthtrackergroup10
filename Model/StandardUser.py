import psycopg2, psycopg2.extras
class StandardUser:

    def __init__(self, ID):
        self.ID = ID
        self.DBusername = "postgres"
        self.DBpassword = "123456"
        self.host = "localhost"
        self.database = "HealthTracker"
        connection = psycopg2.connect(user=self.DBusername, password=self.DBpassword, host=self.host, database=self.database)
        manager = connection.cursor()
        query = manager.execute("SELECT id, firstname, surname, email, dob, weight, height, gender "
                                     "FROM users WHERE id = %s", [ID])
        array_row = manager.fetchone()
        self.id = array_row[0]
        self.fname = array_row[1]
        self.sname = array_row[2]
        self.email = array_row[3]
        self.dob = array_row[4]
        self.weight = array_row[5]
        self.height = array_row[6]
        # print(self.height)
        self.gender = array_row[7]



    def generateBMI(self):
        if (self.height is None or self.weight is None):
            return 0
        else:
            heightMetres = self.height/100
            bmi = self.weight / (heightMetres*heightMetres)
            return bmi



