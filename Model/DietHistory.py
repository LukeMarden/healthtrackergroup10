class DietHistory:

    def __init__(self, dietID, dietName, dietDate, dietTime, dietType,  dietWeight, dietVolume, dietPercentage, dietCalories, dietFat, dietCarbs, dietPro):
        self.dietID = dietID
        self.dietName = dietName
        self.dietDate = dietDate
        self.dietTime = dietTime
        self.dietType = dietType
        self.dietWeight = dietWeight
        self.dietVolume = dietVolume
        self.dietPercentage = dietPercentage
        self.dietCalories = dietCalories
        self.dietFat = dietFat
        self.dietCarbs = dietCarbs
        self.dietPro = dietPro

    @staticmethod
    def createEmptyDiet():
        return DietHistory(0, "", "", "", "", 0, 0, 0, 0, 0, 0, 0)

    def toString(self):
        if self.dietType == "Food":
            return "Name: "+self.dietName+"\nType: "+self.dietType+"\nWeight: "+str(self.dietWeight)+"\nCalories: "+str(self.dietCalories)+"\nFat: "+str(self.dietFat)+"\nCarbohydrate: "+str(self.dietCarbs)+"\nProtein: "+str(self.dietPro)
        else:
            return "Name: "+self.dietName+"\nType: "+self.dietType+"\nVolume: "+str(self.dietVolume)+"\nAlcohol Percentage: "+str(self.dietPercentage)+"\nCalories: "+str(self.dietCalories)+"\nFat: "+str(self.dietFat)+"\nCarbohydrate: "+str(self.dietCarbs)+"\nProtein: "+str(self.dietPro)