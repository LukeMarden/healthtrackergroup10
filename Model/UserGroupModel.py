import psycopg2, psycopg2.extras
class UserGroupModel(object):

    def __init__(self, ID):
        self.DBusername = "postgres"
        self.DBpassword = "123456"
        self.host = "localhost"
        self.database = "HealthTracker"
        connection = psycopg2.connect(user=self.DBusername, password=self.DBpassword, host=self.host, database=self.database)
        manager = connection.cursor()
        self.query = manager.execute("SELECT * FROM groups WHERE groupid = %s", [ID])
        array_row = manager.fetchone()
        self.groupID = array_row[0]
        self.groupName = array_row[1]
        self.adminID = array_row[2]
