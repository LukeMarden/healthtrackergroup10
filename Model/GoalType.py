from enum import Enum

class GoalType(Enum):
    WEIGHT = 1
    EXERCISE = 2
    DISTANCE = 3
    SPEED = 4
    CALORIES = 5
    SUGAR = 6
    WATER = 7
    ALCOHOL = 8

    def toString(GoalType):
        switcher = {
            GoalType.WEIGHT: "Target Weight (kg)",
            GoalType.EXERCISE: "Gym Exercise (reps)",
            GoalType.DISTANCE: "Running Distance (miles/km)",
            GoalType.SPEED: "Average Speed (mph / km/h)",
            GoalType.CALORIES: "Calories Consuming (cal)",
            GoalType.SUGAR: "Sugar Intake (gram)",
            GoalType.WATER: "Water Intake (ml)",
            GoalType.ALCOHOL: "Alcohol Intake (ml)"
        }

        return switcher.get(GoalType)
