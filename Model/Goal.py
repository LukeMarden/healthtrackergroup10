from Model.GoalType import GoalType


class Goal(object):

    def __init__(self, id, goalName, setDate, goalType, endDate, goalValue, currentValue, startValue):
        self.goalID = id
        self.goalName = goalName
        self.setDate = setDate
        self.goalType = goalType
        self.endDate = endDate
        self.goalValue = goalValue
        self.currentValue = currentValue
        self.startValue = startValue


    def toString(self):
        return "Name: "+self.goalName+"\nSet Date: "+self.setDate+"\nEnd Date: "+self.endDate+"\nGoal Type: "+ GoalType.toString(self.goalType)+\
               "\nStart Value: "+str(self.startValue)+"\nCurrent Value: "+str(self.currentValue)+"\nGoal Value: "+str(self.goalValue)+"\nProgress: "+str(self.getProgress())

    def getProgress(self):
        progress = 0

        #If start value is higher than goal value
        if self.startValue > self.goalValue:
            if self.currentValue <= self.goalValue:
                progress = 100
            elif self.currentValue >= self.startValue:
                progress = 0
            else:
                progress = ((self.startValue - self.currentValue)/(self.startValue - self.goalValue)) * 100
        # If start value is lower than goal value
        else:
            if self.currentValue >= self.goalValue:
                progress = 100
            elif self.currentValue <= self.startValue:
                progress = 0
            else:
                progress = ((self.currentValue - self.startValue)/(self.goalValue - self.startValue)) * 100

        return progress


    @staticmethod
    def createEmptyGoal():
        return Goal(-1, "", "", GoalType.WEIGHT, "", 0, 0, 0)