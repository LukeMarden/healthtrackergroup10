import sys
from PyQt5.QtWidgets import QApplication, QWidget, QListWidget, QVBoxLayout, QLabel, QPushButton, QListWidgetItem, \
    QHBoxLayout
from PyQt5 import QtWidgets, QtCore, QtGui


class CustomGoalItemWidget(QWidget):
    def __init__(self, parent=None):
        super(CustomGoalItemWidget, self).__init__(parent)

        self.setupUi(parent)

        # label = QLabel("I am a custom widget")
        #
        # button = QPushButton("A useless button")
        #
        # layout = QHBoxLayout()
        # layout.addWidget(label)
        # layout.addWidget(button)
        #
        #
        #
        # self.setLayout(layout)

    def setupUi(self, parent):
        layout = QHBoxLayout()
        self.frame = QtWidgets.QFrame()
        self.frame.setGeometry(QtCore.QRect(30, 40, 251, 121))
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.frame.sizePolicy().hasHeightForWidth())
        self.frame.setSizePolicy(sizePolicy)
        self.frame.setMinimumSize(QtCore.QSize(251, 121))
        self.frame.setStyleSheet("\n"
                                 "QFrame#frame\n"
                                 "{\n"
                                 "border: 1px solid rgb(76, 76, 76);\n"
                                 "    background-color: rgb(255, 255, 255);\n"
                                 "}\n"
                                 "")
        self.frame.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.frame.setFrameShadow(QtWidgets.QFrame.Raised)
        self.frame.setObjectName("frame")
        self.verticalLayout = QtWidgets.QVBoxLayout(self.frame)
        self.verticalLayout.setObjectName("verticalLayout")
        self.GoalNameText = QtWidgets.QLabel(self.frame)
        self.GoalNameText.setMinimumSize(QtCore.QSize(0, 20))
        font = QtGui.QFont()
        font.setFamily("Segoe UI Semibold")
        font.setPointSize(15)
        font.setBold(True)
        font.setItalic(False)
        font.setWeight(75)
        self.GoalNameText.setFont(font)
        self.GoalNameText.setStyleSheet("font: 63 15pt \"Segoe UI Semibold\";\n"
                                        "font-weight:bold;\n"
                                        "background-color: rgb(255, 255, 255);")
        self.GoalNameText.setAlignment(QtCore.Qt.AlignCenter)
        self.GoalNameText.setObjectName("GoalNameText")
        self.verticalLayout.addWidget(self.GoalNameText)
        self.GoalTypeText = QtWidgets.QLabel(self.frame)
        self.GoalTypeText.setStyleSheet("font: 63 10pt \"Segoe UI Semibold\";\n"
                                        "font-weight:bold;\n"
                                        "background-color: rgb(255, 255, 255);")
        self.GoalTypeText.setObjectName("GoalTypeText")
        self.verticalLayout.addWidget(self.GoalTypeText)
        self.EndDateText = QtWidgets.QLabel(self.frame)
        self.EndDateText.setStyleSheet("font: 63 10pt \"Segoe UI Semibold\";\n"
                                       "font-weight:bold;\n"
                                       "background-color: rgb(255, 255, 255);")
        self.EndDateText.setObjectName("EndDateText")
        self.verticalLayout.addWidget(self.EndDateText)
        self.EndDateText_2 = QtWidgets.QLabel(self.frame)
        self.EndDateText_2.setStyleSheet("font: 63 10pt \"Segoe UI Semibold\";\n"
                                         "font-weight:bold;\n"
                                         "background-color: rgb(255, 255, 255);")
        self.EndDateText_2.setObjectName("EndDateText_2")
        self.verticalLayout.addWidget(self.EndDateText_2)
        self.GoalProgressBar = QtWidgets.QProgressBar(self.frame)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.GoalProgressBar.sizePolicy().hasHeightForWidth())
        self.GoalProgressBar.setSizePolicy(sizePolicy)
        self.GoalProgressBar.setMinimumSize(QtCore.QSize(0, 20))
        self.GoalProgressBar.setStyleSheet("\n"
                                           "QProgressBar {\n"
                                           "    border: 2px solid grey;\n"
                                           "    font: 63 10pt \"Segoe UI Semibold\";\n"
                                           "    font-weight:bold;\n"
                                           "    text-align: center;\n"
                                           "background-color: rgb(255, 255, 255);\n"
                                           "}\n"
                                           "\n"
                                           "QProgressBar::chunk {\n"
                                           "    background-color: rgb(81, 175, 81);\n"
                                           "    width: 20px;\n"
                                           "\n"
                                           "}")
        self.GoalProgressBar.setProperty("value", 2)
        self.GoalProgressBar.setOrientation(QtCore.Qt.Horizontal)
        self.GoalProgressBar.setInvertedAppearance(False)
        self.GoalProgressBar.setObjectName("GoalProgressBar")
        self.verticalLayout.addWidget(self.GoalProgressBar)


        self.retranslateUi(parent)
        layout.addWidget(self.frame)
        self.setLayout(layout)
        QtCore.QMetaObject.connectSlotsByName(parent)

    def retranslateUi(self, parent):
        _translate = QtCore.QCoreApplication.translate
        self.GoalNameText.setText(_translate("Form", "Goal Name"))
        self.GoalTypeText.setText(_translate("Form", "Goal Type"))
        self.EndDateText.setText(_translate("Form", "End Date"))
        self.EndDateText_2.setText(_translate("Form", "Progress:"))




if __name__ == '__main__':
    app = QApplication(sys.argv)
    window = QWidget()

    title = QLabel("Demo for widgets in a QListWidget")

    list = QListWidget()

    item = QListWidgetItem(list)
    item_widget = CustomGoalItemWidget()
    item.setSizeHint(item_widget.sizeHint())
    list.addItem(item)
    list.setItemWidget(item, item_widget)

    list.addItem("string displayed as string")

    item2 = QListWidgetItem(list)
    item_widget2 = CustomGoalItemWidget()
    item2.setSizeHint(item_widget2.sizeHint())
    list.addItem(item2)
    list.setItemWidget(item2, item_widget2)

    window_layout = QVBoxLayout(window)
    window_layout.addWidget(title)
    window_layout.addWidget(list)
    window.setLayout(window_layout)

    window.show()

    sys.exit(app.exec_())