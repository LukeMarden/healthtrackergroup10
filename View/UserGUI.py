import os.path

import PyQt5
from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtCore import QDateTime
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *
from PyQt5.QtChart import *
import pyqtgraph as pg

import psycopg2

# from Controller.UserController import UserController
from Model.Goal import Goal
from Model.GoalType import GoalType
from datetime import datetime, date, timedelta

class UserGUI(object):

    def launchExtendedSignUp(self, ID):
        from Controller.EditExtendedInfoController import EditExtendedInfoController
        auth = EditExtendedInfoController(self.id)
        if auth.exec_():
            self.hide()
            from Controller.UserController import UserController
            auth = UserController(ID)
            auth.exec_()


    def launchGoals(self):
        from Controller.GoalsController import GoalsController
        auth = GoalsController(str(self.id), False, False)
        from Controller.UserController import UserController
        auth = UserController(str(self.id))
        self.hide()
        auth.exec_()

    def launchUserGroups(self):
        from Controller.UserGroupController import UserGroupController
        auth = UserGroupController(self.id)
        auth.exec_()

    def launchDiet(self):
        from Controller.DietHistoryController import DietHistoryController
        auth = DietHistoryController(str(self.id), False)
        from Controller.UserController import UserController
        auth = UserController(str(self.id))
        self.hide()
        auth.exec_()

    def launchActivities(self):
        from Controller.ActivityController import ActivityController
        ui = ActivityController(self.id, False)
        from Controller.UserController import UserController
        auth = UserController(str(self.id))
        self.hide()
        auth.exec_()
        # dialog = QtWidgets.QDialog()
        # ui.setupUi(dialog, self)
        # dialog.exec_()

    def setupUi(self, Dialog):
        Dialog.setObjectName("Dialog")
        Dialog.resize(1350, 590)
        Dialog.setMinimumSize(QtCore.QSize(450, 590))
        Dialog.setMaximumSize(QtCore.QSize(1350, 590))
        Dialog.setStyleSheet("background-color: rgb(255, 255, 255);\n"
                             "\n"
                             "QMessageBox\n"
                             "{\n"
                             "    QLabel\n"
                             "{\n"
                             "    color: rgb(255, 255, 255);\n"
                             "}\n"
                             "\n"
                             "QDialog\n"
                             "{\n"
                             "    background-color: rgb(255, 255, 255);\n"
                             "    color: rgb(255, 255, 255);\n"
                             "font: 75 10pt \"Segoe UI Semibold\";\n"
                             "font-weight:bold;\n"
                             "}\n"
                             "QPushButton {\n"
                             "    background-color: rgb(81, 175, 81);\n"
                             "    padding: 1px;\n"
                             "    border: 4px solid rgb(81, 175, 81);\n"
                             "    border-radius: 1px;\n"
                             "    color: rgb(255, 255, 255);\n"
                             "font: 63 8pt \"Segoe UI Semibold\";\n"
                             "}\n"
                             "\n"
                             "\n"
                             "\n"
                             "QPushButton:Pressed{\n"
                             "    background-color: rgb(55, 140, 55);\n"
                             "    border-color: rgb(55, 140, 55);\n"
                             "}\n"
                             "\n"
                             "}")
        self.gridLayout = QtWidgets.QGridLayout(Dialog)
        self.gridLayout.setContentsMargins(0, 0, 0, 0)
        self.gridLayout.setObjectName("gridLayout")
        self.h_frame = QtWidgets.QFrame(Dialog)
        self.h_frame.setFrameShape(QtWidgets.QFrame.NoFrame)
        self.h_frame.setFrameShadow(QtWidgets.QFrame.Plain)
        self.h_frame.setLineWidth(0)
        self.h_frame.setObjectName("h_frame")
        self.horizontalLayout_10 = QtWidgets.QHBoxLayout(self.h_frame)
        self.horizontalLayout_10.setContentsMargins(0, 0, 0, 0)
        self.horizontalLayout_10.setSpacing(0)
        self.horizontalLayout_10.setObjectName("horizontalLayout_10")
        self.graphsFrame_2 = QtWidgets.QFrame(self.h_frame)
        self.graphsFrame_2.setMinimumSize(QtCore.QSize(450, 50))
        self.graphsFrame_2.setStyleSheet("background-color: rgb(76, 76, 76);")
        self.graphsFrame_2.setFrameShape(QtWidgets.QFrame.NoFrame)
        self.graphsFrame_2.setFrameShadow(QtWidgets.QFrame.Plain)
        self.graphsFrame_2.setObjectName("graphsFrame_2")
        self.verticalLayout_8 = QtWidgets.QVBoxLayout(self.graphsFrame_2)
        self.verticalLayout_8.setContentsMargins(5, 5, 5, 0)
        self.verticalLayout_8.setSpacing(5)
        self.verticalLayout_8.setObjectName("verticalLayout_8")
        self.GraphContainer_3 = QtWidgets.QWidget(self.graphsFrame_2)
        self.GraphContainer_3.setStyleSheet("")
        self.GraphContainer_3.setObjectName("GraphContainer_3")
        self.gridLayout_5 = QtWidgets.QGridLayout(self.GraphContainer_3)
        self.gridLayout_5.setContentsMargins(0, 0, 0, 0)
        self.gridLayout_5.setSpacing(0)
        self.gridLayout_5.setObjectName("gridLayout_5")
        self.verticalLayout_8.addWidget(self.GraphContainer_3)
        self.GraphContainer_4 = QtWidgets.QWidget(self.graphsFrame_2)
        self.GraphContainer_4.setStyleSheet("")
        self.GraphContainer_4.setObjectName("GraphContainer_4")
        self.gridLayout_6 = QtWidgets.QGridLayout(self.GraphContainer_4)
        self.gridLayout_6.setContentsMargins(0, 0, 0, 0)
        self.gridLayout_6.setSpacing(0)
        self.gridLayout_6.setObjectName("gridLayout_6")
        self.verticalLayout_8.addWidget(self.GraphContainer_4)
        self.verticalLayout_8.setStretch(0, 1)
        self.verticalLayout_8.setStretch(1, 1)
        self.horizontalLayout_10.addWidget(self.graphsFrame_2)
        self.frame = QtWidgets.QFrame(self.h_frame)
        self.frame.setLineWidth(1)
        self.frame.setObjectName("frame")
        self.verticalLayout_4 = QtWidgets.QVBoxLayout(self.frame)
        self.verticalLayout_4.setContentsMargins(0, 0, 0, 0)
        self.verticalLayout_4.setObjectName("verticalLayout_4")
        self.frame_8 = QtWidgets.QFrame(self.frame)
        self.frame_8.setMaximumSize(QtCore.QSize(60000, 55000))
        self.frame_8.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.frame_8.setFrameShadow(QtWidgets.QFrame.Raised)
        self.frame_8.setObjectName("frame_8")
        self.verticalLayout_6 = QtWidgets.QVBoxLayout(self.frame_8)
        self.verticalLayout_6.setContentsMargins(15, -1, 15, 9)
        self.verticalLayout_6.setSpacing(0)
        self.verticalLayout_6.setObjectName("verticalLayout_6")
        self.frame_9 = QtWidgets.QFrame(self.frame_8)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.frame_9.sizePolicy().hasHeightForWidth())
        self.frame_9.setSizePolicy(sizePolicy)
        self.frame_9.setMinimumSize(QtCore.QSize(410, 250))
        self.frame_9.setMaximumSize(QtCore.QSize(16777215, 16777215))
        self.frame_9.setStyleSheet("QFrame#frame_9\n"
                                   "{\n"
                                   "     border: 1px solid rgb(76, 76, 76);\n"
                                   "\n"
                                   "}")
        self.frame_9.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.frame_9.setFrameShadow(QtWidgets.QFrame.Raised)
        self.frame_9.setObjectName("frame_9")
        self.verticalLayout = QtWidgets.QVBoxLayout(self.frame_9)
        self.verticalLayout.setContentsMargins(0, 0, 0, 0)
        self.verticalLayout.setSpacing(0)
        self.verticalLayout.setObjectName("verticalLayout")
        self.frame_2 = QtWidgets.QFrame(self.frame_9)
        self.frame_2.setMinimumSize(QtCore.QSize(200, 0))
        self.frame_2.setMaximumSize(QtCore.QSize(16777215, 45))
        self.frame_2.setStyleSheet("")
        self.frame_2.setObjectName("frame_2")
        self.horizontalLayout = QtWidgets.QHBoxLayout(self.frame_2)
        self.horizontalLayout.setContentsMargins(-1, 20, -1, 0)
        self.horizontalLayout.setObjectName("horizontalLayout")
        spacerItem = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout.addItem(spacerItem)
        self.label = QtWidgets.QLabel(self.frame_2)
        self.label.setStyleSheet("\n"
                                 "font: 75  20pt \"Segoe UI Semibold\";\n"
                                 "font-weight:bold;")
        self.label.setAlignment(QtCore.Qt.AlignCenter)
        self.label.setObjectName("label")
        self.horizontalLayout.addWidget(self.label)
        self.label_2 = QtWidgets.QLabel(self.frame_2)
        self.label_2.setStyleSheet("\n"
                                   "font: 75  20pt \"Segoe UI Semibold\";\n"
                                   "font-weight:bold;")
        self.label_2.setAlignment(QtCore.Qt.AlignCenter)
        self.label_2.setObjectName("label_2")
        self.horizontalLayout.addWidget(self.label_2)
        spacerItem1 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout.addItem(spacerItem1)
        self.verticalLayout.addWidget(self.frame_2)
        self.frame_3 = QtWidgets.QFrame(self.frame_9)
        self.frame_3.setMinimumSize(QtCore.QSize(0, 0))
        self.frame_3.setMaximumSize(QtCore.QSize(16777215, 30))
        self.frame_3.setStyleSheet("")
        self.frame_3.setObjectName("frame_3")
        self.horizontalLayout_2 = QtWidgets.QHBoxLayout(self.frame_3)
        self.horizontalLayout_2.setContentsMargins(0, 0, 0, 0)
        self.horizontalLayout_2.setSpacing(6)
        self.horizontalLayout_2.setObjectName("horizontalLayout_2")
        spacerItem2 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout_2.addItem(spacerItem2)
        self.label_3 = QtWidgets.QLabel(self.frame_3)
        self.label_3.setStyleSheet("\n"
                                   "font: 75 11pt \"Segoe UI Semibold\";\n"
                                   "font-weight:bold;")
        self.label_3.setAlignment(QtCore.Qt.AlignCenter)
        self.label_3.setObjectName("label_3")
        self.horizontalLayout_2.addWidget(self.label_3)
        self.label_4 = QtWidgets.QLabel(self.frame_3)
        self.label_4.setStyleSheet("\n"
                                   "font: 75 11pt \"Segoe UI Semibold\";\n"
                                   "font-weight:bold;")
        self.label_4.setAlignment(QtCore.Qt.AlignCenter)
        self.label_4.setObjectName("label_4")
        self.horizontalLayout_2.addWidget(self.label_4)
        spacerItem3 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout_2.addItem(spacerItem3)
        self.verticalLayout.addWidget(self.frame_3)
        spacerItem4 = QtWidgets.QSpacerItem(20, 10, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Maximum)
        self.verticalLayout.addItem(spacerItem4)
        self.frame_14 = QtWidgets.QFrame(self.frame_9)
        self.frame_14.setMinimumSize(QtCore.QSize(0, 50))
        self.frame_14.setStyleSheet("")
        self.frame_14.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.frame_14.setFrameShadow(QtWidgets.QFrame.Raised)
        self.frame_14.setObjectName("frame_14")
        self.gridLayout_2 = QtWidgets.QGridLayout(self.frame_14)
        self.gridLayout_2.setContentsMargins(0, 0, 0, 9)
        self.gridLayout_2.setSpacing(0)
        self.gridLayout_2.setObjectName("gridLayout_2")
        self.label_15 = QtWidgets.QLabel(self.frame_14)
        self.label_15.setStyleSheet("\n"
                                    "font: 75 8pt \"Segoe UI Semibold\";\n"
                                    "font-weight:bold;")
        self.label_15.setTextFormat(QtCore.Qt.AutoText)
        self.label_15.setScaledContents(False)
        self.label_15.setAlignment(QtCore.Qt.AlignCenter)
        self.label_15.setWordWrap(False)
        self.label_15.setObjectName("label_15")
        self.gridLayout_2.addWidget(self.label_15, 0, 0, 1, 1)
        self.verticalLayout.addWidget(self.frame_14)
        self.verticalLayout.setStretch(0, 10)
        self.verticalLayout.setStretch(1, 10)
        self.verticalLayout.setStretch(3, 5)
        self.verticalLayout_6.addWidget(self.frame_9)
        self.frame_18 = QtWidgets.QFrame(self.frame_8)
        self.frame_18.setMinimumSize(QtCore.QSize(0, 50))
        self.frame_18.setFrameShape(QtWidgets.QFrame.NoFrame)
        self.frame_18.setFrameShadow(QtWidgets.QFrame.Plain)
        self.frame_18.setObjectName("frame_18")
        self.horizontalLayout_11 = QtWidgets.QHBoxLayout(self.frame_18)
        self.horizontalLayout_11.setObjectName("horizontalLayout_11")
        self.ShowGraphButton_Left = QtWidgets.QPushButton(self.frame_18)
        self.ShowGraphButton_Left.setMaximumSize(QtCore.QSize(40, 20))
        font = QtGui.QFont()
        font.setFamily("Segoe UI Semibold")
        font.setPointSize(10)
        font.setBold(False)
        font.setItalic(False)
        font.setWeight(7)
        self.ShowGraphButton_Left.setFont(font)
        self.ShowGraphButton_Left.setStyleSheet("QPushButton {\n"
                                                "    background-color: rgb(81, 175, 81);\n"
                                                "    padding-bottom: 1px;\n"
                                                "    border: 4px solid rgb(81, 175, 81);\n"
                                                "    border-radius: 1px;\n"
                                                "    color: rgb(255, 255, 255);\n"
                                                "    font: 63 10pt \"Segoe UI Semibold\";\n"
                                                "}\n"
                                                "\n"
                                                "\n"
                                                "\n"
                                                "QPushButton:Pressed{\n"
                                                "    background-color: rgb(55, 140, 55);\n"
                                                "    border-color: rgb(55, 140, 55);\n"
                                                "}")
        self.ShowGraphButton_Left.setObjectName("ShowGraphButton_Left")
        self.horizontalLayout_11.addWidget(self.ShowGraphButton_Left)
        spacerItem5 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout_11.addItem(spacerItem5)
        self.ShowGraphButton_Right = QtWidgets.QPushButton(self.frame_18)
        self.ShowGraphButton_Right.setMaximumSize(QtCore.QSize(40, 20))
        font = QtGui.QFont()
        font.setFamily("Segoe UI Semibold")
        font.setPointSize(10)
        font.setBold(False)
        font.setItalic(False)
        font.setWeight(7)
        self.ShowGraphButton_Right.setFont(font)
        self.ShowGraphButton_Right.setStyleSheet("QPushButton {\n"
                                                 "    background-color: rgb(81, 175, 81);\n"
                                                 "    padding-bottom: 1px;\n"
                                                 "    border: 4px solid rgb(81, 175, 81);\n"
                                                 "    border-radius: 1px;\n"
                                                 "    color: rgb(255, 255, 255);\n"
                                                 "    font: 63 10pt \"Segoe UI Semibold\";\n"
                                                 "}\n"
                                                 "\n"
                                                 "\n"
                                                 "\n"
                                                 "QPushButton:Pressed{\n"
                                                 "    background-color: rgb(55, 140, 55);\n"
                                                 "    border-color: rgb(55, 140, 55);\n"
                                                 "}")
        self.ShowGraphButton_Right.setObjectName("ShowGraphButton_Right")
        self.horizontalLayout_11.addWidget(self.ShowGraphButton_Right)
        self.verticalLayout_6.addWidget(self.frame_18)
        self.frame_16 = QtWidgets.QFrame(self.frame_8)
        self.frame_16.setMaximumSize(QtCore.QSize(16777215, 140))
        self.frame_16.setStyleSheet("background-color: rgb(76, 76, 76);")
        self.frame_16.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.frame_16.setFrameShadow(QtWidgets.QFrame.Raised)
        self.frame_16.setObjectName("frame_16")
        self.verticalLayout_5 = QtWidgets.QVBoxLayout(self.frame_16)
        self.verticalLayout_5.setContentsMargins(0, 0, 0, 0)
        self.verticalLayout_5.setObjectName("verticalLayout_5")
        self.frame_17 = QtWidgets.QFrame(self.frame_16)
        self.frame_17.setMinimumSize(QtCore.QSize(0, 0))
        self.frame_17.setMaximumSize(QtCore.QSize(16777215, 20))
        self.frame_17.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.frame_17.setFrameShadow(QtWidgets.QFrame.Raised)
        self.frame_17.setObjectName("frame_17")
        self.gridLayout_3 = QtWidgets.QGridLayout(self.frame_17)
        self.gridLayout_3.setContentsMargins(9, 1, 9, 0)
        self.gridLayout_3.setObjectName("gridLayout_3")
        self.label_16 = QtWidgets.QLabel(self.frame_17)
        self.label_16.setMinimumSize(QtCore.QSize(0, 10))
        self.label_16.setStyleSheet("color: rgb(255, 255, 255);\n"
                                    "font: 75 8pt \"Segoe UI Semibold\";\n"
                                    "font-weight:bold;")
        self.label_16.setObjectName("label_16")
        self.gridLayout_3.addWidget(self.label_16, 0, 0, 1, 1)
        self.verticalLayout_5.addWidget(self.frame_17)
        self.frame_10 = QtWidgets.QFrame(self.frame_16)
        self.frame_10.setMinimumSize(QtCore.QSize(0, 115))
        self.frame_10.setStyleSheet("background-color: rgb(221, 221, 221);")
        self.frame_10.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.frame_10.setFrameShadow(QtWidgets.QFrame.Raised)
        self.frame_10.setObjectName("frame_10")
        self.verticalLayout_2 = QtWidgets.QVBoxLayout(self.frame_10)
        self.verticalLayout_2.setContentsMargins(-1, 0, -1, 3)
        self.verticalLayout_2.setSpacing(0)
        self.verticalLayout_2.setObjectName("verticalLayout_2")
        self.frame_4 = QtWidgets.QFrame(self.frame_10)
        self.frame_4.setObjectName("frame_4")
        self.horizontalLayout_3 = QtWidgets.QHBoxLayout(self.frame_4)
        self.horizontalLayout_3.setContentsMargins(1, 1, 1, 1)
        self.horizontalLayout_3.setObjectName("horizontalLayout_3")
        self.label_7 = QtWidgets.QLabel(self.frame_4)
        self.label_7.setMaximumSize(QtCore.QSize(50, 16777215))
        self.label_7.setStyleSheet("font: 63 10pt \"Segoe UI Semibold\";\n"
                                   "")
        self.label_7.setObjectName("label_7")
        self.horizontalLayout_3.addWidget(self.label_7)
        self.label_8 = QtWidgets.QLabel(self.frame_4)
        font = QtGui.QFont()
        font.setFamily("Segoe UI Semibold")
        font.setPointSize(10)
        font.setBold(False)
        font.setItalic(False)
        font.setWeight(7)
        self.label_8.setFont(font)
        self.label_8.setStyleSheet("font: 63 10pt \"Segoe UI Semibold\";\n"
                                   "")
        self.label_8.setObjectName("label_8")
        self.horizontalLayout_3.addWidget(self.label_8)
        self.verticalLayout_2.addWidget(self.frame_4)
        self.frame_5 = QtWidgets.QFrame(self.frame_10)
        self.frame_5.setObjectName("frame_5")
        self.horizontalLayout_7 = QtWidgets.QHBoxLayout(self.frame_5)
        self.horizontalLayout_7.setContentsMargins(1, 1, 1, 1)
        self.horizontalLayout_7.setObjectName("horizontalLayout_7")
        self.label_13 = QtWidgets.QLabel(self.frame_5)
        self.label_13.setMaximumSize(QtCore.QSize(50, 16777215))
        self.label_13.setStyleSheet("font: 63 10pt \"Segoe UI Semibold\";\n"
                                    "")
        self.label_13.setObjectName("label_13")
        self.horizontalLayout_7.addWidget(self.label_13)
        self.label_14 = QtWidgets.QLabel(self.frame_5)
        self.label_14.setStyleSheet("font: 63 10pt \"Segoe UI Semibold\";\n"
                                    "")
        self.label_14.setObjectName("label_14")
        self.horizontalLayout_7.addWidget(self.label_14)
        self.verticalLayout_2.addWidget(self.frame_5)
        self.frame_6 = QtWidgets.QFrame(self.frame_10)
        self.frame_6.setObjectName("frame_6")
        self.horizontalLayout_4 = QtWidgets.QHBoxLayout(self.frame_6)
        self.horizontalLayout_4.setContentsMargins(1, 1, 1, 1)
        self.horizontalLayout_4.setObjectName("horizontalLayout_4")
        self.label_5 = QtWidgets.QLabel(self.frame_6)
        self.label_5.setMaximumSize(QtCore.QSize(50, 16777215))
        self.label_5.setStyleSheet("font: 63 10pt \"Segoe UI Semibold\";\n"
                                   "")
        self.label_5.setObjectName("label_5")
        self.horizontalLayout_4.addWidget(self.label_5)
        self.label_6 = QtWidgets.QLabel(self.frame_6)
        self.label_6.setStyleSheet("font: 63 10pt \"Segoe UI Semibold\";\n"
                                   "")
        self.label_6.setObjectName("label_6")
        self.horizontalLayout_4.addWidget(self.label_6)
        self.verticalLayout_2.addWidget(self.frame_6)
        self.frame_7 = QtWidgets.QFrame(self.frame_10)
        self.frame_7.setObjectName("frame_7")
        self.horizontalLayout_5 = QtWidgets.QHBoxLayout(self.frame_7)
        self.horizontalLayout_5.setContentsMargins(1, 1, 1, 1)
        self.horizontalLayout_5.setObjectName("horizontalLayout_5")
        self.label_9 = QtWidgets.QLabel(self.frame_7)
        self.label_9.setMaximumSize(QtCore.QSize(50, 16777215))
        self.label_9.setStyleSheet("font: 63 10pt \"Segoe UI Semibold\";\n"
                                   "")
        self.label_9.setObjectName("label_9")
        self.horizontalLayout_5.addWidget(self.label_9)
        self.label_10 = QtWidgets.QLabel(self.frame_7)
        self.label_10.setStyleSheet("font: 63 10pt \"Segoe UI Semibold\";\n"
                                    "")
        self.label_10.setObjectName("label_10")
        self.horizontalLayout_5.addWidget(self.label_10)
        self.verticalLayout_2.addWidget(self.frame_7)
        self.frame_15 = QtWidgets.QFrame(self.frame_10)
        self.frame_15.setMinimumSize(QtCore.QSize(0, 0))
        self.frame_15.setCursor(QtGui.QCursor(QtCore.Qt.ArrowCursor))
        self.frame_15.setFrameShape(QtWidgets.QFrame.NoFrame)
        self.frame_15.setFrameShadow(QtWidgets.QFrame.Plain)
        self.frame_15.setObjectName("frame_15")
        self.horizontalLayout_6 = QtWidgets.QHBoxLayout(self.frame_15)
        self.horizontalLayout_6.setContentsMargins(1, 1, 1, 1)
        self.horizontalLayout_6.setObjectName("horizontalLayout_6")
        self.label_12 = QtWidgets.QLabel(self.frame_15)
        self.label_12.setMaximumSize(QtCore.QSize(50, 16777215))
        self.label_12.setStyleSheet("font: 63 10pt \"Segoe UI Semibold\";\n"
                                    "")
        self.label_12.setObjectName("label_12")
        self.horizontalLayout_6.addWidget(self.label_12)
        self.label_11 = QtWidgets.QLabel(self.frame_15)
        self.label_11.setStyleSheet("font: 63 10pt \"Segoe UI Semibold\";\n"
                                    "")
        self.label_11.setObjectName("label_11")
        self.horizontalLayout_6.addWidget(self.label_11)
        self.verticalLayout_2.addWidget(self.frame_15)
        self.verticalLayout_5.addWidget(self.frame_10)
        self.verticalLayout_6.addWidget(self.frame_16)
        self.verticalLayout_6.setStretch(0, 5)
        self.verticalLayout_6.setStretch(1, 1)
        self.verticalLayout_6.setStretch(2, 5)
        self.verticalLayout_4.addWidget(self.frame_8)
        self.frame_13 = QtWidgets.QFrame(self.frame)
        self.frame_13.setStyleSheet("background-color: rgb(76, 76, 76);")
        self.frame_13.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.frame_13.setFrameShadow(QtWidgets.QFrame.Raised)
        self.frame_13.setObjectName("frame_13")
        self.verticalLayout_3 = QtWidgets.QVBoxLayout(self.frame_13)
        self.verticalLayout_3.setObjectName("verticalLayout_3")
        self.frame_11 = QtWidgets.QFrame(self.frame_13)
        self.frame_11.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.frame_11.setFrameShadow(QtWidgets.QFrame.Raised)
        self.frame_11.setObjectName("frame_11")
        self.horizontalLayout_8 = QtWidgets.QHBoxLayout(self.frame_11)
        self.horizontalLayout_8.setSpacing(40)
        self.horizontalLayout_8.setObjectName("horizontalLayout_8")
        self.pushButton = QtWidgets.QPushButton(self.frame_11)
        self.pushButton.setStyleSheet("QPushButton {\n"
                                      "    background-color: rgb(81, 175, 81);\n"
                                      "    padding: 3px;\n"
                                      "    border: 4px solid rgb(81, 175, 81);\n"
                                      "    border-radius: 1px;\n"
                                      "    color: rgb(255, 255, 255);\n"
                                      "font: 63 10pt \"Segoe UI Semibold\";\n"
                                      "}\n"
                                      "\n"
                                      "\n"
                                      "\n"
                                      "QPushButton:Pressed{\n"
                                      "    background-color: rgb(55, 140, 55);\n"
                                      "    border-color: rgb(55, 140, 55);\n"
                                      "}")
        self.pushButton.setObjectName("pushButton")
        self.horizontalLayout_8.addWidget(self.pushButton)
        self.pushButton_2 = QtWidgets.QPushButton(self.frame_11)
        self.pushButton_2.setStyleSheet("QPushButton {\n"
                                        "    background-color: rgb(81, 175, 81);\n"
                                        "    padding: 3px;\n"
                                        "    border: 4px solid rgb(81, 175, 81);\n"
                                        "    border-radius: 1px;\n"
                                        "    color: rgb(255, 255, 255);\n"
                                        "font: 63 10pt \"Segoe UI Semibold\";\n"
                                        "}\n"
                                        "\n"
                                        "\n"
                                        "\n"
                                        "QPushButton:Pressed{\n"
                                        "    background-color: rgb(55, 140, 55);\n"
                                        "    border-color: rgb(55, 140, 55);\n"
                                        "}")
        self.pushButton_2.setObjectName("pushButton_2")
        self.horizontalLayout_8.addWidget(self.pushButton_2)
        self.pushButton_3 = QtWidgets.QPushButton(self.frame_11)
        self.pushButton_3.setStyleSheet("QPushButton {\n"
                                        "    background-color: rgb(81, 175, 81);\n"
                                        "    padding: 3px;\n"
                                        "    border: 4px solid rgb(81, 175, 81);\n"
                                        "    border-radius: 1px;\n"
                                        "    color: rgb(255, 255, 255);\n"
                                        "font: 63 10pt \"Segoe UI Semibold\";\n"
                                        "}\n"
                                        "\n"
                                        "\n"
                                        "\n"
                                        "QPushButton:Pressed{\n"
                                        "    background-color: rgb(55, 140, 55);\n"
                                        "    border-color: rgb(55, 140, 55);\n"
                                        "}")
        self.pushButton_3.setObjectName("pushButton_3")
        self.horizontalLayout_8.addWidget(self.pushButton_3)
        self.verticalLayout_3.addWidget(self.frame_11)
        self.frame_12 = QtWidgets.QFrame(self.frame_13)
        self.frame_12.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.frame_12.setFrameShadow(QtWidgets.QFrame.Raised)
        self.frame_12.setObjectName("frame_12")
        self.horizontalLayout_9 = QtWidgets.QHBoxLayout(self.frame_12)
        self.horizontalLayout_9.setSpacing(80)
        self.horizontalLayout_9.setObjectName("horizontalLayout_9")
        self.pushButton_5 = QtWidgets.QPushButton(self.frame_12)
        self.pushButton_5.setStyleSheet("QPushButton {\n"
                                        "    background-color: rgb(81, 175, 81);\n"
                                        "    padding: 3px;\n"
                                        "    border: 4px solid rgb(81, 175, 81);\n"
                                        "    border-radius: 1px;\n"
                                        "    color: rgb(255, 255, 255);\n"
                                        "font: 63 10pt \"Segoe UI Semibold\";\n"
                                        "}\n"
                                        "\n"
                                        "\n"
                                        "\n"
                                        "QPushButton:Pressed{\n"
                                        "    background-color: rgb(55, 140, 55);\n"
                                        "    border-color: rgb(55, 140, 55);\n"
                                        "}")
        self.pushButton_5.setObjectName("pushButton_5")
        self.horizontalLayout_9.addWidget(self.pushButton_5)
        self.pushButton_4 = QtWidgets.QPushButton(self.frame_12)
        self.pushButton_4.setStyleSheet("QPushButton {\n"
                                        "    background-color: rgb(81, 175, 81);\n"
                                        "    padding: 3px;\n"
                                        "    border: 4px solid rgb(81, 175, 81);\n"
                                        "    border-radius: 1px;\n"
                                        "    color: rgb(255, 255, 255);\n"
                                        "font: 63 10pt \"Segoe UI Semibold\";\n"
                                        "}\n"
                                        "\n"
                                        "\n"
                                        "\n"
                                        "QPushButton:Pressed{\n"
                                        "    background-color: rgb(55, 140, 55);\n"
                                        "    border-color: rgb(55, 140, 55);\n"
                                        "}")
        self.pushButton_4.setObjectName("pushButton_4")
        self.horizontalLayout_9.addWidget(self.pushButton_4)
        self.verticalLayout_3.addWidget(self.frame_12)
        self.verticalLayout_4.addWidget(self.frame_13)
        self.horizontalLayout_10.addWidget(self.frame)
        self.graphsFrame = QtWidgets.QFrame(self.h_frame)
        self.graphsFrame.setEnabled(True)
        self.graphsFrame.setMinimumSize(QtCore.QSize(450, 50))
        self.graphsFrame.setStyleSheet("background-color: rgb(76, 76, 76);")
        self.graphsFrame.setFrameShape(QtWidgets.QFrame.NoFrame)
        self.graphsFrame.setFrameShadow(QtWidgets.QFrame.Plain)
        self.graphsFrame.setObjectName("graphsFrame")
        self.verticalLayout_7 = QtWidgets.QVBoxLayout(self.graphsFrame)
        self.verticalLayout_7.setContentsMargins(5, 5, 5, 5)
        self.verticalLayout_7.setSpacing(5)
        self.verticalLayout_7.setObjectName("verticalLayout_7")
        self.GraphContainer_2 = QtWidgets.QWidget(self.graphsFrame)
        self.GraphContainer_2.setStyleSheet("")
        self.GraphContainer_2.setObjectName("GraphContainer_2")
        self.gridLayout_7 = QtWidgets.QGridLayout(self.GraphContainer_2)
        self.gridLayout_7.setContentsMargins(0, 0, 0, 0)
        self.gridLayout_7.setSpacing(0)
        self.gridLayout_7.setObjectName("gridLayout_7")
        self.verticalLayout_7.addWidget(self.GraphContainer_2)
        self.GraphContainer = QtWidgets.QWidget(self.graphsFrame)
        self.GraphContainer.setStyleSheet("")
        self.GraphContainer.setObjectName("GraphContainer")
        self.gridLayout_8 = QtWidgets.QGridLayout(self.GraphContainer)
        self.gridLayout_8.setContentsMargins(0, 0, 0, 0)
        self.gridLayout_8.setSpacing(0)
        self.gridLayout_8.setObjectName("gridLayout_8")
        self.verticalLayout_7.addWidget(self.GraphContainer)
        self.verticalLayout_7.setStretch(0, 1)
        self.verticalLayout_7.setStretch(1, 1)
        self.horizontalLayout_10.addWidget(self.graphsFrame)
        self.horizontalLayout_10.setStretch(0, 1)
        self.horizontalLayout_10.setStretch(1, 1)
        self.horizontalLayout_10.setStretch(2, 1)
        self.gridLayout.addWidget(self.h_frame, 0, 0, 1, 1)

        self.retranslateUi(Dialog)
        QtCore.QMetaObject.connectSlotsByName(Dialog)


        self.dialogObj = Dialog
        if os.path.exists("config.txt") == False:
            self.updateGraphPanels(True, True, self)
        else:
            f = open("config.txt", "r")
            config = f.read()
            f.close()
            self.updateGraphPanels(config.split()[0] == "True", config.split()[1] == "True", self)

        #Center dialog
        scr = QApplication.desktop().screenGeometry();
        Dialog.move(scr.center() - Dialog.rect().center());

        self.pushButton.clicked.connect(self.launchGoals)
        self.pushButton_2.clicked.connect(self.launchDiet)
        self.pushButton_3.clicked.connect(self.launchActivities)
        self.pushButton_4.clicked.connect(self.launchExtendedSignUp)
        self.pushButton_5.clicked.connect(self.launchUserGroups)
        self.ShowGraphButton_Left.clicked.connect(self.toggleLeftPanel)
        self.ShowGraphButton_Right.clicked.connect(self.toggleRightPanel)
        self.id = Dialog.get_id()
        Dialog.setWindowIcon(QtGui.QIcon('icon.png'))

    def retranslateUi(self, Dialog):
        from Controller.UserController import UserController
        _translate = QtCore.QCoreApplication.translate
        Dialog.setWindowTitle(_translate("Dialog", "Dialog"))
        self.label.setText(_translate("Dialog", "Welcome "))
        self.label_2.setText(_translate("Dialog", "FNAME SURNAME"))
        self.label_3.setText(_translate("Dialog", "Your Score Is:"))
        self.label_4.setText(_translate("Dialog", "000"))
        self.label_15.setText(_translate("Dialog",
                                         "<html><head/><body><p>This is a health tracker produced by Group 10.</p><p>Track your goals, aim to stay healthy.</p><p>Track your diets, see what you have been eating!</p><p>Track your activities, develop a strong body.</p><p>Track your groups, share your score!</p><p><br/></p></body></html>"))
        self.ShowGraphButton_Left.setText(_translate("Dialog", ">>"))
        self.ShowGraphButton_Right.setText(_translate("Dialog", "<<"))
        self.label_16.setText(_translate("Dialog", "User Info:"))
        self.label_7.setText(_translate("Dialog", "Email:"))
        self.label_8.setText(_translate("Dialog", "EMAIL@EMAIL.COM"))
        self.label_13.setText(_translate("Dialog", "Gender:"))
        self.label_14.setText(_translate("Dialog", "XXXXXXX"))
        self.label_5.setText(_translate("Dialog", "DOB:"))
        self.label_6.setText(_translate("Dialog", "00/00/0000"))
        self.label_9.setText(_translate("Dialog", "Weight:"))
        self.label_10.setText(_translate("Dialog", "000kg"))
        self.label_12.setText(_translate("Dialog", "Height:"))
        self.label_11.setText(_translate("Dialog", "000cm"))
        self.pushButton.setText(_translate("Dialog", "Goals"))
        self.pushButton_2.setText(_translate("Dialog", "Diet"))
        self.pushButton_3.setText(_translate("Dialog", "Activities"))
        self.pushButton_5.setText(_translate("Dialog", "Groups"))
        self.pushButton_4.setText(_translate("Dialog", "Edit Details"))



        self.label_2.setText(_translate("Dialog", UserController.get_fname(self) + " " +
                                        UserController.get_sname(self)))
        self.label_8.setText(_translate("Dialog", UserController.get_email(self)))
        self.label_14.setText(_translate("Dialog", str(UserController.get_gender(self))))
        self.label_6.setText(_translate("Dialog", str(UserController.get_dob(self))))
        self.label_10.setText(_translate("Dialog", str(UserController.get_weight(self)) + "kg"))
        self.label_11.setText(_translate("Dialog", str(UserController.get_height(self)) + "cm"))
        bmi = "%.2f" % UserController.get_bmi(self)
        self.label_4.setText(_translate("Dialog", str(bmi)))
        Dialog.setWindowTitle(_translate("Dialog", "Main Page"))


    def toggleLeftPanel(self):
        self.updateGraphPanels(self.graphsFrame_2.isHidden(), not self.graphsFrame.isHidden(), self.dialogObj)

    def toggleRightPanel(self):
        self.updateGraphPanels(not self.graphsFrame_2.isHidden(), self.graphsFrame.isHidden(), self.dialogObj)

    def updateGraphPanels(self, showLeft, showRight, Dialog):
        minWidth = 1350

        _translate = QtCore.QCoreApplication.translate

        if showLeft == False:
            minWidth -= 450
            if not self.graphsFrame_2.isHidden():
                Dialog.move(Dialog.x() + 450, Dialog.y())
                self.graphsFrame_2.hide()
                self.ShowGraphButton_Left.setText(_translate("Dialog", "<<"))
        else:
            if self.graphsFrame_2.isHidden():
                Dialog.move(Dialog.x() - 450, Dialog.y())
                self.graphsFrame_2.show()
                self.ShowGraphButton_Left.setText(_translate("Dialog", ">>"))

        if showRight == False:
            minWidth -= 450
            if not self.graphsFrame.isHidden():
                self.graphsFrame.hide()
                self.ShowGraphButton_Right.setText(_translate("Dialog", ">>"))
        else:
            if self.graphsFrame.isHidden():
                #Dialog.move(Dialog.x() - 450, Dialog.y())
                self.graphsFrame.show()
                self.ShowGraphButton_Right.setText(_translate("Dialog", "<<"))



        Dialog.setMinimumSize(QtCore.QSize(minWidth, 590))
        Dialog.setMaximumSize(QtCore.QSize(minWidth + 50, 590))

        f = open("config.txt", "w")
        f.write(str(not self.graphsFrame_2.isHidden())+" "+str(not self.graphsFrame.isHidden()))
        f.close()


    def updateGoalsGraph(self, userID, dbUserName, dbPassword, dbHost, dbName):
        # COnnect to database
        connection = psycopg2.connect(user=dbUserName, password=dbPassword, host=dbHost, database=dbName)
        cur = connection.cursor()
        cur.execute("SELECT * FROM goals WHERE userID = %s", [str(userID)])
        goalsRawData = cur.fetchall()
        doneGoalsList = []
        for row in goalsRawData:
            addGoal = Goal.createEmptyGoal()
            addGoal.startValue = float(row[5])
            addGoal.currentValue = float(row[6])
            addGoal.goalValue = float(row[7])

            #Get all the finished goals that are within a week
            if addGoal.getProgress() == 100 and row[4] >= (date.today() + timedelta(days=-7)):
                addGoal.goalID = row[0]
                addGoal.setDate = row[2].strftime('%d-%m-%Y')
                addGoal.goalName = row[3]
                addGoal.endDate = row[4].strftime('%d-%m-%Y')
                addGoal.goalType = GoalType(row[8])
                doneGoalsList.append(addGoal)

        # connection.commit()
        cur.close()
        connection.close()


        #Graph
        series = QLineSeries()

        goalsAchievedArray = [0] * 8

        for curGoal in doneGoalsList:
            goalsAchievedArray[7 - abs((date.today() - datetime.strptime(curGoal.endDate, '%d-%m-%Y').date()).days)] += 1

        # series.append(0, 6)   x,y


        cumulativeSum = 0
        for i in range(8):
            cumulativeSum += goalsAchievedArray[i]
            curDate = (date.today() + timedelta(days=i - 7)).strftime('%d-%m-%Y')
            series.append(QDateTime.fromString(curDate, "dd-MM-yyyy").toMSecsSinceEpoch(), cumulativeSum)
            #series.append(7-i, cumulativeSum)

        #chart.legend().hide()
        chart = QChart()
        chart.setTitle("Goals finished in the past week (Cumulative)")
        chart.addSeries(series)
        chart.legend().hide()


        #X Axis
        axisTitleFont = QFont("Segoe UI", 8, QFont.Normal)
        axisTitleFont.setBold(True)
        xAxis = QDateTimeAxis()
        xAxis.setFormat("dd/MM");
        xAxis.setTitleText("Date");
        xAxis.setTickCount(8);
        xAxis.setLabelsFont(QFont("Segoe UI Semibold", 8, QFont.Normal))
        xAxis.setTitleFont(axisTitleFont)

        #Y Axis
        yAxis = QValueAxis()
        yAxis.setTitleText("Goals Achieved (Cumulative)");
        yAxis.setRange(0, cumulativeSum * 2)
        yAxis.setLabelFormat("%d")
        yAxis.setTickCount(cumulativeSum * 2 + 1)
        yAxis.setLabelsFont(QFont("Segoe UI Semibold", 8, QFont.Normal))
        yAxis.setTitleFont(axisTitleFont)

        chart.addAxis(xAxis, PyQt5.QtCore.Qt.AlignBottom)
        series.attachAxis(xAxis)

        chart.addAxis(yAxis, PyQt5.QtCore.Qt.AlignLeft)
        series.attachAxis(yAxis)

        titleFont = QFont("Segoe UI Semibold", 10, QFont.Normal)
        titleFont.setUnderline(True)
        chart.setBackgroundRoundness(0)
        chart.setTitleFont(titleFont)
        #chart.setBackgroundBrush(QtGui.QBrush(QtGui.QColor.fromRgb(221, 221, 221)))


        chart.layout().setContentsMargins(0, 0, 0, 0)
        chartView = QChartView(chart)
        chartView.setRenderHint(QPainter.Antialiasing)
        self.GraphContainer_3.layout().addWidget(chartView)


    def updateActivityGraph(self, userID, userName, password, host, database):
        # graph widget
        #     self.graphWidget = pg.PlotWidget()
        graphWidget = pg.PlotWidget()
        pg.setConfigOptions(antialias=True)
        points = []
        graphWidget.setBackground((255, 255, 255))
        #graphWidget.setTitle("kcals burned per day")
        graphWidget.setTitle("<span style=\"color:rgb(76,76,76);font: 63 10pt Segoe UI Semibold;text-decoration: underline;\">Kcals Burned Per Day</span>")
        #graphWidget.setLabel("bottom", "days active")
        graphWidget.setLabel("bottom", "<span style=\"color:rgb(76,76,76);font: bold 9pt Segoe UI;\">Days Active</span>")
        #graphWidget.setLabel("left", "calories burned (kcals)")
        graphWidget.setLabel("left", "<span style=\"color:rgb(76,76,76);font: bold 9pt Segoe UI;\">Calories Burned (kcals)</span>")
        # function to get data points in the controller
        data = self.getActivityDailyCalsBurned(userID, userName, password, host, database)
        graphWidget.setRange(xRange=[0, len(data) - 1], yRange=([0, max(data)]), padding=0)
        for point in range(len(data)):
            points.append(point)
        pen = pg.mkPen(color=(32, 159, 223), width=2)
        axisPen = pg.mkPen(color=(76, 76, 76), width=1)
        graphWidget.getAxis('bottom').setPen(axisPen)
        graphWidget.getAxis('left').setPen(axisPen)
        graphWidget.plot(points, data, pen=pen)
        self.GraphContainer_2.layout().addWidget(graphWidget)


    def getActivityDailyCalsBurned(self, userID, userName, password, host, database):
        # connection = psycopg2.connect(user=self.dbUserName, password=self.dbPassword, host=self.dbHost, port=self.dbport,
        #                               database=self.dbName)

        connection = psycopg2.connect(user=userName, password=password, host=host, database=database)
        cur = connection.cursor()
        cur.execute("SELECT SUM(kcalsburned) FROM userview WHERE userid = %s GROUP BY datedone", [userID])
        data = cur.fetchall()
        connection.close()
        calsBurned = [0]
        for x in range(len(data)):
            calsBurned.append(data[x][0])
        return calsBurned

    def updateDietGraph(self, userID, userName, password, host, database):
        # Connect to database
        connection = psycopg2.connect(user=userName, password=password, host=host, database=database)
        cur = connection.cursor()
        cur.execute(
            'SELECT SUM(dietcalories), dietdate FROM diets WHERE userid = %s AND CURRENT_DATE - dietdate < 7 AND CURRENT_DATE - dietdate >= 0 GROUP BY dietdate',
            [str(userID)])
        data = cur.fetchall()
        connection.close()

        series = QLineSeries()

        # for x in data:
        #     series.append(int(x[1].strftime("%Y%m%d")), x[0])

        epoch = datetime.utcfromtimestamp(0).date()

        def unix_time_millis(dt):
            return (dt - epoch).total_seconds() * 1000.0

        # for x in data:
        #     series.append(unix_time_millis(x[1]), x[0])

        days = {}

        for x in range(0, 7):
            day = datetime.now() - timedelta(days=x)
            days[unix_time_millis(day.date())] = 0

        for x in data:
            days[unix_time_millis(x[1])] = x[0]

        for day in days:
            series.append(day, days[day])
            print(day, days[day])


        # def unix_time_millis(dt):
        #     return int((dt - epoch).total_seconds() * 1000.0)

        epoch = datetime.utcfromtimestamp(0)

        # chart.legend().hide()
        chart = QChart()
        chart.setTitle("Calories Consumed in the Past Week")
        chart.addSeries(series)
        chart.legend().hide()

        # X Axis
        mini = datetime.now() - timedelta(days=6)
        maxi = datetime.now()
        mini = mini.strftime("%Y-%m-%d")
        maxi = maxi.strftime("%Y-%m-%d")
        mini = QtCore.QDate.fromString(mini, 'yyyy-MM-dd')
        maxi = QtCore.QDate.fromString(maxi, 'yyyy-MM-dd')
        mini = QtCore.QDateTime(mini)
        maxi = QtCore.QDateTime(maxi)

        xAxis = QDateTimeAxis()
        xAxis.setFormat("dd/MM")
        xAxis.setTitleText("Date")
        xAxis.setRange(mini, maxi)

        xAxis.setTickCount(7)

        axisTitleFont = QFont("Segoe UI", 8, QFont.Normal)
        axisTitleFont.setBold(True)
        xAxis.setLabelsFont(QFont("Segoe UI Semibold", 8, QFont.Normal))
        xAxis.setTitleFont(axisTitleFont)

        # Y Axis
        yAxis = QValueAxis()
        yAxis.setTitleText("Total Calories (kcal)")
        yAxis.setLabelsFont(QFont("Segoe UI Semibold", 8, QFont.Normal))
        yAxis.setTitleFont(axisTitleFont)

        chart.addAxis(xAxis, PyQt5.QtCore.Qt.AlignBottom)
        series.attachAxis(xAxis)

        chart.addAxis(yAxis, PyQt5.QtCore.Qt.AlignLeft)
        series.attachAxis(yAxis)

        chartView = QChartView(chart)
        chartView.setRenderHint(QPainter.Antialiasing)

        chart.setBackgroundRoundness(0)
        titleFont = QFont("Segoe UI Semibold", 10, QFont.Normal)
        titleFont.setUnderline(True)
        chart.setTitleFont(titleFont)
        chart.layout().setContentsMargins(0, 0, 0, 0)
        chartView = QChartView(chart)
        chartView.setRenderHint(QPainter.Antialiasing)
        self.GraphContainer_4.layout().addWidget(chartView)
