# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'signup_extended_info.ui'
#
# Created by: PyQt5 UI code generator 5.14.1
#
# WARNING! All changes made in this file will be lost!


from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtWidgets import QMessageBox

class EditExtendedInfoGUI(object):
    def setupUi(self, Dialog, ID):
        Dialog.setObjectName("Dialog")
        Dialog.resize(333, 273)
        Dialog.setMaximumSize(QtCore.QSize(333, 280))
        Dialog.setLayoutDirection(QtCore.Qt.LeftToRight)
        Dialog.setStyleSheet("QLabel\n"
                             "{\n"
                             "color: rgb(255, 255, 255);\n"
                             "}\n"
                             "\n"
                             "QDialog\n"
                             "{\n"
                             "    background-color: rgb(76, 76, 76);\n"
                             "    color: rgb(255, 255, 255);\n"
                             "font: 75 10pt \"Segoe UI Semibold\";\n"
                             "font-weight:bold;\n"
                             "\n"
                             "\n"
                             "}\n"
                             "QPushButton {\n"
                             "    background-color: rgb(81, 175, 81);\n"
                             "    padding: 1px;\n"
                             "    border: 4px solid rgb(81, 175, 81);\n"
                             "    border-radius: 1px;\n"
                             "    color: rgb(255, 255, 255);\n"
                             "font: 63 8pt \"Segoe UI Semibold\";\n"
                             "}\n"
                             "\n"
                             "\n"
                             "\n"
                             "QPushButton:Pressed{\n"
                             "    background-color: rgb(55, 140, 55);\n"
                             "    border-color: rgb(55, 140, 55);\n"
                             "}")
        self.verticalLayout_2 = QtWidgets.QVBoxLayout(Dialog)
        self.verticalLayout_2.setObjectName("verticalLayout_2")
        self.frame = QtWidgets.QFrame(Dialog)
        self.frame.setObjectName("frame")
        self.verticalLayout = QtWidgets.QVBoxLayout(self.frame)
        self.verticalLayout.setContentsMargins(-1, 10, -1, -1)
        self.verticalLayout.setObjectName("verticalLayout")
        self.frame_2 = QtWidgets.QFrame(self.frame)
        self.frame_2.setObjectName("frame_2")
        self.horizontalLayout_2 = QtWidgets.QHBoxLayout(self.frame_2)
        self.horizontalLayout_2.setContentsMargins(1, 1, 1, 1)
        self.horizontalLayout_2.setObjectName("horizontalLayout_2")
        self.label_3 = QtWidgets.QLabel(self.frame_2)
        self.label_3.setStyleSheet("color: rgb(255, 255, 255);\n"
                                   "font: 75 10pt \"Segoe UI Semibold\";\n"
                                   "font-weight:bold;")
        self.label_3.setAlignment(QtCore.Qt.AlignBottom | QtCore.Qt.AlignLeading | QtCore.Qt.AlignLeft)
        self.label_3.setObjectName("label_3")
        self.horizontalLayout_2.addWidget(self.label_3)
        self.label_4 = QtWidgets.QLabel(self.frame_2)
        self.label_4.setStyleSheet("color: rgb(255, 255, 255);\n"
                                   "font: 75 10pt \"Segoe UI Semibold\";\n"
                                   "font-weight:bold;")
        self.label_4.setAlignment(QtCore.Qt.AlignBottom | QtCore.Qt.AlignLeading | QtCore.Qt.AlignLeft)
        self.label_4.setObjectName("label_4")
        self.horizontalLayout_2.addWidget(self.label_4)
        self.verticalLayout.addWidget(self.frame_2)
        self.frame_3 = QtWidgets.QFrame(self.frame)
        self.frame_3.setObjectName("frame_3")
        self.horizontalLayout = QtWidgets.QHBoxLayout(self.frame_3)
        self.horizontalLayout.setContentsMargins(1, 1, 1, 1)
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.spinBox = QtWidgets.QSpinBox(self.frame_3)
        self.spinBox.setEnabled(True)
        self.spinBox.setStyleSheet("QSpinBox{\n"
                                   "   background-color: rgb(225, 225, 225);\n"
                                   "\n"
                                   "    \n"
                                   "    font: 63 8pt \"Segoe UI Semibold\";\n"
                                   "\n"
                                   "}\n"
                                   "\n"
                                   "QSpinBox:up-button{\n"
                                   "    background-color: rgb(81, 175, 81);\n"
                                   "    padding: 1px;\n"
                                   "    border: 4px solid rgb(81, 175, 81);\n"
                                   "    color: rgb(255, 255, 255);\n"
                                   "}\n"
                                   "\n"
                                   "QSpinBox:up-button:Pressed{\n"
                                   "    background-color: rgb(55, 140, 55);\n"
                                   "    border-color: rgb(55, 140, 55);\n"
                                   "}\n"
                                   "\n"
                                   "QSpinBox:down-button{\n"
                                   "    background-color: rgb(81, 175, 81);\n"
                                   "    padding: 1px;\n"
                                   "    border: 4px solid rgb(81, 175, 81);\n"
                                   "    color: rgb(255, 255, 255);\n"
                                   "}\n"
                                   "\n"
                                   "QSpinBox:down-button:Pressed{\n"
                                   "    background-color: rgb(55, 140, 55);\n"
                                   "    border-color: rgb(55, 140, 55);\n"
                                   "}\n"
                                   "\n"
                                   "\n"
                                   "\n"
                                   "QSpinBox:up-arrow {\n"
                                   "width: 0; \n"
                                   "  height: 0; \n"
                                   "  border-left: 4px solid rgb(81, 175, 81);\n"
                                   "  border-right: 4px solid rgb(81, 175, 81);\n"
                                   "  border-bottom: 4px solid  rgb(255, 255, 255);\n"
                                   "}\n"
                                   "\n"
                                   "QSpinBox:down-arrow {\n"
                                   "width: 0; \n"
                                   "  height: 0; \n"
                                   "  border-left: 4px solid rgb(81, 175, 81);\n"
                                   "  border-right: 4px solid rgb(81, 175, 81);\n"
                                   "  border-top: 4px solid  rgb(255, 255, 255);\n"
                                   "}\n"
                                   "")
        self.spinBox.setFrame(False)
        self.spinBox.setMaximum(270)
        self.spinBox.setObjectName("spinBox")
        self.horizontalLayout.addWidget(self.spinBox)
        self.spinBox_2 = QtWidgets.QSpinBox(self.frame_3)
        self.spinBox_2.setStyleSheet("QSpinBox{\n"
                                     "   background-color: rgb(225, 225, 225);\n"
                                     "\n"
                                     "    \n"
                                     "    font: 63 8pt \"Segoe UI Semibold\";\n"
                                     "\n"
                                     "}\n"
                                     "\n"
                                     "QSpinBox:up-button{\n"
                                     "    background-color: rgb(81, 175, 81);\n"
                                     "    padding: 1px;\n"
                                     "    border: 4px solid rgb(81, 175, 81);\n"
                                     "    color: rgb(255, 255, 255);\n"
                                     "}\n"
                                     "\n"
                                     "QSpinBox:up-button:Pressed{\n"
                                     "    background-color: rgb(55, 140, 55);\n"
                                     "    border-color: rgb(55, 140, 55);\n"
                                     "}\n"
                                     "\n"
                                     "QSpinBox:down-button{\n"
                                     "    background-color: rgb(81, 175, 81);\n"
                                     "    padding: 1px;\n"
                                     "    border: 4px solid rgb(81, 175, 81);\n"
                                     "    color: rgb(255, 255, 255);\n"
                                     "}\n"
                                     "\n"
                                     "QSpinBox:down-button:Pressed{\n"
                                     "    background-color: rgb(55, 140, 55);\n"
                                     "    border-color: rgb(55, 140, 55);\n"
                                     "}\n"
                                     "\n"
                                     "\n"
                                     "\n"
                                     "QSpinBox:up-arrow {\n"
                                     "width: 0; \n"
                                     "  height: 0; \n"
                                     "  border-left: 4px solid rgb(81, 175, 81);\n"
                                     "  border-right: 4px solid rgb(81, 175, 81);\n"
                                     "  border-bottom: 4px solid  rgb(255, 255, 255);\n"
                                     "}\n"
                                     "\n"
                                     "QSpinBox:down-arrow {\n"
                                     "width: 0; \n"
                                     "  height: 0; \n"
                                     "  border-left: 4px solid rgb(81, 175, 81);\n"
                                     "  border-right: 4px solid rgb(81, 175, 81);\n"
                                     "  border-top: 4px solid  rgb(255, 255, 255);\n"
                                     "}\n"
                                     "")
        self.spinBox_2.setFrame(False)
        self.spinBox_2.setMaximum(500)
        self.spinBox_2.setObjectName("spinBox_2")
        self.horizontalLayout.addWidget(self.spinBox_2)
        self.horizontalLayout.setStretch(0, 1)
        self.horizontalLayout.setStretch(1, 1)
        self.verticalLayout.addWidget(self.frame_3)
        self.label_2 = QtWidgets.QLabel(self.frame)
        self.label_2.setStyleSheet("color: rgb(255, 255, 255);\n"
                                   "font: 75 10pt \"Segoe UI Semibold\";\n"
                                   "font-weight:bold;")
        self.label_2.setAlignment(QtCore.Qt.AlignBottom | QtCore.Qt.AlignLeading | QtCore.Qt.AlignLeft)
        self.label_2.setObjectName("label_2")
        self.verticalLayout.addWidget(self.label_2)
        self.dateEdit = QtWidgets.QDateEdit(self.frame)
        self.dateEdit.setStyleSheet("QDateEdit{\n"
                                    "   background-color: rgb(225, 225, 225);\n"
                                    "\n"
                                    "    \n"
                                    "    font: 63 8pt \"Segoe UI Semibold\";\n"
                                    "\n"
                                    "}\n"
                                    "\n"
                                    "QDateEdit:up-button{\n"
                                    "    background-color: rgb(81, 175, 81);\n"
                                    "    padding: 1px;\n"
                                    "    border: 4px solid rgb(81, 175, 81);\n"
                                    "    color: rgb(255, 255, 255);\n"
                                    "}\n"
                                    "\n"
                                    "QDateEdit:up-button:Pressed{\n"
                                    "    background-color: rgb(55, 140, 55);\n"
                                    "    border-color: rgb(55, 140, 55);\n"
                                    "}\n"
                                    "\n"
                                    "QDateEdit:down-button{\n"
                                    "    background-color: rgb(81, 175, 81);\n"
                                    "    padding: 1px;\n"
                                    "    border: 4px solid rgb(81, 175, 81);\n"
                                    "    color: rgb(255, 255, 255);\n"
                                    "}\n"
                                    "\n"
                                    "QDateEdit:down-button:Pressed{\n"
                                    "    background-color: rgb(55, 140, 55);\n"
                                    "    border-color: rgb(55, 140, 55);\n"
                                    "}\n"
                                    "\n"
                                    "\n"
                                    "\n"
                                    "QDateEdit:up-arrow {\n"
                                    "width: 0; \n"
                                    "  height: 0; \n"
                                    "  border-left: 4px solid rgb(81, 175, 81);\n"
                                    "  border-right: 4px solid rgb(81, 175, 81);\n"
                                    "  border-bottom: 4px solid  rgb(255, 255, 255);\n"
                                    "}\n"
                                    "\n"
                                    "QDateEdit:down-arrow {\n"
                                    "width: 0; \n"
                                    "  height: 0; \n"
                                    "  border-left: 4px solid rgb(81, 175, 81);\n"
                                    "  border-right: 4px solid rgb(81, 175, 81);\n"
                                    "  border-top: 4px solid  rgb(255, 255, 255);\n"
                                    "}\n"
                                    "")
        self.dateEdit.setFrame(False)
        self.dateEdit.setObjectName("dateEdit")
        self.verticalLayout.addWidget(self.dateEdit)
        self.label_5 = QtWidgets.QLabel(self.frame)
        self.label_5.setStyleSheet("color: rgb(255, 255, 255);\n"
                                   "font: 75 10pt \"Segoe UI Semibold\";\n"
                                   "font-weight:bold;")
        self.label_5.setAlignment(QtCore.Qt.AlignBottom | QtCore.Qt.AlignLeading | QtCore.Qt.AlignLeft)
        self.label_5.setObjectName("label_5")
        self.verticalLayout.addWidget(self.label_5)
        self.comboBox = QtWidgets.QComboBox(self.frame)
        self.comboBox.setStyleSheet("QComboBox{\n"
                                    "   background-color: rgb(225, 225, 225);\n"
                                    "font: 63 8pt \"Segoe UI Semibold\";\n"
                                    "\n"
                                    "}\n"
                                    "\n"
                                    "QComboBox:drop-down{\n"
                                    "    background-color: rgb(81, 175, 81);\n"
                                    "    padding: 1px;\n"
                                    "    border: 4px solid rgb(81, 175, 81);\n"
                                    "    border-radius: 1px;\n"
                                    "    color: rgb(255, 255, 255);\n"
                                    "}\n"
                                    "\n"
                                    "QComboBox:drop-down:Pressed{\n"
                                    "    background-color: rgb(55, 140, 55);\n"
                                    "    border-color: rgb(55, 140, 55);\n"
                                    "}\n"
                                    "\n"
                                    "\n"
                                    "QComboBox:down-arrow {\n"
                                    "width: 0; \n"
                                    "  height: 0; \n"
                                    "  border-left: 5px solid rgb(81, 175, 81);\n"
                                    "  border-right: 5px solid rgb(81, 175, 81);\n"
                                    "  border-top: 5px solid  rgb(255, 255, 255);\n"
                                    "    \n"
                                    "\n"
                                    "}\n"
                                    "\n"
                                    "QComboBox QAbstractItemView {\n"
                                    "background-color: rgb(225, 225, 225);\n"
                                    "    outline: none;\n"
                                    "}\n"
                                    "\n"
                                    "\n"
                                    "\n"
                                    "\n"
                                    "QPushButton:Pressed{\n"
                                    "    background-color: rgb(55, 140, 55);\n"
                                    "    border-color: rgb(55, 140, 55);\n"
                                    "}\n"
                                    "\n"
                                    "")
        self.comboBox.setEditable(True)
        self.comboBox.setFrame(False)
        self.comboBox.setObjectName("comboBox")
        self.comboBox.addItem("")
        self.comboBox.setItemText(0, "")
        self.comboBox.addItem("")
        self.comboBox.addItem("")
        self.comboBox.addItem("")
        self.verticalLayout.addWidget(self.comboBox)
        spacerItem = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Preferred)
        self.verticalLayout.addItem(spacerItem)
        self.buttonBox = QtWidgets.QDialogButtonBox(self.frame)
        self.buttonBox.setStyleSheet("QPushButton {\n"
                                     "    background-color: rgb(81, 175, 81);\n"
                                     "    padding: 2px;\n"
                                     "    border: 4px solid rgb(81, 175, 81);\n"
                                     "    border-radius: 1px;\n"
                                     "    color: rgb(255, 255, 255);\n"
                                     "    padding: 0 8px 0 8px;\n"
                                     "    min-width: 50px;\n"
                                     "font: 63 8pt \"Segoe UI Semibold\";\n"
                                     "}\n"
                                     "\n"
                                     "\n"
                                     "\n"
                                     "QPushButton:Pressed{\n"
                                     "    background-color: rgb(55, 140, 55);\n"
                                     "    border-color: rgb(55, 140, 55);\n"
                                     "}")
        self.buttonBox.setOrientation(QtCore.Qt.Horizontal)
        self.buttonBox.setStandardButtons(QtWidgets.QDialogButtonBox.Cancel | QtWidgets.QDialogButtonBox.Ok)
        self.buttonBox.setObjectName("buttonBox")
        self.verticalLayout.addWidget(self.buttonBox)
        self.verticalLayout_2.addWidget(self.frame)

        self.retranslateUi(Dialog)
        QtCore.QMetaObject.connectSlotsByName(Dialog)

        self.buttonBox.accepted.connect(lambda: self.checkData(ID, Dialog))
        self.buttonBox.rejected.connect(Dialog.reject)

    def checkData(self, ID, Dialog):
        if (self.spinBox_2.value() is not 0 and self.spinBox.value() is not 0):
            from Controller.EditExtendedInfoController import EditExtendedInfoController
            EditExtendedInfoController.addData(self, self.spinBox_2.text(), self.spinBox.text(), self.dateEdit.text(), ID)
        else:
            print("No values can be left as '0'")
            return

    def retranslateUi(self, Dialog):
        _translate = QtCore.QCoreApplication.translate
        Dialog.setWindowTitle(_translate("Dialog", "Dialog"))
        self.label_3.setText(_translate("Dialog", "Height(cm)"))
        self.label_4.setText(_translate("Dialog", "Weight(kg)"))
        self.label_2.setText(_translate("Dialog", "Date Of Birth:"))
        self.label_5.setText(_translate("Dialog", "Gender"))
        self.comboBox.setItemText(1, _translate("Dialog", "Male"))
        self.comboBox.setItemText(2, _translate("Dialog", "Female"))
        self.comboBox.setItemText(3, _translate("Dialog", "Other"))
