from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtCore import QDateTime
from PyQt5.QtWidgets import QMessageBox, QListWidgetItem

from Model.Goal import Goal
from Other.Event import Event
from View.CustomGoalListItem import CustomGoalItemWidget
from View.goalDetail import GoalDetailDialog
from View.goalsAchieved import GoalsAchievedWindow


class GoalMainUI(object):


    def __init__(self):
        self.onGoalAddedEvent = Event()  # Format: Func(Goal)
        self.onGoalDetailRequestEvent = Event()  # Format: Func(index)
        self.onGoalEditedEvent = Event()  # Format: Func(index, Goal)
        self.onGoalRemovedEvent = Event()  # Format: Func(index)
        self.onGoalsDoneRequestEvent = Event() #Format: Func()


    def setupUi(self, MainWindow, launchFrom):
        MainWindow.setObjectName("MainWindow")
        MainWindow.setEnabled(True)
        MainWindow.resize(600, 400)
        MainWindow.setMaximumSize(QtCore.QSize(600, 800))
        MainWindow.setAutoFillBackground(False)
        MainWindow.setStyleSheet("background-color: rgb(255, 255, 255);")
        self.gridLayout = QtWidgets.QGridLayout(MainWindow)
        self.gridLayout.setContentsMargins(0, 0, 0, 0)
        self.gridLayout.setObjectName("gridLayout")
        self.h_frame = QtWidgets.QFrame(MainWindow)
        self.h_frame.setAutoFillBackground(False)
        self.h_frame.setStyleSheet("")
        self.h_frame.setObjectName("h_frame")
        self.horizontalLayout = QtWidgets.QHBoxLayout(self.h_frame)
        self.horizontalLayout.setContentsMargins(0, 0, 0, 0)
        self.horizontalLayout.setSpacing(0)
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.frame = QtWidgets.QFrame(self.h_frame)
        self.frame.setStyleSheet("background-color: rgb(208, 208, 208);\n"
                                 "background-color: rgb(158, 158, 158);\n"
                                 "background-color: rgb(226, 226, 226);\n"
                                 "background-color: rgb(98, 98, 98);\n"
                                 "background-color: rgb(76, 76, 76);")
        self.frame.setObjectName("frame")
        self.verticalLayout = QtWidgets.QVBoxLayout(self.frame)
        self.verticalLayout.setObjectName("verticalLayout")
        spacerItem = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.verticalLayout.addItem(spacerItem)
        self.frame_4 = QtWidgets.QFrame(self.frame)
        self.frame_4.setMinimumSize(QtCore.QSize(0, 0))
        self.frame_4.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.frame_4.setFrameShadow(QtWidgets.QFrame.Raised)
        self.frame_4.setObjectName("frame_4")
        self.gridLayout_3 = QtWidgets.QGridLayout(self.frame_4)
        self.gridLayout_3.setObjectName("gridLayout_3")
        self.AddGoalButton = QtWidgets.QPushButton(self.frame_4)
        self.AddGoalButton.setEnabled(True)
        self.AddGoalButton.setMaximumSize(QtCore.QSize(150, 16777215))
        font = QtGui.QFont()
        font.setFamily("Segoe UI Semibold")
        font.setPointSize(10)
        font.setBold(False)
        font.setItalic(False)
        font.setWeight(7)
        self.AddGoalButton.setFont(font)
        self.AddGoalButton.setStyleSheet("QPushButton {\n"
                                         "    background-color: rgb(81, 175, 81);\n"
                                         "    padding: 3px;\n"
                                         "    border: 4px solid rgb(81, 175, 81);\n"
                                         "    border-radius: 1px;\n"
                                         "    color: rgb(255, 255, 255);\n"
                                         "font: 63 10pt \"Segoe UI Semibold\";\n"
                                         "}\n"
                                         "\n"
                                         "\n"
                                         "\n"
                                         "QPushButton:Pressed{\n"
                                         "    background-color: rgb(55, 140, 55);\n"
                                         "    border-color: rgb(55, 140, 55);\n"
                                         "}")
        self.AddGoalButton.setAutoDefault(True)
        self.AddGoalButton.setObjectName("AddGoalButton")
        self.gridLayout_3.addWidget(self.AddGoalButton, 0, 0, 1, 1)
        self.verticalLayout.addWidget(self.frame_4)
        self.frame_5 = QtWidgets.QFrame(self.frame)
        self.frame_5.setMinimumSize(QtCore.QSize(0, 0))
        self.frame_5.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.frame_5.setFrameShadow(QtWidgets.QFrame.Raised)
        self.frame_5.setObjectName("frame_5")
        self.gridLayout_4 = QtWidgets.QGridLayout(self.frame_5)
        self.gridLayout_4.setObjectName("gridLayout_4")
        self.EditGoalButton = QtWidgets.QPushButton(self.frame_5)
        self.EditGoalButton.setMaximumSize(QtCore.QSize(150, 16777215))
        font = QtGui.QFont()
        font.setFamily("Segoe UI Semibold")
        font.setPointSize(10)
        font.setBold(False)
        font.setItalic(False)
        font.setWeight(7)
        self.EditGoalButton.setFont(font)
        self.EditGoalButton.setStyleSheet("QPushButton {\n"
                                          "    background-color: rgb(81, 175, 81);\n"
                                          "    padding: 3px;\n"
                                          "    border: 4px solid rgb(81, 175, 81);\n"
                                          "    border-radius: 1px;\n"
                                          "    color: rgb(255, 255, 255);\n"
                                          "font: 63 10pt \"Segoe UI Semibold\";\n"
                                          "}\n"
                                          "\n"
                                          "\n"
                                          "\n"
                                          "QPushButton:Pressed{\n"
                                          "    background-color: rgb(55, 140, 55);\n"
                                          "    border-color: rgb(55, 140, 55);\n"
                                          "}")
        self.EditGoalButton.setObjectName("EditGoalButton")
        self.gridLayout_4.addWidget(self.EditGoalButton, 0, 0, 1, 1)
        self.verticalLayout.addWidget(self.frame_5)
        self.frame_6 = QtWidgets.QFrame(self.frame)
        self.frame_6.setMinimumSize(QtCore.QSize(0, 0))
        self.frame_6.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.frame_6.setFrameShadow(QtWidgets.QFrame.Raised)
        self.frame_6.setObjectName("frame_6")
        self.gridLayout_5 = QtWidgets.QGridLayout(self.frame_6)
        self.gridLayout_5.setObjectName("gridLayout_5")
        self.RemoveGoalButton = QtWidgets.QPushButton(self.frame_6)
        self.RemoveGoalButton.setMaximumSize(QtCore.QSize(150, 16777215))
        font = QtGui.QFont()
        font.setFamily("Segoe UI Semibold")
        font.setPointSize(10)
        font.setBold(False)
        font.setItalic(False)
        font.setWeight(7)
        self.RemoveGoalButton.setFont(font)
        self.RemoveGoalButton.setStyleSheet("QPushButton {\n"
                                            "    background-color: rgb(81, 175, 81);\n"
                                            "    padding: 3px;\n"
                                            "    border: 4px solid rgb(81, 175, 81);\n"
                                            "    border-radius: 1px;\n"
                                            "    color: rgb(255, 255, 255);\n"
                                            "font: 63 10pt \"Segoe UI Semibold\";\n"
                                            "}\n"
                                            "\n"
                                            "\n"
                                            "\n"
                                            "QPushButton:Pressed{\n"
                                            "    background-color: rgb(55, 140, 55);\n"
                                            "    border-color: rgb(55, 140, 55);\n"
                                            "}")
        self.RemoveGoalButton.setFlat(False)
        self.RemoveGoalButton.setObjectName("RemoveGoalButton")
        self.gridLayout_5.addWidget(self.RemoveGoalButton, 0, 0, 1, 1)
        self.verticalLayout.addWidget(self.frame_6)
        spacerItem1 = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.verticalLayout.addItem(spacerItem1)
        self.frame_3 = QtWidgets.QFrame(self.frame)
        self.frame_3.setMaximumSize(QtCore.QSize(16777215, 40))
        self.frame_3.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.frame_3.setFrameShadow(QtWidgets.QFrame.Raised)
        self.frame_3.setObjectName("frame_3")
        self.horizontalLayout_2 = QtWidgets.QHBoxLayout(self.frame_3)
        self.horizontalLayout_2.setContentsMargins(0, 0, 0, 0)
        self.horizontalLayout_2.setObjectName("horizontalLayout_2")
        self.GoalsAchievedText = QtWidgets.QLabel(self.frame_3)
        self.GoalsAchievedText.setStyleSheet("color: rgb(255, 255, 255);\n"
                                             "font: 63 10pt \"Segoe UI Semibold\";")
        self.GoalsAchievedText.setObjectName("GoalsAchievedText")
        self.horizontalLayout_2.addWidget(self.GoalsAchievedText)
        self.ShowGoalsDoneButton = QtWidgets.QPushButton(self.frame_3)
        self.ShowGoalsDoneButton.setMaximumSize(QtCore.QSize(80, 16777215))
        self.ShowGoalsDoneButton.setStyleSheet("QPushButton {\n"
                                               "    background-color: rgb(81, 175, 81);\n"
                                               "    padding: 3px;\n"
                                               "    border: 4px solid rgb(81, 175, 81);\n"
                                               "    border-radius: 1px;\n"
                                               "    color: rgb(255, 255, 255);\n"
                                               "font: 63 10pt \"Segoe UI Semibold\";\n"
                                               "}\n"
                                               "\n"
                                               "\n"
                                               "\n"
                                               "QPushButton:Pressed{\n"
                                               "    background-color: rgb(55, 140, 55);\n"
                                               "    border-color: rgb(55, 140, 55);\n"
                                               "}")
        self.ShowGoalsDoneButton.setObjectName("ShowGoalsDoneButton")
        self.horizontalLayout_2.addWidget(self.ShowGoalsDoneButton)
        self.verticalLayout.addWidget(self.frame_3)
        self.horizontalLayout.addWidget(self.frame)
        self.frame_2 = QtWidgets.QFrame(self.h_frame)
        self.frame_2.setStyleSheet("")
        self.frame_2.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.frame_2.setFrameShadow(QtWidgets.QFrame.Raised)
        self.frame_2.setObjectName("frame_2")
        self.gridLayout_2 = QtWidgets.QGridLayout(self.frame_2)
        self.gridLayout_2.setContentsMargins(20, 20, 20, 20)
        self.gridLayout_2.setObjectName("gridLayout_2")
        self.GoalsList = QtWidgets.QListWidget(self.frame_2)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.GoalsList.sizePolicy().hasHeightForWidth())
        self.GoalsList.setSizePolicy(sizePolicy)
        self.GoalsList.setStyleSheet("background-color: rgb(255, 255, 255);\n"
                                     "background-color: rgb(225, 225, 225);")
        self.GoalsList.setObjectName("GoalsList")
        self.gridLayout_2.addWidget(self.GoalsList, 0, 0, 1, 1)
        self.horizontalLayout.addWidget(self.frame_2)
        self.horizontalLayout.setStretch(0, 4)
        self.horizontalLayout.setStretch(1, 5)
        self.gridLayout.addWidget(self.h_frame, 0, 0, 1, 1)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)



        #UI init
        MainWindow.setWindowIcon(QtGui.QIcon('icon.png'))
        self.RemoveGoalButton.clicked.connect(self.onRemoveGoal)
        self.AddGoalButton.clicked.connect(self.onAddGoal)
        self.EditGoalButton.clicked.connect(self.onEditGoal)
        self.ShowGoalsDoneButton.clicked.connect(self.requestDoneGoals)
        if (launchFrom is True):
            self.AddGoalButton.hide()
            self.EditGoalButton.hide()
            self.RemoveGoalButton.hide()
            self.frame_4.hide()
            self.frame_5.hide()
            self.frame_6.hide()

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "MainWindow"))
        self.AddGoalButton.setText(_translate("MainWindow", "+ Add Goal"))
        self.EditGoalButton.setText(_translate("MainWindow", "O Edit Goal"))
        self.RemoveGoalButton.setText(_translate("MainWindow", "x Remove Goal"))
        self.GoalsAchievedText.setText(_translate("MainWindow", "Goals History:"))
        self.ShowGoalsDoneButton.setText(_translate("MainWindow", "Show"))


        MainWindow.setWindowTitle(_translate("MainWindow", "Goals"))


    def onAddGoal(self):
        newGoal = Goal.createEmptyGoal()
        newGoal.setDate = QDateTime.currentDateTime().toString('dd-MM-yyyy')
        newGoal.endDate = QDateTime.currentDateTime().toString('dd-MM-yyyy')
        self.invokeGoalDetailWindow(newGoal, True)

    def onEditGoal(self):
        if self.GoalsList.currentRow() != -1:
            self.onGoalDetailRequestEvent.notify(self.GoalsList.currentRow())

        #PrintMsg(str(self.GoalsList.currentRow()))
        # if self.GoalsList.currentRow() != -1:
        #     editGoal = QtWidgets.QDialog()
        #     ui = GoalDetailDialog()
        #     ui.setupUi(editGoal)
        #     editGoal.show()
        #     editGoal.exec_()

    def onRemoveGoal(self):
        if self.GoalsList.currentRow() != -1:
            self.onGoalRemovedEvent.notify(self.GoalsList.currentRow())
        # self.GoalsList.takeItem(self.GoalsList.currentRow())

    def setUIList(self, showTexts):
        for text in showTexts:
            self.GoalsList.addItem(text)

    def addItemToUIList(self, goal):
        item = QListWidgetItem(self.GoalsList)
        goalWidget = CustomGoalItemWidget()
        item.setSizeHint(goalWidget.sizeHint())
        self.GoalsList.addItem(item)
        self.GoalsList.setItemWidget(item, goalWidget)
        goalWidget.GoalNameText.setText(goal.goalName)
        goalWidget.GoalTypeText.setText("Goal Type: "+goal.goalType.toString())
        goalWidget.EndDateText.setText("End Date: "+goal.endDate)
        goalWidget.GoalProgressBar.setValue(goal.getProgress())
        #self.GoalsList.addItem(addText)

    def removeItemFromUIList(self, index):
        self.GoalsList.takeItem(index)

    def changeItemFromUIList(self, index, goal):
        item = self.GoalsList.item(index)
        goalWidget = CustomGoalItemWidget()
        item.setSizeHint(goalWidget.sizeHint())
        self.GoalsList.addItem(item)
        self.GoalsList.setItemWidget(item, goalWidget)
        goalWidget.GoalNameText.setText(goal.goalName)
        goalWidget.GoalTypeText.setText("Goal Type: "+goal.goalType.toString())
        goalWidget.EndDateText.setText("End Date: "+goal.endDate)
        goalWidget.GoalProgressBar.setValue(goal.getProgress())

    def requestDoneGoals(self):
        self.onGoalsDoneRequestEvent.notify()

    def showGoalsDoneWindow(self, goals):
        dialog = QtWidgets.QDialog()
        ui = GoalsAchievedWindow()
        ui.setupUi(dialog)
        ui.setUIList(goals)
        dialog.exec()

    def invokeGoalDetailWindow(self, goal, isNewGoal):
        dialog = QtWidgets.QDialog()
        ui = GoalDetailDialog()
        ui.setupUi(dialog)
        ui.setUIFromGoal(goal)

        if dialog.exec_():
            if not isNewGoal:
                self.onGoalEditedEvent.notify(self.GoalsList.currentRow(), ui.getCurrentGoal())
            else:
                not self.onGoalAddedEvent.notify(ui.getCurrentGoal())

    def updateGoalsAchievedCount(self, count):
        self.GoalsAchievedText.setText("Goals History: "+str(count))

def PrintMsg(msgText):
    msg = QMessageBox()
    msg.setIcon(QMessageBox.Information)
    msg.setText("Message: " + msgText)
    msg.setStandardButtons(QMessageBox.Ok)
    msg.exec_()