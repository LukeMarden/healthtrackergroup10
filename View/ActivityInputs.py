# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'exerciseInputs.ui'
#
# Created by: PyQt5 UI code generator 5.14.1
#
# WARNING! All changes made in this file will be lost!


from PyQt5 import QtCore, QtGui, QtWidgets


class activityInputs(object):
    def setupUi(self, Dialog, parent, activities):
        Dialog.setObjectName("Dialog")
        Dialog.resize(354, 262)
        Dialog.setMaximumSize(QtCore.QSize(354, 262))
        Dialog.setStyleSheet("background-color: rgb(76, 76, 76);")
        self.gridLayout_2 = QtWidgets.QGridLayout(Dialog)
        self.gridLayout_2.setContentsMargins(0, 0, 0, 0)
        self.gridLayout_2.setObjectName("gridLayout_2")
        self.frame_3 = QtWidgets.QFrame(Dialog)
        self.frame_3.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.frame_3.setFrameShadow(QtWidgets.QFrame.Raised)
        self.frame_3.setObjectName("frame_3")
        self.verticalLayout = QtWidgets.QVBoxLayout(self.frame_3)
        self.verticalLayout.setObjectName("verticalLayout")
        self.frame = QtWidgets.QFrame(self.frame_3)
        self.frame.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.frame.setFrameShadow(QtWidgets.QFrame.Raised)
        self.frame.setObjectName("frame")
        self.gridLayout = QtWidgets.QGridLayout(self.frame)
        self.gridLayout.setObjectName("gridLayout")
        self.label_2 = QtWidgets.QLabel(self.frame)
        font = QtGui.QFont()
        font.setFamily("Segoe UI Semibold")
        font.setPointSize(15)
        font.setBold(True)
        font.setItalic(False)
        font.setWeight(75)
        self.label_2.setFont(font)
        self.label_2.setStyleSheet("color: rgb(255, 255, 255);\n"
                                   "font: 75 15pt \"Segoe UI Semibold\";\n"
                                   "font-weight:bold;")
        self.label_2.setAlignment(QtCore.Qt.AlignLeading | QtCore.Qt.AlignLeft | QtCore.Qt.AlignVCenter)
        self.label_2.setObjectName("label_2")
        self.gridLayout.addWidget(self.label_2, 1, 0, 1, 1)
        self.verticalLayout.addWidget(self.frame)
        self.horizontalFrame = QtWidgets.QFrame(self.frame_3)
        self.horizontalFrame.setObjectName("horizontalFrame")
        self.horizontalLayout = QtWidgets.QHBoxLayout(self.horizontalFrame)
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.label = QtWidgets.QLabel(self.horizontalFrame)
        self.label.setMaximumSize(QtCore.QSize(16777215, 23))
        font = QtGui.QFont()
        font.setFamily("Segoe UI Semibold")
        font.setPointSize(10)
        font.setBold(True)
        font.setItalic(False)
        font.setWeight(75)
        self.label.setFont(font)
        self.label.setStyleSheet("color: rgb(255, 255, 255);\n"
                                 "font: 75 10pt \"Segoe UI Semibold\";\n"
                                 "font-weight:bold;")
        self.label.setLineWidth(1)
        self.label.setScaledContents(False)
        self.label.setObjectName("label")
        self.horizontalLayout.addWidget(self.label)
        self.comboBox = QtWidgets.QComboBox(self.horizontalFrame)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.comboBox.sizePolicy().hasHeightForWidth())
        self.comboBox.setSizePolicy(sizePolicy)
        self.comboBox.setMaximumSize(QtCore.QSize(16777215, 20))
        self.comboBox.setStyleSheet("QComboBox{\n"
                                    "   background-color: rgb(225, 225, 225);\n"
                                    "font: 63 8pt \"Segoe UI Semibold\";\n"
                                    "\n"
                                    "}\n"
                                    "\n"
                                    "QComboBox:drop-down{\n"
                                    "    background-color: rgb(81, 175, 81);\n"
                                    "    padding: 1px;\n"
                                    "    border: 4px solid rgb(81, 175, 81);\n"
                                    "    border-radius: 1px;\n"
                                    "    color: rgb(255, 255, 255);\n"
                                    "}\n"
                                    "\n"
                                    "QComboBox:drop-down:Pressed{\n"
                                    "    background-color: rgb(55, 140, 55);\n"
                                    "    border-color: rgb(55, 140, 55);\n"
                                    "}\n"
                                    "\n"
                                    "\n"
                                    "QComboBox:down-arrow {\n"
                                    "width: 0; \n"
                                    "  height: 0; \n"
                                    "  border-left: 5px solid rgb(81, 175, 81);\n"
                                    "  border-right: 5px solid rgb(81, 175, 81);\n"
                                    "  border-top: 5px solid  rgb(255, 255, 255);\n"
                                    "    \n"
                                    "\n"
                                    "}\n"
                                    "\n"
                                    "QComboBox QAbstractItemView {\n"
                                    "background-color: rgb(225, 225, 225);\n"
                                    "    outline: none;\n"
                                    "}\n"
                                    "\n"
                                    "\n"
                                    "\n"
                                    "\n"
                                    "QPushButton:Pressed{\n"
                                    "    background-color: rgb(55, 140, 55);\n"
                                    "    border-color: rgb(55, 140, 55);\n"
                                    "}\n"
                                    "\n"
                                    "")
        self.comboBox.setEditable(True)
        self.comboBox.setMaxVisibleItems(10)
        self.comboBox.setFrame(False)
        self.comboBox.setObjectName("comboBox")
        self.horizontalLayout.addWidget(self.comboBox)
        self.verticalLayout.addWidget(self.horizontalFrame)
        self.horizontalFrame_3 = QtWidgets.QFrame(self.frame_3)
        self.horizontalFrame_3.setObjectName("horizontalFrame_3")
        self.gridLayout_3 = QtWidgets.QGridLayout(self.horizontalFrame_3)
        self.gridLayout_3.setObjectName("gridLayout_3")
        self.addActivityButton = QtWidgets.QPushButton(self.horizontalFrame_3)
        self.addActivityButton.setMinimumSize(QtCore.QSize(155, 0))
        font = QtGui.QFont()
        font.setFamily("Segoe UI Semibold")
        font.setPointSize(10)
        font.setBold(False)
        font.setItalic(False)
        font.setWeight(7)
        self.addActivityButton.setFont(font)
        self.addActivityButton.setStyleSheet("QPushButton {\n"
                                             "    background-color: rgb(81, 175, 81);\n"
                                             "    padding: 3px;\n"
                                             "    border: 4px solid rgb(81, 175, 81);\n"
                                             "    border-radius: 1px;\n"
                                             "    color: rgb(255, 255, 255);\n"
                                             "font: 63 10pt \"Segoe UI Semibold\";\n"
                                             "}\n"
                                             "\n"
                                             "\n"
                                             "\n"
                                             "QPushButton:Pressed{\n"
                                             "    background-color: rgb(55, 140, 55);\n"
                                             "    border-color: rgb(55, 140, 55);\n"
                                             "}")
        self.addActivityButton.setObjectName("addActivityButton")
        self.gridLayout_3.addWidget(self.addActivityButton, 0, 0, 1, 1)
        self.verticalLayout.addWidget(self.horizontalFrame_3, 0, QtCore.Qt.AlignRight)
        self.horizontalFrame_2 = QtWidgets.QFrame(self.frame_3)
        self.horizontalFrame_2.setObjectName("horizontalFrame_2")
        self.horizontalLayout_2 = QtWidgets.QHBoxLayout(self.horizontalFrame_2)
        self.horizontalLayout_2.setObjectName("horizontalLayout_2")
        self.label_3 = QtWidgets.QLabel(self.horizontalFrame_2)
        self.label_3.setMaximumSize(QtCore.QSize(16777215, 23))
        font = QtGui.QFont()
        font.setFamily("Segoe UI Semibold")
        font.setPointSize(10)
        font.setBold(True)
        font.setItalic(False)
        font.setWeight(75)
        self.label_3.setFont(font)
        self.label_3.setStyleSheet("color: rgb(255, 255, 255);\n"
                                   "font: 75 10pt \"Segoe UI Semibold\";\n"
                                   "font-weight:bold;")
        self.label_3.setObjectName("label_3")
        self.horizontalLayout_2.addWidget(self.label_3)
        self.length = QtWidgets.QLineEdit(self.horizontalFrame_2)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.length.sizePolicy().hasHeightForWidth())
        self.length.setSizePolicy(sizePolicy)
        self.length.setMaximumSize(QtCore.QSize(16777215, 20))
        self.length.setStyleSheet("background-color: rgb(225, 225, 225);\n"
                                  "font: 63 8pt \"Segoe UI Semibold\";")
        self.length.setFrame(False)
        self.length.setObjectName("length")
        self.horizontalLayout_2.addWidget(self.length)
        self.verticalLayout.addWidget(self.horizontalFrame_2)
        self.frame_2 = QtWidgets.QFrame(self.frame_3)
        self.frame_2.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.frame_2.setFrameShadow(QtWidgets.QFrame.Raised)
        self.frame_2.setObjectName("frame_2")
        self.horizontalLayout_3 = QtWidgets.QHBoxLayout(self.frame_2)
        self.horizontalLayout_3.setContentsMargins(9, 9, 9, 9)
        self.horizontalLayout_3.setSpacing(10)
        self.horizontalLayout_3.setObjectName("horizontalLayout_3")
        spacerItem = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout_3.addItem(spacerItem)
        self.saveButton = QtWidgets.QPushButton(self.frame_2)
        font = QtGui.QFont()
        font.setFamily("Segoe UI Semibold")
        font.setPointSize(10)
        font.setBold(False)
        font.setItalic(False)
        font.setWeight(7)
        self.saveButton.setFont(font)
        self.saveButton.setStyleSheet("QPushButton {\n"
                                      "    background-color: rgb(81, 175, 81);\n"
                                      "    padding: 3px;\n"
                                      "    border: 4px solid rgb(81, 175, 81);\n"
                                      "    border-radius: 1px;\n"
                                      "    color: rgb(255, 255, 255);\n"
                                      "font: 63 10pt \"Segoe UI Semibold\";\n"
                                      "}\n"
                                      "\n"
                                      "\n"
                                      "\n"
                                      "QPushButton:Pressed{\n"
                                      "    background-color: rgb(55, 140, 55);\n"
                                      "    border-color: rgb(55, 140, 55);\n"
                                      "}")
        self.saveButton.setObjectName("saveButton")
        self.horizontalLayout_3.addWidget(self.saveButton)
        spacerItem1 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout_3.addItem(spacerItem1)
        self.backButton = QtWidgets.QPushButton(self.frame_2)
        font = QtGui.QFont()
        font.setFamily("Segoe UI Semibold")
        font.setPointSize(10)
        font.setBold(False)
        font.setItalic(False)
        font.setWeight(7)
        self.backButton.setFont(font)
        self.backButton.setStyleSheet("QPushButton {\n"
                                      "    background-color: rgb(81, 175, 81);\n"
                                      "    padding: 3px;\n"
                                      "    border: 4px solid rgb(81, 175, 81);\n"
                                      "    border-radius: 1px;\n"
                                      "    color: rgb(255, 255, 255);\n"
                                      "font: 63 10pt \"Segoe UI Semibold\";\n"
                                      "}\n"
                                      "\n"
                                      "\n"
                                      "\n"
                                      "QPushButton:Pressed{\n"
                                      "    background-color: rgb(55, 140, 55);\n"
                                      "    border-color: rgb(55, 140, 55);\n"
                                      "}")
        self.backButton.setObjectName("backButton")
        self.horizontalLayout_3.addWidget(self.backButton)
        spacerItem2 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout_3.addItem(spacerItem2)
        self.horizontalLayout_3.setStretch(0, 1)
        self.horizontalLayout_3.setStretch(1, 3)
        self.horizontalLayout_3.setStretch(2, 1)
        self.horizontalLayout_3.setStretch(3, 3)
        self.horizontalLayout_3.setStretch(4, 1)
        self.verticalLayout.addWidget(self.frame_2)
        self.gridLayout_2.addWidget(self.frame_3, 0, 0, 1, 1)

        self.retranslateUi(Dialog)
        QtCore.QMetaObject.connectSlotsByName(Dialog)




        Dialog.setWindowIcon(QtGui.QIcon('icon.png'))
        # methods called when buttons pressed
        for activity in activities:
            self.comboBox.addItem(activity[1], activity[0])
        self.saveButton.clicked.connect(lambda: self.saveToDB(Dialog, parent))
        self.backButton.clicked.connect(lambda: self.goBack(Dialog, parent))
        self.addActivityButton.clicked.connect(lambda: self.addActivity(Dialog, parent))

    def retranslateUi(self, Dialog):
        _translate = QtCore.QCoreApplication.translate
        Dialog.setWindowTitle(_translate("Dialog", "Activity Inputs"))
        self.label_2.setText(_translate("Dialog", "Activity input"))
        self.label.setText(_translate("Dialog", "Exercise Type:"))
        self.addActivityButton.setText(_translate("Dialog", "+ Add New Activity"))
        self.label_3.setText(_translate("Dialog", "Exercise Length (mins):"))
        self.saveButton.setText(_translate("Dialog", "Save"))
        self.backButton.setText(_translate("Dialog", "Back"))

    # method to return to the previous page
    def goBack(self, Dialog, parent):
        Dialog.reject()
        parent.ActivityHistory()

    def addActivity(self, Dialog, parent):
        Dialog.reject()
        parent.addNewActivityWindow()

    # method to call controller method with the data to be saved to the database
    def saveToDB(self, Dialog, parent):
        activityID = self.comboBox.currentData()
        activityLength = self.length.text()

        try:
            val = int(activityLength)
        except:
            return

        if activityLength == '' or int(activityLength) <= 0:
            return
        Dialog.reject()
        parent.addActivityLog(activityID, activityLength)
        parent.ActivityHistory(False)