import datetime
from datetime import date

import PyQt5
import psycopg2
from PyQt5 import QtCore, QtWidgets, QtGui
from PyQt5.QtCore import QDate, QDateTime

from UI.diet_item import dietItemDialog
from Model.DietHistory import DietHistory
from View.calendarDialog import SetDateDialog

from PyQt5.QtGui import QPainter
from PyQt5.QtWidgets import *
from PyQt5.QtChart import *

import pyqtgraph as pg


class DietHistoryGUI(object):

    def __init__(self, dietHistoryController):
        self.controller = dietHistoryController
        self.currentSelectedList = None

    def setupUi(self, Dialog, launchFrom):
        Dialog.setObjectName("Dialog")
        Dialog.resize(1000, 527)
        Dialog.setMaximumSize(QtCore.QSize(1500, 1500))
        Dialog.setStyleSheet("background-color: rgb(255, 255, 255);")
        self.gridLayout = QtWidgets.QGridLayout(Dialog)
        self.gridLayout.setContentsMargins(0, 0, 0, 0)
        self.gridLayout.setObjectName("gridLayout")
        self.frame_2 = QtWidgets.QFrame(Dialog)
        self.frame_2.setFrameShape(QtWidgets.QFrame.NoFrame)
        self.frame_2.setFrameShadow(QtWidgets.QFrame.Raised)
        self.frame_2.setLineWidth(1)
        self.frame_2.setObjectName("frame_2")
        self.verticalLayout = QtWidgets.QVBoxLayout(self.frame_2)
        self.verticalLayout.setContentsMargins(0, 20, 0, 0)
        self.verticalLayout.setSpacing(6)
        self.verticalLayout.setObjectName("verticalLayout")
        self.frame_3 = QtWidgets.QFrame(self.frame_2)
        self.frame_3.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.frame_3.setFrameShadow(QtWidgets.QFrame.Raised)
        self.frame_3.setObjectName("frame_3")
        self.gridLayout_2 = QtWidgets.QGridLayout(self.frame_3)
        self.gridLayout_2.setHorizontalSpacing(24)
        self.gridLayout_2.setObjectName("gridLayout_2")
        self.dateLabel = QtWidgets.QLabel(self.frame_3)
        self.dateLabel.setMinimumSize(QtCore.QSize(0, 20))
        self.dateLabel.setMaximumSize(QtCore.QSize(16777215, 50))
        self.dateLabel.setStyleSheet("font: 63 10pt \"Segoe UI Semibold\";\n"
                                     "")
        self.dateLabel.setObjectName("dateLabel")
        self.gridLayout_2.addWidget(self.dateLabel, 1, 1, 1, 1)
        self.dateEdit = QtWidgets.QDateEdit(self.frame_3)
        self.dateEdit.setMinimumSize(QtCore.QSize(0, 20))
        self.dateEdit.setMaximumSize(QtCore.QSize(16777215, 50))
        self.dateEdit.setStyleSheet("QDateEdit{\n"
                                    "   background-color: rgb(225, 225, 225);\n"
                                    "\n"
                                    "    \n"
                                    "    font: 63 8pt \"Segoe UI Semibold\";\n"
                                    "\n"
                                    "}\n"
                                    "\n"
                                    "QDateEdit:up-button{\n"
                                    "    background-color: rgb(81, 175, 81);\n"
                                    "    padding: 1px;\n"
                                    "    border: 4px solid rgb(81, 175, 81);\n"
                                    "    color: rgb(255, 255, 255);\n"
                                    "}\n"
                                    "\n"
                                    "QDateEdit:up-button:Pressed{\n"
                                    "    background-color: rgb(55, 140, 55);\n"
                                    "    border-color: rgb(55, 140, 55);\n"
                                    "}\n"
                                    "\n"
                                    "QDateEdit:down-button{\n"
                                    "    background-color: rgb(81, 175, 81);\n"
                                    "    padding: 1px;\n"
                                    "    border: 4px solid rgb(81, 175, 81);\n"
                                    "    color: rgb(255, 255, 255);\n"
                                    "}\n"
                                    "\n"
                                    "QDateEdit:down-button:Pressed{\n"
                                    "    background-color: rgb(55, 140, 55);\n"
                                    "    border-color: rgb(55, 140, 55);\n"
                                    "}\n"
                                    "\n"
                                    "\n"
                                    "\n"
                                    "QDateEdit:up-arrow {\n"
                                    "width: 0; \n"
                                    "  height: 0; \n"
                                    "  border-left: 4px solid rgb(81, 175, 81);\n"
                                    "  border-right: 4px solid rgb(81, 175, 81);\n"
                                    "  border-bottom: 4px solid  rgb(255, 255, 255);\n"
                                    "}\n"
                                    "\n"
                                    "QDateEdit:down-arrow {\n"
                                    "width: 0; \n"
                                    "  height: 0; \n"
                                    "  border-left: 4px solid rgb(81, 175, 81);\n"
                                    "  border-right: 4px solid rgb(81, 175, 81);\n"
                                    "  border-top: 4px solid  rgb(255, 255, 255);\n"
                                    "}\n"
                                    "")
        self.dateEdit.setFrame(False)
        self.dateEdit.setObjectName("dateEdit")
        self.gridLayout_2.addWidget(self.dateEdit, 1, 2, 1, 1)
        self.chooseDate = QtWidgets.QPushButton(self.frame_3)
        self.chooseDate.setMaximumSize(QtCore.QSize(200, 16777215))
        self.chooseDate.setStyleSheet("QPushButton {\n"
                                      "    background-color: rgb(81, 175, 81);\n"
                                      "    padding: 3px;\n"
                                      "    border: 4px solid rgb(81, 175, 81);\n"
                                      "    border-radius: 1px;\n"
                                      "    color: rgb(255, 255, 255);\n"
                                      "font: 63 10pt \"Segoe UI Semibold\";\n"
                                      "}\n"
                                      "\n"
                                      "\n"
                                      "\n"
                                      "QPushButton:Pressed{\n"
                                      "    background-color: rgb(55, 140, 55);\n"
                                      "    border-color: rgb(55, 140, 55);\n"
                                      "}")
        self.chooseDate.setObjectName("chooseDate")
        self.gridLayout_2.addWidget(self.chooseDate, 1, 3, 1, 1)
        self.verticalLayout.addWidget(self.frame_3)
        self.scrollArea = QtWidgets.QScrollArea(self.frame_2)
        self.scrollArea.setFrameShape(QtWidgets.QFrame.NoFrame)
        self.scrollArea.setWidgetResizable(True)
        self.scrollArea.setObjectName("scrollArea")
        self.scrollAreaWidgetContents = QtWidgets.QWidget()
        self.scrollAreaWidgetContents.setGeometry(QtCore.QRect(0, 0, 1000, 354))
        self.scrollAreaWidgetContents.setObjectName("scrollAreaWidgetContents")
        self.horizontalLayout = QtWidgets.QHBoxLayout(self.scrollAreaWidgetContents)
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.frame = QtWidgets.QFrame(self.scrollAreaWidgetContents)
        self.frame.setMinimumSize(QtCore.QSize(0, 0))
        self.frame.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.frame.setFrameShadow(QtWidgets.QFrame.Raised)
        self.frame.setObjectName("frame")
        self.gridLayout_4 = QtWidgets.QGridLayout(self.frame)
        self.gridLayout_4.setObjectName("gridLayout_4")
        self.breakfastLabel = QtWidgets.QLabel(self.frame)
        self.breakfastLabel.setMaximumSize(QtCore.QSize(16777215, 15))
        self.breakfastLabel.setStyleSheet("font: 63 10pt \"Segoe UI Semibold\";\n"
                                          "")
        self.breakfastLabel.setObjectName("breakfastLabel")
        self.gridLayout_4.addWidget(self.breakfastLabel, 0, 0, 1, 1)
        self.breakfastList = QtWidgets.QListWidget(self.frame)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.breakfastList.sizePolicy().hasHeightForWidth())
        self.breakfastList.setSizePolicy(sizePolicy)
        self.breakfastList.setMinimumSize(QtCore.QSize(200, 200))
        self.breakfastList.setStyleSheet("background-color: rgb(255, 255, 255);\n"
                                         "background-color: rgb(225, 225, 225);")
        self.breakfastList.setObjectName("breakfastList")
        self.gridLayout_4.addWidget(self.breakfastList, 1, 0, 1, 1)
        self.horizontalLayout.addWidget(self.frame)
        self.frame_4 = QtWidgets.QFrame(self.scrollAreaWidgetContents)
        self.frame_4.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.frame_4.setFrameShadow(QtWidgets.QFrame.Raised)
        self.frame_4.setObjectName("frame_4")
        self.gridLayout_8 = QtWidgets.QGridLayout(self.frame_4)
        self.gridLayout_8.setObjectName("gridLayout_8")
        self.lunchLabel = QtWidgets.QLabel(self.frame_4)
        self.lunchLabel.setMaximumSize(QtCore.QSize(16777215, 15))
        self.lunchLabel.setStyleSheet("font: 63 10pt \"Segoe UI Semibold\";\n"
                                      "")
        self.lunchLabel.setObjectName("lunchLabel")
        self.gridLayout_8.addWidget(self.lunchLabel, 0, 0, 1, 1)
        self.lunchList = QtWidgets.QListWidget(self.frame_4)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.lunchList.sizePolicy().hasHeightForWidth())
        self.lunchList.setSizePolicy(sizePolicy)
        self.lunchList.setMinimumSize(QtCore.QSize(200, 200))
        self.lunchList.setStyleSheet("background-color: rgb(255, 255, 255);\n"
                                     "background-color: rgb(225, 225, 225);")
        self.lunchList.setObjectName("lunchList")
        self.gridLayout_8.addWidget(self.lunchList, 1, 0, 1, 1)
        self.horizontalLayout.addWidget(self.frame_4)
        self.frame_5 = QtWidgets.QFrame(self.scrollAreaWidgetContents)
        self.frame_5.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.frame_5.setFrameShadow(QtWidgets.QFrame.Raised)
        self.frame_5.setObjectName("frame_5")
        self.gridLayout_5 = QtWidgets.QGridLayout(self.frame_5)
        self.gridLayout_5.setObjectName("gridLayout_5")
        self.dinnerList = QtWidgets.QListWidget(self.frame_5)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.dinnerList.sizePolicy().hasHeightForWidth())
        self.dinnerList.setSizePolicy(sizePolicy)
        self.dinnerList.setMinimumSize(QtCore.QSize(200, 200))
        self.dinnerList.setStyleSheet("background-color: rgb(255, 255, 255);\n"
                                      "background-color: rgb(225, 225, 225);")
        self.dinnerList.setObjectName("dinnerList")
        self.gridLayout_5.addWidget(self.dinnerList, 1, 0, 1, 1)
        self.dinnerLabel = QtWidgets.QLabel(self.frame_5)
        self.dinnerLabel.setMaximumSize(QtCore.QSize(16777215, 15))
        self.dinnerLabel.setStyleSheet("font: 63 10pt \"Segoe UI Semibold\";\n"
                                       "")
        self.dinnerLabel.setObjectName("dinnerLabel")
        self.gridLayout_5.addWidget(self.dinnerLabel, 0, 0, 1, 1)
        self.horizontalLayout.addWidget(self.frame_5)
        self.frame_6 = QtWidgets.QFrame(self.scrollAreaWidgetContents)
        self.frame_6.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.frame_6.setFrameShadow(QtWidgets.QFrame.Raised)
        self.frame_6.setObjectName("frame_6")
        self.gridLayout_6 = QtWidgets.QGridLayout(self.frame_6)
        self.gridLayout_6.setObjectName("gridLayout_6")
        self.snackLabel = QtWidgets.QLabel(self.frame_6)
        self.snackLabel.setMaximumSize(QtCore.QSize(16777215, 15))
        self.snackLabel.setStyleSheet("font: 63 10pt \"Segoe UI Semibold\";")
        self.snackLabel.setObjectName("snackLabel")
        self.gridLayout_6.addWidget(self.snackLabel, 0, 0, 1, 1)
        self.snackList = QtWidgets.QListWidget(self.frame_6)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.snackList.sizePolicy().hasHeightForWidth())
        self.snackList.setSizePolicy(sizePolicy)
        self.snackList.setMinimumSize(QtCore.QSize(200, 200))
        self.snackList.setStyleSheet("background-color: rgb(255, 255, 255);\n"
                                     "background-color: rgb(225, 225, 225);")
        self.snackList.setObjectName("snackList")
        self.gridLayout_6.addWidget(self.snackList, 1, 0, 1, 1)
        self.horizontalLayout.addWidget(self.frame_6)
        self.scrollArea.setWidget(self.scrollAreaWidgetContents)
        self.verticalLayout.addWidget(self.scrollArea)
        self.frame_8 = QtWidgets.QFrame(self.frame_2)
        self.frame_8.setMinimumSize(QtCore.QSize(0, 90))
        self.frame_8.setStyleSheet("background-color: rgb(76, 76, 76);")
        self.frame_8.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.frame_8.setFrameShadow(QtWidgets.QFrame.Raised)
        self.frame_8.setObjectName("frame_8")
        self.gridLayout_7 = QtWidgets.QGridLayout(self.frame_8)
        self.gridLayout_7.setContentsMargins(10, -1, 10, -1)
        self.gridLayout_7.setHorizontalSpacing(20)
        self.gridLayout_7.setObjectName("gridLayout_7")
        self.editItem = QtWidgets.QPushButton(self.frame_8)
        self.editItem.setStyleSheet("QPushButton {\n"
                                    "    background-color: rgb(81, 175, 81);\n"
                                    "    padding: 3px;\n"
                                    "    border: 4px solid rgb(81, 175, 81);\n"
                                    "    border-radius: 1px;\n"
                                    "    color: rgb(255, 255, 255);\n"
                                    "font: 63 10pt \"Segoe UI Semibold\";\n"
                                    "}\n"
                                    "\n"
                                    "\n"
                                    "\n"
                                    "QPushButton:Pressed{\n"
                                    "    background-color: rgb(55, 140, 55);\n"
                                    "    border-color: rgb(55, 140, 55);\n"
                                    "}")
        self.editItem.setObjectName("editItem")
        self.gridLayout_7.addWidget(self.editItem, 2, 1, 1, 1)
        self.removeItem = QtWidgets.QPushButton(self.frame_8)
        self.removeItem.setStyleSheet("QPushButton {\n"
                                      "    background-color: rgb(81, 175, 81);\n"
                                      "    padding: 3px;\n"
                                      "    border: 4px solid rgb(81, 175, 81);\n"
                                      "    border-radius: 1px;\n"
                                      "    color: rgb(255, 255, 255);\n"
                                      "font: 63 10pt \"Segoe UI Semibold\";\n"
                                      "}\n"
                                      "\n"
                                      "\n"
                                      "\n"
                                      "QPushButton:Pressed{\n"
                                      "    background-color: rgb(55, 140, 55);\n"
                                      "    border-color: rgb(55, 140, 55);\n"
                                      "}")
        self.removeItem.setObjectName("removeItem")
        self.gridLayout_7.addWidget(self.removeItem, 2, 2, 1, 1)
        self.addItem = QtWidgets.QPushButton(self.frame_8)
        self.addItem.setStyleSheet("QPushButton {\n"
                                   "    background-color: rgb(81, 175, 81);\n"
                                   "    padding: 3px;\n"
                                   "    border: 4px solid rgb(81, 175, 81);\n"
                                   "    border-radius: 1px;\n"
                                   "    color: rgb(255, 255, 255);\n"
                                   "font: 63 10pt \"Segoe UI Semibold\";\n"
                                   "}\n"
                                   "\n"
                                   "\n"
                                   "\n"
                                   "QPushButton:Pressed{\n"
                                   "    background-color: rgb(55, 140, 55);\n"
                                   "    border-color: rgb(55, 140, 55);\n"
                                   "}")
        self.addItem.setObjectName("addItem")
        self.gridLayout_7.addWidget(self.addItem, 2, 0, 1, 1)
        self.verticalLayout.addWidget(self.frame_8)
        self.gridLayout.addWidget(self.frame_2, 0, 0, 1, 1)

        self.retranslateUi(Dialog)
        QtCore.QMetaObject.connectSlotsByName(Dialog)



        # UI init
        Dialog.setWindowIcon(QtGui.QIcon('icon.png'))
        self.breakfastList.setFocusPolicy(QtCore.Qt.ClickFocus)
        self.dateEdit.setDisplayFormat("dd/MM/yyyy")
        self.dateEdit.setDate(QDate.currentDate())
        self.breakfastList.itemClicked.connect(self.focused)
        self.lunchList.itemClicked.connect(self.focused)
        self.dinnerList.itemClicked.connect(self.focused)
        self.snackList.itemClicked.connect(self.focused)

        self.dateEdit.dateChanged.connect(self.onDateChanged)
        self.addItem.clicked.connect(lambda: self.onAddItem())
        self.editItem.clicked.connect(lambda: self.onEditItem())
        self.removeItem.clicked.connect(lambda: self.deleteItem())
        self.chooseDate.clicked.connect(lambda: self.showCalendar())
        self.updateList()

        if (launchFrom is True):
            self.frame_8.hide()
            self.addItem.hide()
            self.editItem.hide()
            self.removeItem.hide()

        # GRAPH TEST

        # chart = QChart()
        # series = QLineSeries()
        # series.append(0, 6)
        # series.append(2, 4)
        # series.append(3, 8)
        # series.append(7, 4)
        # series.append(10, 5)
        # chart.legend().hide()
        # chart.addSeries(series)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("Dialog", "Dialog"))
        self.dateLabel.setText(_translate("Dialog", "Date:"))
        self.chooseDate.setText(_translate("Dialog", "Choose Date From Calendar"))
        self.breakfastLabel.setText(_translate("Dialog", "Breakfast"))
        self.lunchLabel.setText(_translate("Dialog", "Lunch"))
        self.dinnerLabel.setText(_translate("Dialog", "Dinner"))
        self.snackLabel.setText(_translate("Dialog", "Snack"))
        self.editItem.setText(_translate("Dialog", "O Edit Item"))
        self.removeItem.setText(_translate("Dialog", "x Remove Item"))
        self.addItem.setText(_translate("Dialog", "+ Add Item"))


        MainWindow.setWindowTitle(_translate("Dialog", "Diet History"))



    def onAddItem(self):
        addItem = QtWidgets.QDialog()
        ui = dietItemDialog(self.controller, DietHistory.createEmptyDiet(), False)
        ui.setupUi(addItem)
        addItem.exec_()

    def onEditItem(self):

        editItem = QtWidgets.QDialog()
        diet = DietHistory.createEmptyDiet()
        if self.currentSelectedList == self.breakfastList:
            diet = self.controller.breakfastList[self.breakfastList.currentRow()]
        elif self.currentSelectedList == self.lunchList:
            diet = self.controller.lunchList[self.lunchList.currentRow()]
        elif self.currentSelectedList == self.dinnerList:
            diet = self.controller.dinnerList[self.dinnerList.currentRow()]
        elif self.currentSelectedList == self.snackList:
            diet = self.controller.snackList[self.snackList.currentRow()]
        else:
            return

        ui = dietItemDialog(self.controller, diet, True)
        ui.setupUi(editItem)
        ui.updateUI()
        editItem.exec_()

    def onDateChanged(self):
        print("Date changed")
        self.controller.initializeItemList()

    def showCalendar(self):
        calendarDialog = QtWidgets.QDialog()
        ui = SetDateDialog()
        ui.setupUi(calendarDialog)
        ui.setSelectedDateToUI(self.dateEdit.date().toString('dd-MM-yyyy'))
        if calendarDialog.exec_():
            qdate = QDate.fromString(ui.getSelectedDate().toString('dd-MM-yyyy'), "dd-MM-yyyy");
            self.dateEdit.setDate(qdate)

    def deleteItem(self):
        if self.currentSelectedList == self.breakfastList:
            self.controller.onRemoveItem(self.controller.breakfastList[self.breakfastList.currentRow()].dietID)
            self.controller.breakfastList.pop(self.breakfastList.currentRow())
            self.updateList()
        elif self.currentSelectedList == self.lunchList:
            self.controller.onRemoveItem(self.controller.lunchList[self.lunchList.currentRow()].dietID)
            self.controller.lunchList.pop(self.lunchList.currentRow())
            self.updateList()
        elif self.currentSelectedList == self.dinnerList:
            self.controller.onRemoveItem(self.controller.dinnerList[self.dinnerList.currentRow()].dietID)
            self.controller.dinnerList.pop(self.dinnerList.currentRow())
            self.updateList()
        elif self.currentSelectedList == self.snackList:
            self.controller.onRemoveItem(self.controller.snackList[self.snackList.currentRow()].dietID)
            self.controller.snackList.pop(self.snackList.currentRow())
            self.updateList()

    def focused(self, item):
        self.currentSelectedList = item.listWidget()
        print("Focused List: " + str(item.listWidget()))
        if item.listWidget() == self.breakfastList:
            print("Breakfast list is focused")
        if item.listWidget() == self.lunchList:
            print("Lunch list is focused")
        if item.listWidget() == self.dinnerList:
            print("Dinner list is focused")
        if item.listWidget() == self.snackList:
            print("Snack list is focused")

    def updateList(self):
        self.breakfastList.clear()
        self.lunchList.clear()
        self.dinnerList.clear()
        self.snackList.clear()
        for diet in self.controller.breakfastList:
            self.breakfastList.addItem(diet.toString())
        for diet in self.controller.lunchList:
            self.lunchList.addItem(diet.toString())
        for diet in self.controller.dinnerList:
            self.dinnerList.addItem(diet.toString())
        for diet in self.controller.snackList:
            self.snackList.addItem(diet.toString())

    def editListItem(self, index, changeString):
        item = self.controller.dietList.item(index)
        item.setText(changeString)
        print(item)
