# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'activityHistory_2withgraph.ui'
#
# Created by: PyQt5 UI code generator 5.14.2
#
# WARNING! All changes made in this file will be lost!


from PyQt5 import QtCore, QtGui, QtWidgets
from pyqtgraph import PlotWidget, plot
import pyqtgraph as pg


class activityHistoryGUI(object):
    def setupUi(self, Dialog, parent, launchFrom):
        Dialog.setObjectName("Dialog")
        Dialog.resize(489, 450)
        Dialog.setMinimumSize(QtCore.QSize(0, 0))
        Dialog.setMaximumSize(QtCore.QSize(560, 450))
        Dialog.setStyleSheet("background-color: rgb(255, 255, 255);")
        self.verticalLayout = QtWidgets.QVBoxLayout(Dialog)
        self.verticalLayout.setContentsMargins(0, 0, 0, 0)
        self.verticalLayout.setObjectName("verticalLayout")
        self.frame = QtWidgets.QFrame(Dialog)
        self.frame.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.frame.setFrameShadow(QtWidgets.QFrame.Raised)
        self.frame.setObjectName("frame")
        self.gridLayout = QtWidgets.QGridLayout(self.frame)
        self.gridLayout.setObjectName("gridLayout")
        self.label = QtWidgets.QLabel(self.frame)
        font = QtGui.QFont()
        font.setFamily("Segoe UI Semibold")
        font.setPointSize(15)
        font.setBold(True)
        font.setItalic(False)
        font.setWeight(75)
        self.label.setFont(font)
        self.label.setStyleSheet("font: 63 15pt \"Segoe UI Semibold\";\n"
                                 "font-weight:bold;\n"
                                 "\n"
                                 "")
        self.label.setObjectName("label")
        self.gridLayout.addWidget(self.label, 0, 0, 1, 1)
        self.verticalLayout.addWidget(self.frame)
        self.frame_3 = QtWidgets.QFrame(Dialog)
        self.frame_3.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.frame_3.setFrameShadow(QtWidgets.QFrame.Raised)
        self.frame_3.setObjectName("frame_3")
        self.gridLayout_2 = QtWidgets.QGridLayout(self.frame_3)
        self.gridLayout_2.setContentsMargins(-1, 0, -1, -1)
        self.gridLayout_2.setObjectName("gridLayout_2")


        self.graphWidget = pg.PlotWidget()


        self.graphWidget.setMinimumSize(QtCore.QSize(400, 250))
        self.graphWidget.setObjectName("graphWidget")
        self.gridLayout_2.addWidget(self.graphWidget, 0, 0, 1, 1)
        self.verticalLayout.addWidget(self.frame_3)
        self.frame_2 = QtWidgets.QFrame(Dialog)
        self.frame_2.setMinimumSize(QtCore.QSize(0, 80))
        self.frame_2.setStyleSheet("background-color: rgb(76, 76, 76);")
        self.frame_2.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.frame_2.setFrameShadow(QtWidgets.QFrame.Raised)
        self.frame_2.setObjectName("frame_2")
        self.horizontalLayout = QtWidgets.QHBoxLayout(self.frame_2)
        self.horizontalLayout.setObjectName("horizontalLayout")
        spacerItem = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout.addItem(spacerItem)
        self.addExcercise = QtWidgets.QPushButton(self.frame_2)
        font = QtGui.QFont()
        font.setFamily("Segoe UI Semibold")
        font.setPointSize(10)
        font.setBold(False)
        font.setItalic(False)
        font.setWeight(7)
        self.addExcercise.setFont(font)
        self.addExcercise.setStyleSheet("QPushButton {\n"
                                        "    background-color: rgb(81, 175, 81);\n"
                                        "    padding: 3px;\n"
                                        "    border: 4px solid rgb(81, 175, 81);\n"
                                        "    border-radius: 1px;\n"
                                        "    color: rgb(255, 255, 255);\n"
                                        "font: 63 10pt \"Segoe UI Semibold\";\n"
                                        "}\n"
                                        "\n"
                                        "\n"
                                        "\n"
                                        "QPushButton:Pressed{\n"
                                        "    background-color: rgb(55, 140, 55);\n"
                                        "    border-color: rgb(55, 140, 55);\n"
                                        "}")
        self.addExcercise.setObjectName("addExcercise")
        self.horizontalLayout.addWidget(self.addExcercise)
        spacerItem1 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout.addItem(spacerItem1)
        self.closeActivityHistory = QtWidgets.QPushButton(self.frame_2)
        font = QtGui.QFont()
        font.setFamily("Segoe UI Semibold")
        font.setPointSize(10)
        font.setBold(False)
        font.setItalic(False)
        font.setWeight(7)
        self.closeActivityHistory.setFont(font)
        self.closeActivityHistory.setStyleSheet("QPushButton {\n"
                                                "    background-color: rgb(81, 175, 81);\n"
                                                "    padding: 3px;\n"
                                                "    border: 4px solid rgb(81, 175, 81);\n"
                                                "    border-radius: 1px;\n"
                                                "    color: rgb(255, 255, 255);\n"
                                                "font: 63 10pt \"Segoe UI Semibold\";\n"
                                                "}\n"
                                                "\n"
                                                "\n"
                                                "\n"
                                                "QPushButton:Pressed{\n"
                                                "    background-color: rgb(55, 140, 55);\n"
                                                "    border-color: rgb(55, 140, 55);\n"
                                                "}")
        self.closeActivityHistory.setObjectName("closeActivityHistory")
        self.horizontalLayout.addWidget(self.closeActivityHistory)
        spacerItem2 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout.addItem(spacerItem2)
        self.horizontalLayout.setStretch(0, 1)
        self.horizontalLayout.setStretch(1, 2)
        self.horizontalLayout.setStretch(2, 1)
        self.horizontalLayout.setStretch(3, 2)
        self.horizontalLayout.setStretch(4, 1)
        self.verticalLayout.addWidget(self.frame_2)

        self.retranslateUi(Dialog)
        QtCore.QMetaObject.connectSlotsByName(Dialog)




        Dialog.setWindowIcon(QtGui.QIcon('icon.png'))
    # methods called when buttons pressed
        self.addExcercise.clicked.connect(lambda: self.addActivity(Dialog, parent))
        self.closeActivityHistory.clicked.connect(lambda: self.closeActivities(Dialog))

    # graph widget
    #     self.graphWidget = pg.PlotWidget()
        self.graphWidget.setObjectName("graph")
        self.gridLayout_2.addWidget(self.graphWidget, 0, 0, 1, 1)
        points = []

        self.graphWidget.setBackground((255, 255, 255))
        # graphWidget.setTitle("kcals burned per day")
        self.graphWidget.setTitle(
            "<span style=\"color:rgb(76,76,76);font: 63 10pt Segoe UI Semibold;text-decoration: underline;\">Kcals Burned Per Day</span>")
        # graphWidget.setLabel("bottom", "days active")
        self.graphWidget.setLabel("bottom",
                             "<span style=\"color:rgb(76,76,76);font: bold 9pt Segoe UI;\">Days Active</span>")
        # graphWidget.setLabel("left", "calories burned (kcals)")
        self.graphWidget.setLabel("left",
                             "<span style=\"color:rgb(76,76,76);font: bold 9pt Segoe UI;\">Calories Burned (kcals)</span>")
        # function to get data points in the controller
        data = parent.getActivityDailyCalsBurned()
        self.graphWidget.setRange(xRange=[0, len(data) - 1], yRange=([0, max(data)]), padding=0)
        for point in range(len(data)):
            points.append(point)
        pen = pg.mkPen(color=(32, 159, 223), width=2)
        axisPen = pg.mkPen(color=(76, 76, 76), width=1)
        self.graphWidget.getAxis('bottom').setPen(axisPen)
        self.graphWidget.getAxis('left').setPen(axisPen)
        self.graphWidget.plot(points, data, pen=pen)

        if (launchFrom is True):
            self.addExcercise.hide()
            self.closeActivityHistory.hide()
            self.frame_2.hide()

    # method to change pages to the page to add activities
    def addActivity(self, Dialog, parent):
        Dialog.reject()
        parent.addActivityLogWindow()

    # calls controller method to close the activity pages
    def closeActivities(self, Dialog):
        Dialog.reject()

    def retranslateUi(self, Dialog):
        _translate = QtCore.QCoreApplication.translate
        Dialog.setWindowTitle(_translate("Dialog", "Activity Summary"))
        self.label.setText(_translate("Dialog", "Activity Summary"))
        self.addExcercise.setText(_translate("Dialog", "+ Add Excercise"))
        self.closeActivityHistory.setText(_translate("Dialog", "Back"))