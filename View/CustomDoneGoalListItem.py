import sys
from PyQt5.QtWidgets import QApplication, QWidget, QListWidget, QVBoxLayout, QLabel, QPushButton, QListWidgetItem, \
    QHBoxLayout
from PyQt5 import QtWidgets, QtCore, QtGui


class CustomDoneGoalItemWidget(QWidget):
    def __init__(self, parent=None):
        super(CustomDoneGoalItemWidget, self).__init__(parent)

        self.setupUi(parent)

        # label = QLabel("I am a custom widget")
        #
        # button = QPushButton("A useless button")
        #
        # layout = QHBoxLayout()
        # layout.addWidget(label)
        # layout.addWidget(button)
        #
        #
        #
        # self.setLayout(layout)

    def setupUi(self, parent):
        self.frame = QtWidgets.QFrame()
        self.frame.setGeometry(QtCore.QRect(20, 30, 271, 151))
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.frame.sizePolicy().hasHeightForWidth())
        self.frame.setSizePolicy(sizePolicy)
        self.frame.setMinimumSize(QtCore.QSize(211, 121))
        self.frame.setStyleSheet("\n"
                                 "QFrame#frame\n"
                                 "{\n"
                                 "border: 1px solid rgb(76, 76, 76);\n"
                                 "background-color: rgb(255, 255, 255);\n"
                                 "}")
        self.frame.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.frame.setFrameShadow(QtWidgets.QFrame.Raised)
        self.frame.setObjectName("frame")
        self.verticalLayout = QtWidgets.QVBoxLayout(self.frame)
        self.verticalLayout.setObjectName("verticalLayout")
        self.GoalNameText = QtWidgets.QLabel(self.frame)
        self.GoalNameText.setMinimumSize(QtCore.QSize(0, 20))
        font = QtGui.QFont()
        font.setFamily("Segoe UI Semibold")
        font.setPointSize(15)
        font.setBold(True)
        font.setItalic(False)
        font.setWeight(75)
        self.GoalNameText.setFont(font)
        self.GoalNameText.setStyleSheet("font: 63 15pt \"Segoe UI Semibold\";\n"
                                        "font-weight:bold;\n"
                                        "background-color: rgb(255, 255, 255);")
        self.GoalNameText.setAlignment(QtCore.Qt.AlignCenter)
        self.GoalNameText.setObjectName("GoalNameText")
        self.verticalLayout.addWidget(self.GoalNameText)
        self.GoalTypeText = QtWidgets.QLabel(self.frame)
        self.GoalTypeText.setStyleSheet("font: 63 10pt \"Segoe UI Semibold\";\n"
                                        "font-weight:bold;\n"
                                        "background-color: rgb(255, 255, 255);")
        self.GoalTypeText.setObjectName("GoalTypeText")
        self.verticalLayout.addWidget(self.GoalTypeText)
        self.SetDateText = QtWidgets.QLabel(self.frame)
        self.SetDateText.setStyleSheet("font: 63 10pt \"Segoe UI Semibold\";\n"
                                       "font-weight:bold;\n"
                                       "background-color: rgb(255, 255, 255);")
        self.SetDateText.setObjectName("SetDateText")
        self.verticalLayout.addWidget(self.SetDateText)
        self.EndDateText = QtWidgets.QLabel(self.frame)
        self.EndDateText.setStyleSheet("font: 63 10pt \"Segoe UI Semibold\";\n"
                                       "font-weight:bold;\n"
                                       "background-color: rgb(255, 255, 255);")
        self.EndDateText.setObjectName("EndDateText")
        self.verticalLayout.addWidget(self.EndDateText)
        self.StartValueText = QtWidgets.QLabel(self.frame)
        self.StartValueText.setStyleSheet("font: 63 10pt \"Segoe UI Semibold\";\n"
                                          "font-weight:bold;\n"
                                          "background-color: rgb(255, 255, 255);")
        self.StartValueText.setObjectName("StartValueText")
        self.verticalLayout.addWidget(self.StartValueText)
        self.GoalValueText = QtWidgets.QLabel(self.frame)
        self.GoalValueText.setStyleSheet("font: 63 10pt \"Segoe UI Semibold\";\n"
                                         "font-weight:bold;\n"
                                         "background-color: rgb(255, 255, 255);")
        self.GoalValueText.setObjectName("GoalValueText")
        self.verticalLayout.addWidget(self.GoalValueText)
        self.GoalAchievedText = QtWidgets.QLabel(self.frame)
        self.GoalAchievedText.setStyleSheet("font: 63 10pt \"Segoe UI Semibold\";\n"
                                            "font-weight:bold;\n"
                                            "background-color: rgb(255, 255, 255);")
        self.GoalAchievedText.setObjectName("GoalAchievedText")
        self.verticalLayout.addWidget(self.GoalAchievedText)

        self.retranslateUi(parent)
        layout = QHBoxLayout()
        layout.addWidget(self.frame)
        self.setLayout(layout)
        QtCore.QMetaObject.connectSlotsByName(parent)

    def retranslateUi(self, Parent):
        _translate = QtCore.QCoreApplication.translate
        self.GoalNameText.setText(_translate("Form", "Goal Name"))
        self.GoalTypeText.setText(_translate("Form", "Goal Type"))
        self.SetDateText.setText(_translate("Form", "Set Date"))
        self.EndDateText.setText(_translate("Form", "End Date"))
        self.StartValueText.setText(_translate("Form", "Start Value:"))
        self.GoalValueText.setText(_translate("Form", "Goal Value:"))
        self.GoalAchievedText.setText(_translate("Form", "Goal Achieved:"))


