/* SQL to create table to hold the activities */
CREATE TABLE activities
(
	activityID SERIAL PRIMARY KEY,
	activityName VARCHAR(30) NOT NULL UNIQUE,
	kcalsPerMin INT NOT NULL
);

/* SQL to create table to hold activity data of users */
CREATE TABLE userActivities
(
	userID INT NOT NULL,
	activityID INT NOT NULL,
	activityLength INT NOT NULL,
	dateDone DATE,
	timeDone TIME,
	PRIMARY KEY (userID, activityID, dateDone, timeDone),
	FOREIGN KEY (userID) REFERENCES users (id)
		ON DELETE CASCADE
		ON UPDATE CASCADE,
	FOREIGN KEY (activityID) REFERENCES activities
		ON DELETE CASCADE
		ON UPDATE CASCADE
);

/* creates a view that can be used to show users relevant data */
CREATE VIEW userView AS
(
	SELECT userID, activities.activityName, activityLength, dateDone, timeDone, (activityLength*activities.kcalspermin) AS kcalsBurned
	FROM userActivities LEFT JOIN activities ON userActivities.activityID = activities.activityID
	ORDER BY dateDone, timeDone
);

/* data to go into the activities table */
INSERT INTO activities (activityName, kcalsPerMin) VALUES ('walking', '3');
INSERT INTO activities (activityName, kcalsPerMin) VALUES ('running', '12');
INSERT INTO activities (activityName, kcalsPerMin) VALUES ('cycling', '7');
INSERT INTO activities (activityName, kcalsPerMin) VALUES ('swimming', '6');
INSERT INTO activities (activityName, kcalsPerMin) VALUES ('badminton', '6');
INSERT INTO activities (activityName, kcalsPerMin) VALUES ('baseball', '5');
INSERT INTO activities (activityName, kcalsPerMin) VALUES ('basketball', '7');
INSERT INTO activities (activityName, kcalsPerMin) VALUES ('bowling', '4');
INSERT INTO activities (activityName, kcalsPerMin) VALUES ('golf', '5');
INSERT INTO activities (activityName, kcalsPerMin) VALUES ('gymnastics', '4');
INSERT INTO activities (activityName, kcalsPerMin) VALUES ('hockey', '8');
INSERT INTO activities (activityName, kcalsPerMin) VALUES ('lacrosse', '8');
INSERT INTO activities (activityName, kcalsPerMin) VALUES ('football', '7');
INSERT INTO activities (activityName, kcalsPerMin) VALUES ('table tennis', '4');
INSERT INTO activities (activityName, kcalsPerMin) VALUES ('tennis', '8');
INSERT INTO activities (activityName, kcalsPerMin) VALUES ('volleyball', '4');
INSERT INTO activities (activityName, kcalsPerMin) VALUES ('rugby', '7');

/* test data for a user */
INSERT INTO users (id, firstName, surname, email, password, dob, weight, height)
VALUES (1, 'Norman', 'Brownstains', 'NBrownS@hotmail.com', 'insecurePassword', date '1985/06/18', 35, 165);


INSERT INTO userActivities (userID, activityID, activityLength, dateDone, timeDone)
VALUES (1, 1, 120, date '2019/06/25', time '11:30:06');