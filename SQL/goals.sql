--
-- PostgreSQL database dump
--

-- Dumped from database version 12.2
-- Dumped by pg_dump version 12.2

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: goals; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.goals (
    id serial not null
        constraint goals_pkey
        primary key,
    userID character varying NOT NULL,
    setDate date NOT NULL,
    goalName character varying NOT NULL,
    endDate date NOT NULL,
    StartValue numeric NOT NULL,
    CurrentValue numeric NOT NULL,
    goalValue numeric NOT NULL,
    goalType integer NOT NULL
);


ALTER TABLE public.goals OWNER TO postgres;

--
-- Name: goals goals_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--



--
-- PostgreSQL database dump complete
--

