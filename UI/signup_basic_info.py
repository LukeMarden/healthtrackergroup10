# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'signup_basic_info.ui'
#
# Created by: PyQt5 UI code generator 5.14.1
#
# WARNING! All changes made in this file will be lost!


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_Dialog(object):
    def setupUi(self, Dialog):
        Dialog.setObjectName("Dialog")
        Dialog.resize(406, 210)
        Dialog.setMinimumSize(QtCore.QSize(406, 210))
        Dialog.setMaximumSize(QtCore.QSize(406, 210))
        Dialog.setLayoutDirection(QtCore.Qt.LeftToRight)
        self.buttonBox = QtWidgets.QDialogButtonBox(Dialog)
        self.buttonBox.setGeometry(QtCore.QRect(-10, 170, 341, 32))
        self.buttonBox.setOrientation(QtCore.Qt.Horizontal)
        self.buttonBox.setStandardButtons(QtWidgets.QDialogButtonBox.Cancel|QtWidgets.QDialogButtonBox.Ok)
        self.buttonBox.setObjectName("buttonBox")
        self.firstname = QtWidgets.QLineEdit(Dialog)
        self.firstname.setGeometry(QtCore.QRect(50, 50, 131, 21))
        self.firstname.setObjectName("firstname")
        self.email = QtWidgets.QLineEdit(Dialog)
        self.email.setGeometry(QtCore.QRect(50, 80, 281, 21))
        self.email.setInputMethodHints(QtCore.Qt.ImhNone)
        self.email.setText("")
        self.email.setEchoMode(QtWidgets.QLineEdit.Normal)
        self.email.setObjectName("email")
        self.password_1 = QtWidgets.QLineEdit(Dialog)
        self.password_1.setGeometry(QtCore.QRect(50, 110, 281, 21))
        self.password_1.setInputMethodHints(QtCore.Qt.ImhHiddenText|QtCore.Qt.ImhNoAutoUppercase|QtCore.Qt.ImhNoPredictiveText|QtCore.Qt.ImhSensitiveData)
        self.password_1.setText("")
        self.password_1.setEchoMode(QtWidgets.QLineEdit.Password)
        self.password_1.setObjectName("password_1")
        self.password_2 = QtWidgets.QLineEdit(Dialog)
        self.password_2.setGeometry(QtCore.QRect(50, 140, 281, 21))
        self.password_2.setInputMethodHints(QtCore.Qt.ImhHiddenText|QtCore.Qt.ImhNoAutoUppercase|QtCore.Qt.ImhNoPredictiveText|QtCore.Qt.ImhSensitiveData)
        self.password_2.setText("")
        self.password_2.setEchoMode(QtWidgets.QLineEdit.Password)
        self.password_2.setObjectName("password_2")
        self.surname = QtWidgets.QLineEdit(Dialog)
        self.surname.setGeometry(QtCore.QRect(200, 50, 131, 21))
        self.surname.setInputMethodHints(QtCore.Qt.ImhNone)
        self.surname.setText("")
        self.surname.setEchoMode(QtWidgets.QLineEdit.Normal)
        self.surname.setObjectName("surname")
        self.label = QtWidgets.QLabel(Dialog)
        self.label.setGeometry(QtCore.QRect(30, 20, 311, 16))
        font = QtGui.QFont()
        font.setPointSize(14)
        self.label.setFont(font)
        self.label.setTextFormat(QtCore.Qt.RichText)
        self.label.setAlignment(QtCore.Qt.AlignCenter)
        self.label.setObjectName("label")

        self.retranslateUi(Dialog)
        self.buttonBox.accepted.connect(Dialog.accept)
        self.buttonBox.rejected.connect(Dialog.reject)
        QtCore.QMetaObject.connectSlotsByName(Dialog)

    def retranslateUi(self, Dialog):
        _translate = QtCore.QCoreApplication.translate
        Dialog.setWindowTitle(_translate("Dialog", "Dialog"))
        self.firstname.setPlaceholderText(_translate("Dialog", "First Name"))
        self.email.setPlaceholderText(_translate("Dialog", "Email"))
        self.password_1.setPlaceholderText(_translate("Dialog", "Password"))
        self.password_2.setPlaceholderText(_translate("Dialog", "Please enter password again"))
        self.surname.setPlaceholderText(_translate("Dialog", "Surname"))
        self.label.setText(_translate("Dialog", "<html><head/><body><p><span style=\" font-weight:600; text-decoration: underline;\">Please enter your details into the form:</span></p></body></html>"))
