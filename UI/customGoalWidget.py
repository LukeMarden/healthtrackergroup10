# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'ui/customGoalWidget.ui'
#
# Created by: PyQt5 UI code generator 5.14.1
#
# WARNING! All changes made in this file will be lost!


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_Form(object):
    def setupUi(self, Form):
        Form.setObjectName("Form")
        Form.resize(400, 300)
        self.frame = QtWidgets.QFrame(Form)
        self.frame.setGeometry(QtCore.QRect(70, 90, 211, 121))
        self.frame.setMinimumSize(QtCore.QSize(211, 121))
        self.frame.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.frame.setFrameShadow(QtWidgets.QFrame.Raised)
        self.frame.setObjectName("frame")
        self.verticalLayout = QtWidgets.QVBoxLayout(self.frame)
        self.verticalLayout.setObjectName("verticalLayout")
        self.GoalNameText = QtWidgets.QLabel(self.frame)
        font = QtGui.QFont()
        font.setPointSize(11)
        self.GoalNameText.setFont(font)
        self.GoalNameText.setAlignment(QtCore.Qt.AlignCenter)
        self.GoalNameText.setObjectName("GoalNameText")
        self.verticalLayout.addWidget(self.GoalNameText)
        self.GoalTypeText = QtWidgets.QLabel(self.frame)
        self.GoalTypeText.setObjectName("GoalTypeText")
        self.verticalLayout.addWidget(self.GoalTypeText)
        self.EndDateText = QtWidgets.QLabel(self.frame)
        self.EndDateText.setObjectName("EndDateText")
        self.verticalLayout.addWidget(self.EndDateText)
        self.EndDateText_2 = QtWidgets.QLabel(self.frame)
        self.EndDateText_2.setObjectName("EndDateText_2")
        self.verticalLayout.addWidget(self.EndDateText_2)
        self.GoalProgressBar = QtWidgets.QProgressBar(self.frame)
        self.GoalProgressBar.setStyleSheet("\n"
"QProgressBar {\n"
"    border: 2px solid grey;\n"
"    border-radius: 5px;\n"
"text-align: center;\n"
"}\n"
"\n"
"QProgressBar::chunk {\n"
"    background-color: rgb(0, 203, 0);\n"
"    width: 20px;\n"
"}")
        self.GoalProgressBar.setProperty("value", 24)
        self.GoalProgressBar.setOrientation(QtCore.Qt.Horizontal)
        self.GoalProgressBar.setInvertedAppearance(False)
        self.GoalProgressBar.setObjectName("GoalProgressBar")
        self.verticalLayout.addWidget(self.GoalProgressBar)

        self.retranslateUi(Form)
        QtCore.QMetaObject.connectSlotsByName(Form)

    def retranslateUi(self, Form):
        _translate = QtCore.QCoreApplication.translate
        Form.setWindowTitle(_translate("Form", "Form"))
        self.GoalNameText.setText(_translate("Form", "Goal Name"))
        self.GoalTypeText.setText(_translate("Form", "Goal Type"))
        self.EndDateText.setText(_translate("Form", "End Date"))
        self.EndDateText_2.setText(_translate("Form", "Progress:"))
