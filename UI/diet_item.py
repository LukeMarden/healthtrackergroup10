# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'diet_item.ui'
#
# Created by: PyQt5 UI code generator 5.14.1
#
# WARNING! All changes made in this file will be lost!


from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtCore import QDate

from Model.DietHistory import DietHistory


class dietItemDialog(object):

    def __init__(self, dietHistoryController, dietItem, isEdit):
        self.controller = dietHistoryController
        self.dietItem = dietItem
        self.isEdit = isEdit


    def setupUi(self, Dialog):
        Dialog.setObjectName("Dialog")
        Dialog.resize(451, 705)
        Dialog.setMaximumSize(QtCore.QSize(600, 750))
        Dialog.setStyleSheet("background-color: rgb(76, 76, 76);")
        self.gridLayout = QtWidgets.QGridLayout(Dialog)
        self.gridLayout.setObjectName("gridLayout")
        self.frame = QtWidgets.QFrame(Dialog)
        self.frame.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.frame.setFrameShadow(QtWidgets.QFrame.Raised)
        self.frame.setObjectName("frame")
        self.gridLayout_2 = QtWidgets.QGridLayout(self.frame)
        self.gridLayout_2.setVerticalSpacing(0)
        self.gridLayout_2.setObjectName("gridLayout_2")
        self.frame_6 = QtWidgets.QFrame(self.frame)
        self.frame_6.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.frame_6.setFrameShadow(QtWidgets.QFrame.Raised)
        self.frame_6.setObjectName("frame_6")
        self.gridLayout_7 = QtWidgets.QGridLayout(self.frame_6)
        self.gridLayout_7.setObjectName("gridLayout_7")
        spacerItem = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.gridLayout_7.addItem(spacerItem, 1, 0, 1, 1)
        spacerItem1 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.gridLayout_7.addItem(spacerItem1, 1, 2, 1, 1)
        self.buttonBox = QtWidgets.QDialogButtonBox(self.frame_6)
        font = QtGui.QFont()
        font.setFamily("Segoe UI Semibold")
        font.setBold(True)
        font.setWeight(75)
        self.buttonBox.setFont(font)
        self.buttonBox.setStyleSheet("QPushButton {\n"
                                     "    background-color: rgb(81, 175, 81);\n"
                                     "    padding: 2px;\n"
                                     "    border: 4px solid rgb(81, 175, 81);\n"
                                     "    border-radius: 1px;\n"
                                     "    color: rgb(255, 255, 255);\n"
                                     "    padding: 0 8px 0 8px;\n"
                                     "    min-width: 50px;\n"
                                     "}\n"
                                     "\n"
                                     "\n"
                                     "\n"
                                     "QPushButton:Pressed{\n"
                                     "    background-color: rgb(55, 140, 55);\n"
                                     "    border-color: rgb(55, 140, 55);\n"
                                     "}")
        self.buttonBox.setStandardButtons(QtWidgets.QDialogButtonBox.Cancel | QtWidgets.QDialogButtonBox.Ok)
        self.buttonBox.setObjectName("buttonBox")
        self.gridLayout_7.addWidget(self.buttonBox, 1, 1, 1, 1)
        self.line_3 = QtWidgets.QFrame(self.frame_6)
        self.line_3.setStyleSheet("background-color: rgb(255, 255, 255);")
        self.line_3.setFrameShape(QtWidgets.QFrame.HLine)
        self.line_3.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.line_3.setObjectName("line_3")
        self.gridLayout_7.addWidget(self.line_3, 0, 0, 1, 3)
        self.gridLayout_2.addWidget(self.frame_6, 7, 0, 1, 1)
        self.frame_8 = QtWidgets.QFrame(self.frame)
        self.frame_8.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.frame_8.setFrameShadow(QtWidgets.QFrame.Raised)
        self.frame_8.setObjectName("frame_8")
        self.gridLayout_8 = QtWidgets.QGridLayout(self.frame_8)
        self.gridLayout_8.setObjectName("gridLayout_8")
        self.line_2 = QtWidgets.QFrame(self.frame_8)
        self.line_2.setStyleSheet("background-color: rgb(255, 255, 255);")
        self.line_2.setFrameShape(QtWidgets.QFrame.HLine)
        self.line_2.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.line_2.setObjectName("line_2")
        self.gridLayout_8.addWidget(self.line_2, 1, 0, 1, 1)
        self.label_3 = QtWidgets.QLabel(self.frame_8)
        self.label_3.setMaximumSize(QtCore.QSize(16777215, 40))
        font = QtGui.QFont()
        font.setFamily("Segoe UI Semibold")
        font.setPointSize(11)
        font.setBold(True)
        font.setWeight(75)
        self.label_3.setFont(font)
        self.label_3.setStyleSheet("color: rgb(255, 255, 255);")
        self.label_3.setObjectName("label_3")
        self.gridLayout_8.addWidget(self.label_3, 0, 0, 1, 1)
        self.frame_4 = QtWidgets.QFrame(self.frame_8)
        self.frame_4.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.frame_4.setFrameShadow(QtWidgets.QFrame.Raised)
        self.frame_4.setObjectName("frame_4")
        self.gridLayout_6 = QtWidgets.QGridLayout(self.frame_4)
        self.gridLayout_6.setObjectName("gridLayout_6")
        self.label_5 = QtWidgets.QLabel(self.frame_4)
        font = QtGui.QFont()
        font.setFamily("Segoe UI Semibold")
        font.setBold(True)
        font.setWeight(75)
        self.label_5.setFont(font)
        self.label_5.setStyleSheet("color: rgb(255, 255, 255);")
        self.label_5.setObjectName("label_5")
        self.gridLayout_6.addWidget(self.label_5, 3, 0, 1, 1)
        self.itemCal = QtWidgets.QDoubleSpinBox(self.frame_4)
        self.itemCal.setStyleSheet("QDoubleSpinBox{\n"
                                   "   background-color: rgb(225, 225, 225);\n"
                                   "\n"
                                   "    \n"
                                   "    font: 63 8pt \"Segoe UI Semibold\";\n"
                                   "\n"
                                   "}\n"
                                   "\n"
                                   "QDoubleSpinBox:up-button{\n"
                                   "    background-color: rgb(81, 175, 81);\n"
                                   "    padding: 1px;\n"
                                   "    border: 4px solid rgb(81, 175, 81);\n"
                                   "    color: rgb(255, 255, 255);\n"
                                   "}\n"
                                   "\n"
                                   "QDoubleSpinBox:up-button:Pressed{\n"
                                   "    background-color: rgb(55, 140, 55);\n"
                                   "    border-color: rgb(55, 140, 55);\n"
                                   "}\n"
                                   "\n"
                                   "QDoubleSpinBox:down-button{\n"
                                   "    background-color: rgb(81, 175, 81);\n"
                                   "    padding: 1px;\n"
                                   "    border: 4px solid rgb(81, 175, 81);\n"
                                   "    color: rgb(255, 255, 255);\n"
                                   "}\n"
                                   "\n"
                                   "QDoubleSpinBox:down-button:Pressed{\n"
                                   "    background-color: rgb(55, 140, 55);\n"
                                   "    border-color: rgb(55, 140, 55);\n"
                                   "}\n"
                                   "\n"
                                   "\n"
                                   "\n"
                                   "QDoubleSpinBox:up-arrow {\n"
                                   "width: 0; \n"
                                   "  height: 0; \n"
                                   "  border-left: 4px solid rgb(81, 175, 81);\n"
                                   "  border-right: 4px solid rgb(81, 175, 81);\n"
                                   "  border-bottom: 4px solid  rgb(255, 255, 255);\n"
                                   "}\n"
                                   "\n"
                                   "QDoubleSpinBox:down-arrow {\n"
                                   "width: 0; \n"
                                   "  height: 0; \n"
                                   "  border-left: 4px solid rgb(81, 175, 81);\n"
                                   "  border-right: 4px solid rgb(81, 175, 81);\n"
                                   "  border-top: 4px solid  rgb(255, 255, 255);\n"
                                   "}\n"
                                   "")
        self.itemCal.setFrame(False)
        self.itemCal.setMaximum(10000.0)
        self.itemCal.setSingleStep(15.0)
        self.itemCal.setObjectName("itemCal")
        self.gridLayout_6.addWidget(self.itemCal, 2, 1, 1, 1)
        self.label_4 = QtWidgets.QLabel(self.frame_4)
        font = QtGui.QFont()
        font.setFamily("Segoe UI Semibold")
        font.setBold(True)
        font.setWeight(75)
        self.label_4.setFont(font)
        self.label_4.setStyleSheet("color: rgb(255, 255, 255);")
        self.label_4.setObjectName("label_4")
        self.gridLayout_6.addWidget(self.label_4, 2, 0, 1, 1)
        self.itemCarb = QtWidgets.QDoubleSpinBox(self.frame_4)
        self.itemCarb.setStyleSheet("QDoubleSpinBox{\n"
                                    "   background-color: rgb(225, 225, 225);\n"
                                    "\n"
                                    "    \n"
                                    "    font: 63 8pt \"Segoe UI Semibold\";\n"
                                    "\n"
                                    "}\n"
                                    "\n"
                                    "QDoubleSpinBox:up-button{\n"
                                    "    background-color: rgb(81, 175, 81);\n"
                                    "    padding: 1px;\n"
                                    "    border: 4px solid rgb(81, 175, 81);\n"
                                    "    color: rgb(255, 255, 255);\n"
                                    "}\n"
                                    "\n"
                                    "QDoubleSpinBox:up-button:Pressed{\n"
                                    "    background-color: rgb(55, 140, 55);\n"
                                    "    border-color: rgb(55, 140, 55);\n"
                                    "}\n"
                                    "\n"
                                    "QDoubleSpinBox:down-button{\n"
                                    "    background-color: rgb(81, 175, 81);\n"
                                    "    padding: 1px;\n"
                                    "    border: 4px solid rgb(81, 175, 81);\n"
                                    "    color: rgb(255, 255, 255);\n"
                                    "}\n"
                                    "\n"
                                    "QDoubleSpinBox:down-button:Pressed{\n"
                                    "    background-color: rgb(55, 140, 55);\n"
                                    "    border-color: rgb(55, 140, 55);\n"
                                    "}\n"
                                    "\n"
                                    "\n"
                                    "\n"
                                    "QDoubleSpinBox:up-arrow {\n"
                                    "width: 0; \n"
                                    "  height: 0; \n"
                                    "  border-left: 4px solid rgb(81, 175, 81);\n"
                                    "  border-right: 4px solid rgb(81, 175, 81);\n"
                                    "  border-bottom: 4px solid  rgb(255, 255, 255);\n"
                                    "}\n"
                                    "\n"
                                    "QDoubleSpinBox:down-arrow {\n"
                                    "width: 0; \n"
                                    "  height: 0; \n"
                                    "  border-left: 4px solid rgb(81, 175, 81);\n"
                                    "  border-right: 4px solid rgb(81, 175, 81);\n"
                                    "  border-top: 4px solid  rgb(255, 255, 255);\n"
                                    "}\n"
                                    "")
        self.itemCarb.setFrame(False)
        self.itemCarb.setMaximum(1000.0)
        self.itemCarb.setObjectName("itemCarb")
        self.gridLayout_6.addWidget(self.itemCarb, 4, 1, 1, 1)
        self.itemPro = QtWidgets.QDoubleSpinBox(self.frame_4)
        self.itemPro.setStyleSheet("QDoubleSpinBox{\n"
                                   "   background-color: rgb(225, 225, 225);\n"
                                   "\n"
                                   "    \n"
                                   "    font: 63 8pt \"Segoe UI Semibold\";\n"
                                   "\n"
                                   "}\n"
                                   "\n"
                                   "QDoubleSpinBox:up-button{\n"
                                   "    background-color: rgb(81, 175, 81);\n"
                                   "    padding: 1px;\n"
                                   "    border: 4px solid rgb(81, 175, 81);\n"
                                   "    color: rgb(255, 255, 255);\n"
                                   "}\n"
                                   "\n"
                                   "QDoubleSpinBox:up-button:Pressed{\n"
                                   "    background-color: rgb(55, 140, 55);\n"
                                   "    border-color: rgb(55, 140, 55);\n"
                                   "}\n"
                                   "\n"
                                   "QDoubleSpinBox:down-button{\n"
                                   "    background-color: rgb(81, 175, 81);\n"
                                   "    padding: 1px;\n"
                                   "    border: 4px solid rgb(81, 175, 81);\n"
                                   "    color: rgb(255, 255, 255);\n"
                                   "}\n"
                                   "\n"
                                   "QDoubleSpinBox:down-button:Pressed{\n"
                                   "    background-color: rgb(55, 140, 55);\n"
                                   "    border-color: rgb(55, 140, 55);\n"
                                   "}\n"
                                   "\n"
                                   "\n"
                                   "\n"
                                   "QDoubleSpinBox:up-arrow {\n"
                                   "width: 0; \n"
                                   "  height: 0; \n"
                                   "  border-left: 4px solid rgb(81, 175, 81);\n"
                                   "  border-right: 4px solid rgb(81, 175, 81);\n"
                                   "  border-bottom: 4px solid  rgb(255, 255, 255);\n"
                                   "}\n"
                                   "\n"
                                   "QDoubleSpinBox:down-arrow {\n"
                                   "width: 0; \n"
                                   "  height: 0; \n"
                                   "  border-left: 4px solid rgb(81, 175, 81);\n"
                                   "  border-right: 4px solid rgb(81, 175, 81);\n"
                                   "  border-top: 4px solid  rgb(255, 255, 255);\n"
                                   "}\n"
                                   "")
        self.itemPro.setFrame(False)
        self.itemPro.setMaximum(1000.0)
        self.itemPro.setObjectName("itemPro")
        self.gridLayout_6.addWidget(self.itemPro, 5, 1, 1, 1)
        self.label_6 = QtWidgets.QLabel(self.frame_4)
        font = QtGui.QFont()
        font.setFamily("Segoe UI Semibold")
        font.setBold(True)
        font.setWeight(75)
        self.label_6.setFont(font)
        self.label_6.setStyleSheet("color: rgb(255, 255, 255);")
        self.label_6.setObjectName("label_6")
        self.gridLayout_6.addWidget(self.label_6, 4, 0, 1, 1)
        self.label_7 = QtWidgets.QLabel(self.frame_4)
        font = QtGui.QFont()
        font.setFamily("Segoe UI Semibold")
        font.setBold(True)
        font.setWeight(75)
        self.label_7.setFont(font)
        self.label_7.setStyleSheet("color: rgb(255, 255, 255);")
        self.label_7.setObjectName("label_7")
        self.gridLayout_6.addWidget(self.label_7, 5, 0, 1, 1)
        self.itemFat = QtWidgets.QDoubleSpinBox(self.frame_4)
        self.itemFat.setStyleSheet("QDoubleSpinBox{\n"
                                   "   background-color: rgb(225, 225, 225);\n"
                                   "\n"
                                   "    \n"
                                   "    font: 63 8pt \"Segoe UI Semibold\";\n"
                                   "\n"
                                   "}\n"
                                   "\n"
                                   "QDoubleSpinBox:up-button{\n"
                                   "    background-color: rgb(81, 175, 81);\n"
                                   "    padding: 1px;\n"
                                   "    border: 4px solid rgb(81, 175, 81);\n"
                                   "    color: rgb(255, 255, 255);\n"
                                   "}\n"
                                   "\n"
                                   "QDoubleSpinBox:up-button:Pressed{\n"
                                   "    background-color: rgb(55, 140, 55);\n"
                                   "    border-color: rgb(55, 140, 55);\n"
                                   "}\n"
                                   "\n"
                                   "QDoubleSpinBox:down-button{\n"
                                   "    background-color: rgb(81, 175, 81);\n"
                                   "    padding: 1px;\n"
                                   "    border: 4px solid rgb(81, 175, 81);\n"
                                   "    color: rgb(255, 255, 255);\n"
                                   "}\n"
                                   "\n"
                                   "QDoubleSpinBox:down-button:Pressed{\n"
                                   "    background-color: rgb(55, 140, 55);\n"
                                   "    border-color: rgb(55, 140, 55);\n"
                                   "}\n"
                                   "\n"
                                   "\n"
                                   "\n"
                                   "QDoubleSpinBox:up-arrow {\n"
                                   "width: 0; \n"
                                   "  height: 0; \n"
                                   "  border-left: 4px solid rgb(81, 175, 81);\n"
                                   "  border-right: 4px solid rgb(81, 175, 81);\n"
                                   "  border-bottom: 4px solid  rgb(255, 255, 255);\n"
                                   "}\n"
                                   "\n"
                                   "QDoubleSpinBox:down-arrow {\n"
                                   "width: 0; \n"
                                   "  height: 0; \n"
                                   "  border-left: 4px solid rgb(81, 175, 81);\n"
                                   "  border-right: 4px solid rgb(81, 175, 81);\n"
                                   "  border-top: 4px solid  rgb(255, 255, 255);\n"
                                   "}\n"
                                   "")
        self.itemFat.setFrame(False)
        self.itemFat.setMaximum(1000.0)
        self.itemFat.setObjectName("itemFat")
        self.gridLayout_6.addWidget(self.itemFat, 3, 1, 1, 1)
        self.gridLayout_8.addWidget(self.frame_4, 2, 0, 1, 1)
        self.gridLayout_2.addWidget(self.frame_8, 1, 0, 2, 1)
        self.frame_14 = QtWidgets.QFrame(self.frame)
        self.frame_14.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.frame_14.setFrameShadow(QtWidgets.QFrame.Raised)
        self.frame_14.setObjectName("frame_14")
        self.gridLayout_45 = QtWidgets.QGridLayout(self.frame_14)
        self.gridLayout_45.setVerticalSpacing(0)
        self.gridLayout_45.setObjectName("gridLayout_45")
        self.frame_11 = QtWidgets.QFrame(self.frame_14)
        self.frame_11.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.frame_11.setFrameShadow(QtWidgets.QFrame.Raised)
        self.frame_11.setObjectName("frame_11")
        self.gridLayout_12 = QtWidgets.QGridLayout(self.frame_11)
        self.gridLayout_12.setObjectName("gridLayout_12")
        self.label_10 = QtWidgets.QLabel(self.frame_11)
        font = QtGui.QFont()
        font.setFamily("Segoe UI Semibold")
        font.setPointSize(10)
        font.setBold(True)
        font.setWeight(75)
        self.label_10.setFont(font)
        self.label_10.setStyleSheet("color: rgb(255, 255, 255);")
        self.label_10.setObjectName("label_10")
        self.gridLayout_12.addWidget(self.label_10, 0, 0, 1, 1)
        self.line_5 = QtWidgets.QFrame(self.frame_11)
        self.line_5.setStyleSheet("background-color: rgb(255, 255, 255);")
        self.line_5.setFrameShape(QtWidgets.QFrame.HLine)
        self.line_5.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.line_5.setObjectName("line_5")
        self.gridLayout_12.addWidget(self.line_5, 1, 0, 1, 1)
        self.frame_13 = QtWidgets.QFrame(self.frame_11)
        self.frame_13.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.frame_13.setFrameShadow(QtWidgets.QFrame.Raised)
        self.frame_13.setObjectName("frame_13")
        self.gridLayout_14 = QtWidgets.QGridLayout(self.frame_13)
        self.gridLayout_14.setObjectName("gridLayout_14")
        self.label_12 = QtWidgets.QLabel(self.frame_13)
        font = QtGui.QFont()
        font.setFamily("Segoe UI Semibold")
        font.setBold(True)
        font.setWeight(75)
        self.label_12.setFont(font)
        self.label_12.setStyleSheet("color: rgb(255, 255, 255);")
        self.label_12.setObjectName("label_12")
        self.gridLayout_14.addWidget(self.label_12, 0, 0, 1, 1)
        self.itemVol = QtWidgets.QDoubleSpinBox(self.frame_13)
        self.itemVol.setStyleSheet("QDoubleSpinBox{\n"
                                   "   background-color: rgb(225, 225, 225);\n"
                                   "\n"
                                   "    \n"
                                   "    font: 63 8pt \"Segoe UI Semibold\";\n"
                                   "\n"
                                   "}\n"
                                   "\n"
                                   "QDoubleSpinBox:up-button{\n"
                                   "    background-color: rgb(81, 175, 81);\n"
                                   "    padding: 1px;\n"
                                   "    border: 4px solid rgb(81, 175, 81);\n"
                                   "    color: rgb(255, 255, 255);\n"
                                   "}\n"
                                   "\n"
                                   "QDoubleSpinBox:up-button:Pressed{\n"
                                   "    background-color: rgb(55, 140, 55);\n"
                                   "    border-color: rgb(55, 140, 55);\n"
                                   "}\n"
                                   "\n"
                                   "QDoubleSpinBox:down-button{\n"
                                   "    background-color: rgb(81, 175, 81);\n"
                                   "    padding: 1px;\n"
                                   "    border: 4px solid rgb(81, 175, 81);\n"
                                   "    color: rgb(255, 255, 255);\n"
                                   "}\n"
                                   "\n"
                                   "QDoubleSpinBox:down-button:Pressed{\n"
                                   "    background-color: rgb(55, 140, 55);\n"
                                   "    border-color: rgb(55, 140, 55);\n"
                                   "}\n"
                                   "\n"
                                   "\n"
                                   "\n"
                                   "QDoubleSpinBox:up-arrow {\n"
                                   "width: 0; \n"
                                   "  height: 0; \n"
                                   "  border-left: 4px solid rgb(81, 175, 81);\n"
                                   "  border-right: 4px solid rgb(81, 175, 81);\n"
                                   "  border-bottom: 4px solid  rgb(255, 255, 255);\n"
                                   "}\n"
                                   "\n"
                                   "QDoubleSpinBox:down-arrow {\n"
                                   "width: 0; \n"
                                   "  height: 0; \n"
                                   "  border-left: 4px solid rgb(81, 175, 81);\n"
                                   "  border-right: 4px solid rgb(81, 175, 81);\n"
                                   "  border-top: 4px solid  rgb(255, 255, 255);\n"
                                   "}\n"
                                   "")
        self.itemVol.setFrame(False)
        self.itemVol.setMaximum(10000.0)
        self.itemVol.setSingleStep(25.0)
        self.itemVol.setObjectName("itemVol")
        self.gridLayout_14.addWidget(self.itemVol, 0, 1, 1, 1)
        self.label_13 = QtWidgets.QLabel(self.frame_13)
        font = QtGui.QFont()
        font.setFamily("Segoe UI Semibold")
        font.setBold(True)
        font.setWeight(75)
        self.label_13.setFont(font)
        self.label_13.setStyleSheet("color: rgb(255, 255, 255);")
        self.label_13.setObjectName("label_13")
        self.gridLayout_14.addWidget(self.label_13, 1, 0, 1, 1)
        self.itemPercent = QtWidgets.QDoubleSpinBox(self.frame_13)
        self.itemPercent.setStyleSheet("QDoubleSpinBox{\n"
                                       "   background-color: rgb(225, 225, 225);\n"
                                       "\n"
                                       "    \n"
                                       "    font: 63 8pt \"Segoe UI Semibold\";\n"
                                       "\n"
                                       "}\n"
                                       "\n"
                                       "QDoubleSpinBox:up-button{\n"
                                       "    background-color: rgb(81, 175, 81);\n"
                                       "    padding: 1px;\n"
                                       "    border: 4px solid rgb(81, 175, 81);\n"
                                       "    color: rgb(255, 255, 255);\n"
                                       "}\n"
                                       "\n"
                                       "QDoubleSpinBox:up-button:Pressed{\n"
                                       "    background-color: rgb(55, 140, 55);\n"
                                       "    border-color: rgb(55, 140, 55);\n"
                                       "}\n"
                                       "\n"
                                       "QDoubleSpinBox:down-button{\n"
                                       "    background-color: rgb(81, 175, 81);\n"
                                       "    padding: 1px;\n"
                                       "    border: 4px solid rgb(81, 175, 81);\n"
                                       "    color: rgb(255, 255, 255);\n"
                                       "}\n"
                                       "\n"
                                       "QDoubleSpinBox:down-button:Pressed{\n"
                                       "    background-color: rgb(55, 140, 55);\n"
                                       "    border-color: rgb(55, 140, 55);\n"
                                       "}\n"
                                       "\n"
                                       "\n"
                                       "\n"
                                       "QDoubleSpinBox:up-arrow {\n"
                                       "width: 0; \n"
                                       "  height: 0; \n"
                                       "  border-left: 4px solid rgb(81, 175, 81);\n"
                                       "  border-right: 4px solid rgb(81, 175, 81);\n"
                                       "  border-bottom: 4px solid  rgb(255, 255, 255);\n"
                                       "}\n"
                                       "\n"
                                       "QDoubleSpinBox:down-arrow {\n"
                                       "width: 0; \n"
                                       "  height: 0; \n"
                                       "  border-left: 4px solid rgb(81, 175, 81);\n"
                                       "  border-right: 4px solid rgb(81, 175, 81);\n"
                                       "  border-top: 4px solid  rgb(255, 255, 255);\n"
                                       "}\n"
                                       "")
        self.itemPercent.setFrame(False)
        self.itemPercent.setSingleStep(1.0)
        self.itemPercent.setObjectName("itemPercent")
        self.gridLayout_14.addWidget(self.itemPercent, 1, 1, 1, 1)
        self.gridLayout_12.addWidget(self.frame_13, 2, 0, 1, 1)
        self.gridLayout_45.addWidget(self.frame_11, 8, 0, 1, 1)
        self.frame_2 = QtWidgets.QFrame(self.frame_14)
        self.frame_2.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.frame_2.setFrameShadow(QtWidgets.QFrame.Raised)
        self.frame_2.setObjectName("frame_2")
        self.gridLayout_3 = QtWidgets.QGridLayout(self.frame_2)
        self.gridLayout_3.setObjectName("gridLayout_3")
        self.itemMeal = QtWidgets.QComboBox(self.frame_2)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.itemMeal.sizePolicy().hasHeightForWidth())
        self.itemMeal.setSizePolicy(sizePolicy)
        self.itemMeal.setMaximumSize(QtCore.QSize(16777215, 20))
        self.itemMeal.setStyleSheet("QComboBox{\n"
                                    "   background-color: rgb(225, 225, 225);\n"
                                    "font: 63 8pt \"Segoe UI Semibold\";\n"
                                    "\n"
                                    "}\n"
                                    "\n"
                                    "QComboBox:drop-down{\n"
                                    "    background-color: rgb(81, 175, 81);\n"
                                    "    padding: 1px;\n"
                                    "    border: 4px solid rgb(81, 175, 81);\n"
                                    "    border-radius: 1px;\n"
                                    "    color: rgb(255, 255, 255);\n"
                                    "}\n"
                                    "\n"
                                    "QComboBox:drop-down:Pressed{\n"
                                    "    background-color: rgb(55, 140, 55);\n"
                                    "    border-color: rgb(55, 140, 55);\n"
                                    "}\n"
                                    "\n"
                                    "\n"
                                    "QComboBox:down-arrow {\n"
                                    "width: 0; \n"
                                    "  height: 0; \n"
                                    "  border-left: 5px solid rgb(81, 175, 81);\n"
                                    "  border-right: 5px solid rgb(81, 175, 81);\n"
                                    "  border-top: 5px solid  rgb(255, 255, 255);\n"
                                    "    \n"
                                    "\n"
                                    "}\n"
                                    "\n"
                                    "QComboBox QAbstractItemView {\n"
                                    "background-color: rgb(225, 225, 225);\n"
                                    "    outline: none;\n"
                                    "}\n"
                                    "\n"
                                    "\n"
                                    "\n"
                                    "\n"
                                    "QPushButton:Pressed{\n"
                                    "    background-color: rgb(55, 140, 55);\n"
                                    "    border-color: rgb(55, 140, 55);\n"
                                    "}\n"
                                    "\n"
                                    "")
        self.itemMeal.setEditable(True)
        self.itemMeal.setFrame(False)
        self.itemMeal.setObjectName("itemMeal")
        self.itemMeal.addItem("")
        self.itemMeal.addItem("")
        self.itemMeal.addItem("")
        self.itemMeal.addItem("")
        self.gridLayout_3.addWidget(self.itemMeal, 2, 0, 1, 1)
        self.label = QtWidgets.QLabel(self.frame_2)
        font = QtGui.QFont()
        font.setFamily("Segoe UI Semibold")
        font.setBold(True)
        font.setWeight(75)
        self.label.setFont(font)
        self.label.setStyleSheet("color: rgb(255, 255, 255);")
        self.label.setObjectName("label")
        self.gridLayout_3.addWidget(self.label, 1, 0, 1, 1)
        self.gridLayout_45.addWidget(self.frame_2, 5, 0, 1, 1)
        self.frame_9 = QtWidgets.QFrame(self.frame_14)
        self.frame_9.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.frame_9.setFrameShadow(QtWidgets.QFrame.Raised)
        self.frame_9.setObjectName("frame_9")
        self.gridLayout_10 = QtWidgets.QGridLayout(self.frame_9)
        self.gridLayout_10.setObjectName("gridLayout_10")
        self.label_8 = QtWidgets.QLabel(self.frame_9)
        font = QtGui.QFont()
        font.setFamily("Segoe UI Semibold")
        font.setBold(True)
        font.setWeight(75)
        self.label_8.setFont(font)
        self.label_8.setStyleSheet("color: rgb(255, 255, 255);")
        self.label_8.setObjectName("label_8")
        self.gridLayout_10.addWidget(self.label_8, 0, 0, 1, 1)
        self.itemFood = QtWidgets.QComboBox(self.frame_9)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.itemFood.sizePolicy().hasHeightForWidth())
        self.itemFood.setSizePolicy(sizePolicy)
        self.itemFood.setMaximumSize(QtCore.QSize(16777215, 20))
        self.itemFood.setStyleSheet("QComboBox{\n"
                                    "   background-color: rgb(225, 225, 225);\n"
                                    "font: 63 8pt \"Segoe UI Semibold\";\n"
                                    "\n"
                                    "}\n"
                                    "\n"
                                    "QComboBox:drop-down{\n"
                                    "    background-color: rgb(81, 175, 81);\n"
                                    "    padding: 1px;\n"
                                    "    border: 4px solid rgb(81, 175, 81);\n"
                                    "    border-radius: 1px;\n"
                                    "    color: rgb(255, 255, 255);\n"
                                    "}\n"
                                    "\n"
                                    "QComboBox:drop-down:Pressed{\n"
                                    "    background-color: rgb(55, 140, 55);\n"
                                    "    border-color: rgb(55, 140, 55);\n"
                                    "}\n"
                                    "\n"
                                    "\n"
                                    "QComboBox:down-arrow {\n"
                                    "width: 0; \n"
                                    "  height: 0; \n"
                                    "  border-left: 5px solid rgb(81, 175, 81);\n"
                                    "  border-right: 5px solid rgb(81, 175, 81);\n"
                                    "  border-top: 5px solid  rgb(255, 255, 255);\n"
                                    "    \n"
                                    "\n"
                                    "}\n"
                                    "\n"
                                    "QComboBox QAbstractItemView {\n"
                                    "background-color: rgb(225, 225, 225);\n"
                                    "    outline: none;\n"
                                    "}\n"
                                    "\n"
                                    "\n"
                                    "\n"
                                    "\n"
                                    "QPushButton:Pressed{\n"
                                    "    background-color: rgb(55, 140, 55);\n"
                                    "    border-color: rgb(55, 140, 55);\n"
                                    "}\n"
                                    "\n"
                                    "")
        self.itemFood.setEditable(True)
        self.itemFood.setFrame(False)
        self.itemFood.setObjectName("itemFood")
        self.itemFood.addItem("")
        self.itemFood.addItem("")
        self.gridLayout_10.addWidget(self.itemFood, 1, 0, 1, 1)
        spacerItem2 = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Maximum)
        self.gridLayout_10.addItem(spacerItem2, 2, 0, 1, 1)
        self.gridLayout_45.addWidget(self.frame_9, 6, 0, 1, 1)
        self.frame_3 = QtWidgets.QFrame(self.frame_14)
        self.frame_3.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.frame_3.setFrameShadow(QtWidgets.QFrame.Raised)
        self.frame_3.setObjectName("frame_3")
        self.gridLayout_4 = QtWidgets.QGridLayout(self.frame_3)
        self.gridLayout_4.setObjectName("gridLayout_4")
        self.label_2 = QtWidgets.QLabel(self.frame_3)
        font = QtGui.QFont()
        font.setFamily("Segoe UI Semibold")
        font.setBold(True)
        font.setWeight(75)
        self.label_2.setFont(font)
        self.label_2.setStyleSheet("color: rgb(255, 255, 255);")
        self.label_2.setObjectName("label_2")
        self.gridLayout_4.addWidget(self.label_2, 1, 0, 1, 1)
        self.itemName = QtWidgets.QLineEdit(self.frame_3)
        self.itemName.setStyleSheet("background-color: rgb(225, 225, 225);\n"
                                    "font: 63 8pt \"Segoe UI Semibold\";")
        self.itemName.setText("")
        self.itemName.setFrame(False)
        self.itemName.setObjectName("itemName")
        self.gridLayout_4.addWidget(self.itemName, 2, 0, 1, 1)
        self.gridLayout_45.addWidget(self.frame_3, 4, 0, 1, 1)
        self.frame_10 = QtWidgets.QFrame(self.frame_14)
        self.frame_10.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.frame_10.setFrameShadow(QtWidgets.QFrame.Raised)
        self.frame_10.setObjectName("frame_10")
        self.gridLayout_11 = QtWidgets.QGridLayout(self.frame_10)
        self.gridLayout_11.setObjectName("gridLayout_11")
        self.frame_12 = QtWidgets.QFrame(self.frame_10)
        self.frame_12.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.frame_12.setFrameShadow(QtWidgets.QFrame.Raised)
        self.frame_12.setObjectName("frame_12")
        self.gridLayout_13 = QtWidgets.QGridLayout(self.frame_12)
        self.gridLayout_13.setObjectName("gridLayout_13")
        self.label_11 = QtWidgets.QLabel(self.frame_12)
        font = QtGui.QFont()
        font.setFamily("Segoe UI Semibold")
        font.setBold(True)
        font.setWeight(75)
        self.label_11.setFont(font)
        self.label_11.setStyleSheet("color: rgb(255, 255, 255);")
        self.label_11.setObjectName("label_11")
        self.gridLayout_13.addWidget(self.label_11, 0, 0, 1, 1)
        self.itemWeights = QtWidgets.QDoubleSpinBox(self.frame_12)
        self.itemWeights.setStyleSheet("QDoubleSpinBox{\n"
                                       "   background-color: rgb(225, 225, 225);\n"
                                       "\n"
                                       "    \n"
                                       "    font: 63 8pt \"Segoe UI Semibold\";\n"
                                       "\n"
                                       "}\n"
                                       "\n"
                                       "QDoubleSpinBox:up-button{\n"
                                       "    background-color: rgb(81, 175, 81);\n"
                                       "    padding: 1px;\n"
                                       "    border: 4px solid rgb(81, 175, 81);\n"
                                       "    color: rgb(255, 255, 255);\n"
                                       "}\n"
                                       "\n"
                                       "QDoubleSpinBox:up-button:Pressed{\n"
                                       "    background-color: rgb(55, 140, 55);\n"
                                       "    border-color: rgb(55, 140, 55);\n"
                                       "}\n"
                                       "\n"
                                       "QDoubleSpinBox:down-button{\n"
                                       "    background-color: rgb(81, 175, 81);\n"
                                       "    padding: 1px;\n"
                                       "    border: 4px solid rgb(81, 175, 81);\n"
                                       "    color: rgb(255, 255, 255);\n"
                                       "}\n"
                                       "\n"
                                       "QDoubleSpinBox:down-button:Pressed{\n"
                                       "    background-color: rgb(55, 140, 55);\n"
                                       "    border-color: rgb(55, 140, 55);\n"
                                       "}\n"
                                       "\n"
                                       "\n"
                                       "\n"
                                       "QDoubleSpinBox:up-arrow {\n"
                                       "width: 0; \n"
                                       "  height: 0; \n"
                                       "  border-left: 4px solid rgb(81, 175, 81);\n"
                                       "  border-right: 4px solid rgb(81, 175, 81);\n"
                                       "  border-bottom: 4px solid  rgb(255, 255, 255);\n"
                                       "}\n"
                                       "\n"
                                       "QDoubleSpinBox:down-arrow {\n"
                                       "width: 0; \n"
                                       "  height: 0; \n"
                                       "  border-left: 4px solid rgb(81, 175, 81);\n"
                                       "  border-right: 4px solid rgb(81, 175, 81);\n"
                                       "  border-top: 4px solid  rgb(255, 255, 255);\n"
                                       "}\n"
                                       "")
        self.itemWeights.setFrame(False)
        self.itemWeights.setMaximum(10000.0)
        self.itemWeights.setSingleStep(25.0)
        self.itemWeights.setObjectName("itemWeights")
        self.gridLayout_13.addWidget(self.itemWeights, 0, 1, 1, 1)
        self.gridLayout_11.addWidget(self.frame_12, 6, 0, 1, 2)
        self.label_9 = QtWidgets.QLabel(self.frame_10)
        font = QtGui.QFont()
        font.setFamily("Segoe UI Semibold")
        font.setPointSize(10)
        font.setBold(True)
        font.setWeight(75)
        self.label_9.setFont(font)
        self.label_9.setStyleSheet("color: rgb(255, 255, 255);")
        self.label_9.setObjectName("label_9")
        self.gridLayout_11.addWidget(self.label_9, 4, 0, 1, 1)
        self.line_4 = QtWidgets.QFrame(self.frame_10)
        self.line_4.setStyleSheet("background-color: rgb(255, 255, 255);")
        self.line_4.setFrameShape(QtWidgets.QFrame.HLine)
        self.line_4.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.line_4.setObjectName("line_4")
        self.gridLayout_11.addWidget(self.line_4, 5, 0, 1, 2)
        self.gridLayout_45.addWidget(self.frame_10, 7, 0, 1, 1)
        self.frame_5 = QtWidgets.QFrame(self.frame_14)
        self.frame_5.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.frame_5.setFrameShadow(QtWidgets.QFrame.Raised)
        self.frame_5.setObjectName("frame_5")
        self.gridLayout_5 = QtWidgets.QGridLayout(self.frame_5)
        self.gridLayout_5.setObjectName("gridLayout_5")
        self.itemDate = QtWidgets.QDateEdit(self.frame_5)
        self.itemDate.setStyleSheet("QDateEdit{\n"
                                    "   background-color: rgb(225, 225, 225);\n"
                                    "\n"
                                    "    \n"
                                    "    font: 63 8pt \"Segoe UI Semibold\";\n"
                                    "\n"
                                    "}\n"
                                    "\n"
                                    "QDateEdit:up-button{\n"
                                    "    background-color: rgb(81, 175, 81);\n"
                                    "    padding: 1px;\n"
                                    "    border: 4px solid rgb(81, 175, 81);\n"
                                    "    color: rgb(255, 255, 255);\n"
                                    "}\n"
                                    "\n"
                                    "QDateEdit:up-button:Pressed{\n"
                                    "    background-color: rgb(55, 140, 55);\n"
                                    "    border-color: rgb(55, 140, 55);\n"
                                    "}\n"
                                    "\n"
                                    "QDateEdit:down-button{\n"
                                    "    background-color: rgb(81, 175, 81);\n"
                                    "    padding: 1px;\n"
                                    "    border: 4px solid rgb(81, 175, 81);\n"
                                    "    color: rgb(255, 255, 255);\n"
                                    "}\n"
                                    "\n"
                                    "QDateEdit:down-button:Pressed{\n"
                                    "    background-color: rgb(55, 140, 55);\n"
                                    "    border-color: rgb(55, 140, 55);\n"
                                    "}\n"
                                    "\n"
                                    "\n"
                                    "\n"
                                    "QDateEdit:up-arrow {\n"
                                    "width: 0; \n"
                                    "  height: 0; \n"
                                    "  border-left: 4px solid rgb(81, 175, 81);\n"
                                    "  border-right: 4px solid rgb(81, 175, 81);\n"
                                    "  border-bottom: 4px solid  rgb(255, 255, 255);\n"
                                    "}\n"
                                    "\n"
                                    "QDateEdit:down-arrow {\n"
                                    "width: 0; \n"
                                    "  height: 0; \n"
                                    "  border-left: 4px solid rgb(81, 175, 81);\n"
                                    "  border-right: 4px solid rgb(81, 175, 81);\n"
                                    "  border-top: 4px solid  rgb(255, 255, 255);\n"
                                    "}\n"
                                    "")
        self.itemDate.setFrame(False)
        self.itemDate.setObjectName("itemDate")
        self.itemDate.setDisplayFormat("dd/MM/yyyy")
        self.gridLayout_5.addWidget(self.itemDate, 1, 1, 1, 1)
        self.dateLabel = QtWidgets.QLabel(self.frame_5)
        font = QtGui.QFont()
        font.setFamily("Segoe UI Semibold")
        font.setBold(True)
        font.setWeight(75)
        self.dateLabel.setFont(font)
        self.dateLabel.setStyleSheet("color: rgb(255, 255, 255);")
        self.dateLabel.setObjectName("dateLabel")
        self.gridLayout_5.addWidget(self.dateLabel, 0, 1, 1, 1)
        self.gridLayout_45.addWidget(self.frame_5, 3, 0, 1, 1)
        self.gridLayout_2.addWidget(self.frame_14, 0, 0, 1, 1)
        self.gridLayout.addWidget(self.frame, 0, 0, 1, 1)

        self.retranslateUi(Dialog)
        QtCore.QMetaObject.connectSlotsByName(Dialog)

        #UI init

        # self.buttonBox.accepted.connect(lambda: DietHistoryController.onAddItem(self))
        if self.isEdit:
            self.buttonBox.accepted.connect(self.editItem)
        else:
            self.buttonBox.accepted.connect(self.addItem)
        self.buttonBox.accepted.connect(Dialog.accept)
        self.buttonBox.rejected.connect(Dialog.reject)
        self.itemDate.setDate(QDate.currentDate())
        self.itemFood.currentIndexChanged.connect(self.indexChanged)

        if self.itemFood.currentIndex() == 0:
            self.grayOutDrink()
        else:
            self.grayOutFood()

    def retranslateUi(self, Dialog):
        _translate = QtCore.QCoreApplication.translate
        Dialog.setWindowTitle(_translate("Dialog", "Dialog"))
        self.label_3.setText(_translate("Dialog", "Nutrition Information"))
        self.label_5.setText(_translate("Dialog", "Fat (g):"))
        self.label_4.setText(_translate("Dialog", "Calories (kcal):"))
        self.label_6.setText(_translate("Dialog", "Carbohydrate (g):"))
        self.label_7.setText(_translate("Dialog", "Protein (g):"))
        self.label_10.setText(_translate("Dialog", "For Drink Only:"))
        self.label_12.setText(_translate("Dialog", "Volume (ml):"))
        self.label_13.setText(_translate("Dialog", "Alcohol Percentage:"))
        self.itemMeal.setItemText(0, _translate("Dialog", "Breakfast"))
        self.itemMeal.setItemText(1, _translate("Dialog", "Lunch"))
        self.itemMeal.setItemText(2, _translate("Dialog", "Dinner"))
        self.itemMeal.setItemText(3, _translate("Dialog", "Snack"))
        self.label.setText(_translate("Dialog", "Select Meal Time:"))
        self.label_8.setText(_translate("Dialog", "Select Type:"))
        self.itemFood.setItemText(0, _translate("Dialog", "Food"))
        self.itemFood.setItemText(1, _translate("Dialog", "Drink"))
        self.label_2.setText(_translate("Dialog", "Item Name:"))
        self.label_11.setText(_translate("Dialog", "Weight (g):"))
        self.label_9.setText(_translate("Dialog", "For Food Only:"))
        self.dateLabel.setText(_translate("Dialog", "Date:"))

    def grayOutDrink(self):
        self.itemWeights.setEnabled(True)
        self.itemVol.setEnabled(False)
        self.itemPercent.setEnabled(False)

    def grayOutFood(self):
        self.itemPercent.setEnabled(True)
        self.itemVol.setEnabled(True)
        self.itemWeights.setEnabled(False)

    def grayOutName(self):
        self.itemName.setEnabled(False)


    def addItem(self):
        dietHistory = DietHistory(-1, self.itemName.text(), self.itemDate.date().toString("MM/dd/yyyy"), str(self.itemMeal.currentText()) , str(self.itemFood.currentText()), str(self.itemWeights.value()), str(self.itemVol.value()), str(self.itemPercent.value()), str(self.itemCal.value()), str(self.itemFat.value()), str(self.itemCarb.value()), str(self.itemPro.value()))
        self.controller.onAddItem(dietHistory)

    def editItem(self):
        dietHistory = DietHistory(self.dietItem.dietID, self.itemName.text(), self.itemDate.date().toString("MM/dd/yyyy"),
                                  str(self.itemMeal.currentText()), str(self.itemFood.currentText()),
                                  str(self.itemWeights.value()), str(self.itemVol.value()),
                                  str(self.itemPercent.value()), str(self.itemCal.value()), str(self.itemFat.value()),
                                  str(self.itemCarb.value()), str(self.itemPro.value()))
        self.controller.onEditItem(dietHistory)
        pass

    def indexChanged(self, index):
        if self.itemFood.currentIndex() == 0:
            self.grayOutDrink()
        else:
            self.grayOutFood()

    def updateUI(self):
        self.itemDate.setDate(QDate.fromString(self.dietItem.dietDate, "MM/dd/yyyy"))
        self.itemName.setText(self.dietItem.dietName)
        if self.dietItem.dietTime == "Breakfast":
            self.itemMeal.setCurrentIndex(0)
        elif self.dietItem.dietTime == "Lunch":
            self.itemMeal.setCurrentIndex(1)
        elif self.dietItem.dietTime == "Dinner":
            self.itemMeal.setCurrentIndex(2)
        else:
            self.itemMeal.setCurrentIndex(3)

        if self.dietItem.dietType == "Food":
            self.itemFood.setCurrentIndex(0)
        elif self.dietItem.dietType == "Drink":
            self.itemFood.setCurrentIndex(1)

        self.itemWeights.setValue(self.dietItem.dietWeight)
        self.itemVol.setValue(self.dietItem.dietVolume)
        self.itemPercent.setValue(self.dietItem.dietVolume)
        self.itemCal.setValue(self.dietItem.dietCalories)
        self.itemFat.setValue(self.dietItem.dietFat)
        self.itemCarb.setValue(self.dietItem.dietCarbs)
        self.itemPro.setValue(self.dietItem.dietPro)



        self.grayOutName()

        # self.