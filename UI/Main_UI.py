from PyQt5 import QtCore, QtGui, QtWidgets
import psycopg2
import psycopg2.extras


class MainWindowUI(object):
    def launchExtendedSignUp(self, ID):
        from View.signup_extended_info_controller import signup_extended_info_controller
        auth = signup_extended_info_controller(ID)
        auth.exec_()

    def launchSignup(self):
        auth = signup()
        auth.exec_()

    def login(self):
        username = self.username.text()
        password = self.password.text()

        connection = psycopg2.connect(user="", password="", host="localhost", database="HealthTracker")
        manager = connection.cursor()
        self.query = manager.execute("SELECT email, password FROM users WHERE email = %s AND password = %s", [username, password])
        array_row = manager.fetchone()
        if (array_row == None):
            print("Login failed")
            return
        else:
            getUsername = array_row[0]
            getPassword = array_row[1]

        if getUsername == username and getPassword == password:
            self.query = manager.execute("SELECT id FROM users WHERE email = %s", [username])
            ID = manager.fetchone()[0]
            self.query = manager.execute("SELECT weight, height, dob FROM users WHERE id = %s", [ID])
            array_row = manager.fetchone()
            if (not array_row[0] or not array_row[1] or not array_row[2]):
                self.hide()
                self.launchExtendedSignUp(ID)
                return
            else:
                from Controller.UserController import UserController
                auth = UserController(ID)
                self.hide()
                auth.exec_()
                return

        #     this is where the users page will load
        else:
            print("Login failed")
        manager.close()
        connection.close()

    def setupUi(self, Dialog):
        Dialog.setWindowTitle("Sign Up")
        Dialog.setObjectName("Sign Up")
        Dialog.resize(406, 294)
        Dialog.setMinimumSize(QtCore.QSize(406, 294))
        Dialog.setMaximumSize(QtCore.QSize(406, 294))
        Dialog.setLayoutDirection(QtCore.Qt.LeftToRight)
        self.buttonBox = QtWidgets.QDialogButtonBox(Dialog)
        self.buttonBox.setGeometry(QtCore.QRect(0, 200, 341, 32))
        self.buttonBox.setOrientation(QtCore.Qt.Horizontal)
        self.buttonBox.setStandardButtons(QtWidgets.QDialogButtonBox.Cancel|QtWidgets.QDialogButtonBox.Ok)
        self.buttonBox.setObjectName("buttonBox")
        self.username = QtWidgets.QLineEdit(Dialog)
        self.username.setGeometry(QtCore.QRect(60, 120, 281, 21))
        self.username.setObjectName("username")
        self.password = QtWidgets.QLineEdit(Dialog)
        self.password.setGeometry(QtCore.QRect(60, 160, 281, 21))
        self.password.setInputMethodHints(QtCore.Qt.ImhHiddenText|QtCore.Qt.ImhNoAutoUppercase|QtCore.Qt.ImhNoPredictiveText|QtCore.Qt.ImhSensitiveData)
        self.password.setText("")
        self.password.setEchoMode(QtWidgets.QLineEdit.Password)
        self.password.setObjectName("password")
        self.label = QtWidgets.QLabel(Dialog)
        self.label.setGeometry(QtCore.QRect(10, 20, 381, 61))
        font = QtGui.QFont()
        font.setPointSize(30)
        self.label.setFont(font)
        self.label.setAlignment(QtCore.Qt.AlignCenter)
        self.label.setObjectName("label")
        self.label_2 = QtWidgets.QLabel(Dialog)
        self.label_2.setGeometry(QtCore.QRect(60, 90, 261, 16))
        self.label_2.setObjectName("label_2")
        self.pushButton = QtWidgets.QPushButton(Dialog)
        self.pushButton.setGeometry(QtCore.QRect(60, 240, 281, 32))
        self.pushButton.setObjectName("pushButton")
        self.pushButton.clicked.connect(lambda: self.launchSignup())
        self.retranslateUi(Dialog)
        self.buttonBox.accepted.connect(lambda: self.login())
        # Or Dialog.accept
        self.buttonBox.rejected.connect(Dialog.reject)
        QtCore.QMetaObject.connectSlotsByName(Dialog)

    def retranslateUi(self, Dialog):
        _translate = QtCore.QCoreApplication.translate
        Dialog.setWindowTitle(_translate("Dialog", "Log in"))
        self.username.setPlaceholderText(_translate("Dialog", "Email"))
        self.password.setPlaceholderText(_translate("Dialog", "Password"))
        self.label.setText(_translate("Dialog", "Group 10 Health Tracker"))
        self.label_2.setText(_translate("Dialog", "Please enter your login details:"))
        self.pushButton.setText(_translate("Dialog", "Click Here To Sign Up"))


class MainWindow(QtWidgets.QDialog, MainWindowUI):

    def __init__(self, parent=None):
        QtWidgets.QDialog.__init__(self, parent=parent)
        self.setupUi(self)


class signupUI(object):

    def launchMainWindow(self):
        auth = MainWindow()
        auth.exec_()

    def signup(self):
        firstName = self.firstname.text()
        surname = self.surname.text()
        email = self.email.text()
        password = self.password_1.text()
        if (not firstName or not surname or not email or not password):
            print("Please leave no boxes empty.")
            return
        if (password != self.password_2.text()):
            print("Passwords don't match.")
            return
        connection = psycopg2.connect(user="", password="", host="localhost", database="HealthTracker")
        manager = connection.cursor()
        self.query = manager.execute("SELECT EXISTS (SELECT users.email FROM users WHERE users.email = %s)", [email])
        if (manager.fetchone()[0] is True):
            print("'" + email + "'" + " is already registered to an account")
            return
        self.query = manager.execute("INSERT INTO users(firstname, surname, email, password) VALUES (%s, %s, %s, %s)", [firstName, surname, email, password])
        connection.commit()
        manager.close()
        connection.close()
        self.hide()

    def setupUi(self, Dialog):
        Dialog.setObjectName("Dialog")
        Dialog.resize(406, 210)
        Dialog.setMinimumSize(QtCore.QSize(406, 210))
        Dialog.setMaximumSize(QtCore.QSize(406, 210))
        Dialog.setLayoutDirection(QtCore.Qt.LeftToRight)
        self.buttonBox = QtWidgets.QDialogButtonBox(Dialog)
        self.buttonBox.setGeometry(QtCore.QRect(-10, 170, 341, 32))
        self.buttonBox.setOrientation(QtCore.Qt.Horizontal)
        self.buttonBox.setStandardButtons(QtWidgets.QDialogButtonBox.Cancel|QtWidgets.QDialogButtonBox.Ok)
        self.buttonBox.setObjectName("buttonBox")
        self.firstname = QtWidgets.QLineEdit(Dialog)
        self.firstname.setGeometry(QtCore.QRect(50, 50, 131, 21))
        self.firstname.setObjectName("firstname")
        self.email = QtWidgets.QLineEdit(Dialog)
        self.email.setGeometry(QtCore.QRect(50, 80, 281, 21))
        self.email.setInputMethodHints(QtCore.Qt.ImhNone)
        self.email.setText("")
        self.email.setEchoMode(QtWidgets.QLineEdit.Normal)
        self.email.setObjectName("email")
        self.password_1 = QtWidgets.QLineEdit(Dialog)
        self.password_1.setGeometry(QtCore.QRect(50, 110, 281, 21))
        self.password_1.setInputMethodHints(QtCore.Qt.ImhHiddenText|QtCore.Qt.ImhNoAutoUppercase|QtCore.Qt.ImhNoPredictiveText|QtCore.Qt.ImhSensitiveData)
        self.password_1.setText("")
        self.password_1.setEchoMode(QtWidgets.QLineEdit.Password)
        self.password_1.setObjectName("password_1")
        self.password_2 = QtWidgets.QLineEdit(Dialog)
        self.password_2.setGeometry(QtCore.QRect(50, 140, 281, 21))
        self.password_2.setInputMethodHints(QtCore.Qt.ImhHiddenText|QtCore.Qt.ImhNoAutoUppercase|QtCore.Qt.ImhNoPredictiveText|QtCore.Qt.ImhSensitiveData)
        self.password_2.setText("")
        self.password_2.setEchoMode(QtWidgets.QLineEdit.Password)
        self.password_2.setObjectName("password_2")
        self.surname = QtWidgets.QLineEdit(Dialog)
        self.surname.setGeometry(QtCore.QRect(200, 50, 131, 21))
        self.surname.setInputMethodHints(QtCore.Qt.ImhNone)
        self.surname.setText("")
        self.surname.setEchoMode(QtWidgets.QLineEdit.Normal)
        self.surname.setObjectName("surname")
        self.label = QtWidgets.QLabel(Dialog)
        self.label.setGeometry(QtCore.QRect(30, 20, 311, 16))
        font = QtGui.QFont()
        font.setPointSize(14)
        self.label.setFont(font)
        self.label.setTextFormat(QtCore.Qt.RichText)
        self.label.setAlignment(QtCore.Qt.AlignCenter)
        self.label.setObjectName("label")

        self.retranslateUi(Dialog)
        self.buttonBox.accepted.connect(lambda: self.signup())
        self.buttonBox.rejected.connect(self.hide)
        QtCore.QMetaObject.connectSlotsByName(Dialog)

    def retranslateUi(self, Dialog):
        _translate = QtCore.QCoreApplication.translate
        Dialog.setWindowTitle(_translate("Dialog", "Dialog"))
        self.firstname.setPlaceholderText(_translate("Dialog", "First Name"))
        self.email.setPlaceholderText(_translate("Dialog", "Email"))
        self.password_1.setPlaceholderText(_translate("Dialog", "Password"))
        self.password_2.setPlaceholderText(_translate("Dialog", "Please enter password again"))
        self.surname.setPlaceholderText(_translate("Dialog", "Surname"))
        self.label.setText(_translate("Dialog", "<html><head/><body><p><span style=\" font-weight:600; text-decoration: underline;\">Please enter your details into the form:</span></p></body></html>"))


class signup(QtWidgets.QDialog, signupUI):

    def __init__(self, parent=None):
        QtWidgets.QDialog.__init__(self, parent=parent)
        self.setupUi(self)


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    auth = MainWindow()
    auth.exec_()