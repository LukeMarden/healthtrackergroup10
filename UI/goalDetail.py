# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'goal_Detail.ui'
#
# Created by: PyQt5 UI code generator 5.14.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtWidgets
from PyQt5.QtWidgets import QMessageBox
from PyQt5 import QtGui

from Model.Goal import Goal
from Model.GoalType import GoalType
from UI.goalCalendar import SetDateDialog


class GoalDetailDialog(object):


    def __init__(self):
        self.currentGoal = Goal.createEmptyGoal()

    def setupUi(self, Dialog):
        Dialog.setObjectName("Dialog")
        Dialog.resize(300, 300)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(Dialog.sizePolicy().hasHeightForWidth())
        Dialog.setSizePolicy(sizePolicy)
        Dialog.setMaximumSize(QtCore.QSize(300, 300))
        Dialog.setStyleSheet("background-color: rgb(76, 76, 76);")
        self.verticalLayout = QtWidgets.QVBoxLayout(Dialog)
        self.verticalLayout.setContentsMargins(-1, -1, -1, 9)
        self.verticalLayout.setSpacing(0)
        self.verticalLayout.setObjectName("verticalLayout")
        self.frame = QtWidgets.QFrame(Dialog)
        self.frame.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.frame.setFrameShadow(QtWidgets.QFrame.Raised)
        self.frame.setObjectName("frame")
        self.horizontalLayout_2 = QtWidgets.QHBoxLayout(self.frame)
        self.horizontalLayout_2.setSpacing(9)
        self.horizontalLayout_2.setObjectName("horizontalLayout_2")
        self.frame_7 = QtWidgets.QFrame(self.frame)
        self.frame_7.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.frame_7.setFrameShadow(QtWidgets.QFrame.Raised)
        self.frame_7.setObjectName("frame_7")
        self.verticalLayout_3 = QtWidgets.QVBoxLayout(self.frame_7)
        self.verticalLayout_3.setObjectName("verticalLayout_3")
        self.label_7 = QtWidgets.QLabel(self.frame_7)
        self.label_7.setMaximumSize(QtCore.QSize(16777215, 20))
        font = QtGui.QFont()
        font.setFamily("Segoe UI Semibold")
        font.setPointSize(10)
        font.setBold(True)
        font.setWeight(75)
        self.label_7.setFont(font)
        self.label_7.setStyleSheet("color: rgb(255, 255, 255);")
        self.label_7.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignTrailing | QtCore.Qt.AlignVCenter)
        self.label_7.setObjectName("label_7")
        self.verticalLayout_3.addWidget(self.label_7)
        self.label_5 = QtWidgets.QLabel(self.frame_7)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.label_5.sizePolicy().hasHeightForWidth())
        self.label_5.setSizePolicy(sizePolicy)
        self.label_5.setMaximumSize(QtCore.QSize(16777215, 20))
        font = QtGui.QFont()
        font.setFamily("Segoe UI Semibold")
        font.setPointSize(10)
        font.setBold(True)
        font.setWeight(75)
        self.label_5.setFont(font)
        self.label_5.setStyleSheet("color: rgb(255, 255, 255);")
        self.label_5.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignTrailing | QtCore.Qt.AlignVCenter)
        self.label_5.setObjectName("label_5")
        self.verticalLayout_3.addWidget(self.label_5)
        self.label_8 = QtWidgets.QLabel(self.frame_7)
        self.label_8.setMaximumSize(QtCore.QSize(16777215, 20))
        font = QtGui.QFont()
        font.setFamily("Segoe UI Semibold")
        font.setPointSize(10)
        font.setBold(True)
        font.setWeight(75)
        self.label_8.setFont(font)
        self.label_8.setStyleSheet("color: rgb(255, 255, 255);")
        self.label_8.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignTrailing | QtCore.Qt.AlignVCenter)
        self.label_8.setObjectName("label_8")
        self.verticalLayout_3.addWidget(self.label_8)
        self.label_6 = QtWidgets.QLabel(self.frame_7)
        self.label_6.setMaximumSize(QtCore.QSize(16777215, 20))
        font = QtGui.QFont()
        font.setFamily("Segoe UI Semibold")
        font.setPointSize(10)
        font.setBold(True)
        font.setWeight(75)
        self.label_6.setFont(font)
        self.label_6.setStyleSheet("color: rgb(255, 255, 255);")
        self.label_6.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignTrailing | QtCore.Qt.AlignVCenter)
        self.label_6.setObjectName("label_6")
        self.verticalLayout_3.addWidget(self.label_6)
        self.label_4 = QtWidgets.QLabel(self.frame_7)
        self.label_4.setMaximumSize(QtCore.QSize(16777215, 25))
        font = QtGui.QFont()
        font.setFamily("Segoe UI Semibold")
        font.setPointSize(10)
        font.setBold(True)
        font.setWeight(75)
        self.label_4.setFont(font)
        self.label_4.setStyleSheet("color: rgb(255, 255, 255);")
        self.label_4.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignTrailing | QtCore.Qt.AlignVCenter)
        self.label_4.setObjectName("label_4")
        self.verticalLayout_3.addWidget(self.label_4)
        self.horizontalLayout_2.addWidget(self.frame_7)
        self.frame_2 = QtWidgets.QFrame(self.frame)
        self.frame_2.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.frame_2.setFrameShadow(QtWidgets.QFrame.Raised)
        self.frame_2.setObjectName("frame_2")
        self.verticalLayout_2 = QtWidgets.QVBoxLayout(self.frame_2)
        self.verticalLayout_2.setObjectName("verticalLayout_2")
        self.GoalNameText = QtWidgets.QLineEdit(self.frame_2)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.GoalNameText.sizePolicy().hasHeightForWidth())
        self.GoalNameText.setSizePolicy(sizePolicy)
        self.GoalNameText.setMaximumSize(QtCore.QSize(16777215, 20))
        self.GoalNameText.setStyleSheet("background-color: rgb(225, 225, 225);\n"
                                        "font: 63 8pt \"Segoe UI Semibold\";")
        self.GoalNameText.setFrame(False)
        self.GoalNameText.setObjectName("GoalNameText")
        self.verticalLayout_2.addWidget(self.GoalNameText)
        self.GoalTypesComboBox = QtWidgets.QComboBox(self.frame_2)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.GoalTypesComboBox.sizePolicy().hasHeightForWidth())
        self.GoalTypesComboBox.setSizePolicy(sizePolicy)
        self.GoalTypesComboBox.setMinimumSize(QtCore.QSize(0, 0))
        self.GoalTypesComboBox.setMaximumSize(QtCore.QSize(16777215, 20))
        self.GoalTypesComboBox.setStyleSheet("QComboBox{\n"
                                             "   background-color: rgb(225, 225, 225);\n"
                                             "font: 63 8pt \"Segoe UI Semibold\";\n"
                                             "\n"
                                             "}\n"
                                             "\n"
                                             "QComboBox:drop-down{\n"
                                             "    background-color: rgb(81, 175, 81);\n"
                                             "    padding: 1px;\n"
                                             "    border: 4px solid rgb(81, 175, 81);\n"
                                             "    border-radius: 1px;\n"
                                             "    color: rgb(255, 255, 255);\n"
                                             "}\n"
                                             "\n"
                                             "QComboBox:drop-down:Pressed{\n"
                                             "    background-color: rgb(55, 140, 55);\n"
                                             "    border-color: rgb(55, 140, 55);\n"
                                             "}\n"
                                             "\n"
                                             "\n"
                                             "QComboBox:down-arrow {\n"
                                             "width: 0; \n"
                                             "  height: 0; \n"
                                             "  border-left: 5px solid rgb(81, 175, 81);\n"
                                             "  border-right: 5px solid rgb(81, 175, 81);\n"
                                             "  border-top: 5px solid  rgb(255, 255, 255);\n"
                                             "    \n"
                                             "\n"
                                             "}\n"
                                             "\n"
                                             "QComboBox QAbstractItemView {\n"
                                             "background-color: rgb(225, 225, 225);\n"
                                             "    outline: none;\n"
                                             "}\n"
                                             "\n"
                                             "\n"
                                             "\n"
                                             "\n"
                                             "QPushButton:Pressed{\n"
                                             "    background-color: rgb(55, 140, 55);\n"
                                             "    border-color: rgb(55, 140, 55);\n"
                                             "}\n"
                                             "\n"
                                             "")
        self.GoalTypesComboBox.setEditable(True)
        self.GoalTypesComboBox.setCurrentText("")
        self.GoalTypesComboBox.setIconSize(QtCore.QSize(16, 16))
        self.GoalTypesComboBox.setDuplicatesEnabled(True)
        self.GoalTypesComboBox.setFrame(False)
        self.GoalTypesComboBox.setObjectName("GoalTypesComboBox")
        self.verticalLayout_2.addWidget(self.GoalTypesComboBox)
        self.CurrentValueSpin = QtWidgets.QDoubleSpinBox(self.frame_2)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.CurrentValueSpin.sizePolicy().hasHeightForWidth())
        self.CurrentValueSpin.setSizePolicy(sizePolicy)
        self.CurrentValueSpin.setMaximumSize(QtCore.QSize(16777215, 20))
        font = QtGui.QFont()
        font.setFamily("Segoe UI Semibold")
        font.setPointSize(8)
        font.setBold(False)
        font.setItalic(False)
        font.setWeight(7)
        self.CurrentValueSpin.setFont(font)
        self.CurrentValueSpin.setStyleSheet("QDoubleSpinBox{\n"
                                            "   background-color: rgb(225, 225, 225);\n"
                                            "\n"
                                            "    \n"
                                            "    font: 63 8pt \"Segoe UI Semibold\";\n"
                                            "\n"
                                            "}\n"
                                            "\n"
                                            "QDoubleSpinBox:up-button{\n"
                                            "    background-color: rgb(81, 175, 81);\n"
                                            "    padding: 1px;\n"
                                            "    border: 4px solid rgb(81, 175, 81);\n"
                                            "    color: rgb(255, 255, 255);\n"
                                            "}\n"
                                            "\n"
                                            "QDoubleSpinBox:up-button:Pressed{\n"
                                            "    background-color: rgb(55, 140, 55);\n"
                                            "    border-color: rgb(55, 140, 55);\n"
                                            "}\n"
                                            "\n"
                                            "QDoubleSpinBox:down-button{\n"
                                            "    background-color: rgb(81, 175, 81);\n"
                                            "    padding: 1px;\n"
                                            "    border: 4px solid rgb(81, 175, 81);\n"
                                            "    color: rgb(255, 255, 255);\n"
                                            "}\n"
                                            "\n"
                                            "QDoubleSpinBox:down-button:Pressed{\n"
                                            "    background-color: rgb(55, 140, 55);\n"
                                            "    border-color: rgb(55, 140, 55);\n"
                                            "}\n"
                                            "\n"
                                            "\n"
                                            "\n"
                                            "QDoubleSpinBox:up-arrow {\n"
                                            "width: 0; \n"
                                            "  height: 0; \n"
                                            "  border-left: 4px solid rgb(81, 175, 81);\n"
                                            "  border-right: 4px solid rgb(81, 175, 81);\n"
                                            "  border-bottom: 4px solid  rgb(255, 255, 255);\n"
                                            "}\n"
                                            "\n"
                                            "QDoubleSpinBox:down-arrow {\n"
                                            "width: 0; \n"
                                            "  height: 0; \n"
                                            "  border-left: 4px solid rgb(81, 175, 81);\n"
                                            "  border-right: 4px solid rgb(81, 175, 81);\n"
                                            "  border-top: 4px solid  rgb(255, 255, 255);\n"
                                            "}\n"
                                            "")
        self.CurrentValueSpin.setMaximum(10000.0)
        self.CurrentValueSpin.setObjectName("CurrentValueSpin")
        self.verticalLayout_2.addWidget(self.CurrentValueSpin)
        self.GoalValueSpin = QtWidgets.QDoubleSpinBox(self.frame_2)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.GoalValueSpin.sizePolicy().hasHeightForWidth())
        self.GoalValueSpin.setSizePolicy(sizePolicy)
        self.GoalValueSpin.setMaximumSize(QtCore.QSize(16777215, 20))
        font = QtGui.QFont()
        font.setFamily("Segoe UI Semibold")
        font.setPointSize(8)
        font.setBold(False)
        font.setItalic(False)
        font.setWeight(7)
        self.GoalValueSpin.setFont(font)
        self.GoalValueSpin.setStyleSheet("QDoubleSpinBox{\n"
                                         "   background-color: rgb(225, 225, 225);\n"
                                         "\n"
                                         "    \n"
                                         "    font: 63 8pt \"Segoe UI Semibold\";\n"
                                         "\n"
                                         "}\n"
                                         "\n"
                                         "QDoubleSpinBox:up-button{\n"
                                         "    background-color: rgb(81, 175, 81);\n"
                                         "    padding: 1px;\n"
                                         "    border: 4px solid rgb(81, 175, 81);\n"
                                         "    color: rgb(255, 255, 255);\n"
                                         "}\n"
                                         "\n"
                                         "QDoubleSpinBox:up-button:Pressed{\n"
                                         "    background-color: rgb(55, 140, 55);\n"
                                         "    border-color: rgb(55, 140, 55);\n"
                                         "}\n"
                                         "\n"
                                         "QDoubleSpinBox:down-button{\n"
                                         "    background-color: rgb(81, 175, 81);\n"
                                         "    padding: 1px;\n"
                                         "    border: 4px solid rgb(81, 175, 81);\n"
                                         "    color: rgb(255, 255, 255);\n"
                                         "}\n"
                                         "\n"
                                         "QDoubleSpinBox:down-button:Pressed{\n"
                                         "    background-color: rgb(55, 140, 55);\n"
                                         "    border-color: rgb(55, 140, 55);\n"
                                         "}\n"
                                         "\n"
                                         "\n"
                                         "\n"
                                         "QDoubleSpinBox:up-arrow {\n"
                                         "width: 0; \n"
                                         "  height: 0; \n"
                                         "  border-left: 4px solid rgb(81, 175, 81);\n"
                                         "  border-right: 4px solid rgb(81, 175, 81);\n"
                                         "  border-bottom: 4px solid  rgb(255, 255, 255);\n"
                                         "}\n"
                                         "\n"
                                         "QDoubleSpinBox:down-arrow {\n"
                                         "width: 0; \n"
                                         "  height: 0; \n"
                                         "  border-left: 4px solid rgb(81, 175, 81);\n"
                                         "  border-right: 4px solid rgb(81, 175, 81);\n"
                                         "  border-top: 4px solid  rgb(255, 255, 255);\n"
                                         "}\n"
                                         "")
        self.GoalValueSpin.setMaximum(10000.0)
        self.GoalValueSpin.setObjectName("GoalValueSpin")
        self.verticalLayout_2.addWidget(self.GoalValueSpin)
        self.EditDateButton = QtWidgets.QPushButton(self.frame_2)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.EditDateButton.sizePolicy().hasHeightForWidth())
        self.EditDateButton.setSizePolicy(sizePolicy)
        self.EditDateButton.setMaximumSize(QtCore.QSize(16777215, 25))
        self.EditDateButton.setStyleSheet("QPushButton {\n"
                                          "    background-color: rgb(81, 175, 81);\n"
                                          "    padding: 1px;\n"
                                          "    border: 4px solid rgb(81, 175, 81);\n"
                                          "    border-radius: 1px;\n"
                                          "    color: rgb(255, 255, 255);\n"
                                          "font: 63 8pt \"Segoe UI Semibold\";\n"
                                          "}\n"
                                          "\n"
                                          "\n"
                                          "\n"
                                          "QPushButton:Pressed{\n"
                                          "    background-color: rgb(55, 140, 55);\n"
                                          "    border-color: rgb(55, 140, 55);\n"
                                          "}")
        self.EditDateButton.setObjectName("EditDateButton")
        self.verticalLayout_2.addWidget(self.EditDateButton)
        self.horizontalLayout_2.addWidget(self.frame_2)
        self.verticalLayout.addWidget(self.frame)
        self.buttonBox = QtWidgets.QDialogButtonBox(Dialog)
        font = QtGui.QFont()
        font.setFamily("Segoe UI Semibold")
        font.setBold(True)
        font.setWeight(75)
        self.buttonBox.setFont(font)
        self.buttonBox.setStyleSheet("QPushButton {\n"
                                     "    background-color: rgb(81, 175, 81);\n"
                                     "    padding: 2px;\n"
                                     "    border: 4px solid rgb(81, 175, 81);\n"
                                     "    border-radius: 1px;\n"
                                     "    color: rgb(255, 255, 255);\n"
                                     "    padding: 0 8px 0 8px;\n"
                                     "    min-width: 50px;\n"
                                     "font: 63 8pt \"Segoe UI Semibold\";\n"
                                     "}\n"
                                     "\n"
                                     "\n"
                                     "\n"
                                     "QPushButton:Pressed{\n"
                                     "    background-color: rgb(55, 140, 55);\n"
                                     "    border-color: rgb(55, 140, 55);\n"
                                     "}")
        self.buttonBox.setStandardButtons(QtWidgets.QDialogButtonBox.Cancel | QtWidgets.QDialogButtonBox.Ok)
        self.buttonBox.setCenterButtons(True)
        self.buttonBox.setObjectName("buttonBox")
        self.verticalLayout.addWidget(self.buttonBox)
        spacerItem = QtWidgets.QSpacerItem(20, 0, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.verticalLayout.addItem(spacerItem)
        self.verticalLayout.setStretch(0, 10)
        self.verticalLayout.setStretch(1, 10)
        self.verticalLayout.setStretch(2, 1)

        self.retranslateUi(Dialog)
        QtCore.QMetaObject.connectSlotsByName(Dialog)

        #UI init
        self.buttonBox.accepted.connect(Dialog.accept)
        self.buttonBox.accepted.connect(self.onGoalDetailSet)
        self.buttonBox.rejected.connect(Dialog.reject)
        self.EditDateButton.clicked.connect(self.callCalendar)
        self.GoalTypesComboBox.addItem("Target Weight (kg)")
        self.GoalTypesComboBox.addItem("Gym Exercise (reps)")
        self.GoalTypesComboBox.addItem("Running Distance (miles/km)")
        self.GoalTypesComboBox.addItem("Average Speed (mph / km/h)")
        self.GoalTypesComboBox.addItem("Calories Consuming (cal)")
        self.GoalTypesComboBox.addItem("Sugar Intake (gram)")
        self.GoalTypesComboBox.addItem("Water Intake (ml)")
        self.GoalTypesComboBox.addItem("Alcohol Intake (ml)")
        self.GoalTypesComboBox.setCurrentIndex(0)


    def retranslateUi(self, Dialog):
        _translate = QtCore.QCoreApplication.translate
        Dialog.setWindowTitle(_translate("Dialog", "Dialog"))
        self.label_7.setText(_translate("Dialog", "Goal Name:"))
        self.label_5.setText(_translate("Dialog", "Goal Type:"))
        self.label_8.setText(_translate("Dialog", "Current Value:"))
        self.label_6.setText(_translate("Dialog", "Goal Value:"))
        self.label_4.setText(_translate("Dialog", "Goal End Date: "))
        self.EditDateButton.setText(_translate("Dialog", "Edit"))

    def callCalendar(self):
        calendarDialog = QtWidgets.QDialog()
        ui = SetDateDialog()
        ui.setupUi(calendarDialog)
        ui.setSelectedDateToUI(self.currentGoal.endDate)
        if calendarDialog.exec_():
            self.currentGoal.endDate = ui.getSelectedDate().toString('dd-MM-yyyy')
            self.EditDateButton.setText(self.currentGoal.endDate)

    def setUIFromGoal(self, setGoal):
        self.currentGoal = setGoal
        self.GoalNameText.setText(setGoal.goalName)
        switcher = {
            GoalType.WEIGHT: 0,
            GoalType.EXERCISE: 1,
            GoalType.DISTANCE: 2,
            GoalType.SPEED: 3,
            GoalType.CALORIES: 4,
            GoalType.SUGAR: 5,
            GoalType.WATER: 6,
            GoalType.ALCOHOL: 7
        }
        self.GoalTypesComboBox.setCurrentIndex(switcher.get(setGoal.goalType))
        self.GoalValueSpin.setValue(setGoal.goalValue)
        self.CurrentValueSpin.setValue(setGoal.currentValue)
        self.EditDateButton.setText(self.currentGoal.endDate)

    def onGoalDetailSet(self):
        self.currentGoal.goalName = self.GoalNameText.text()
        index = self.GoalTypesComboBox.currentIndex()
        switcher = {
            0: GoalType.WEIGHT,
            1: GoalType.EXERCISE,
            2: GoalType.DISTANCE,
            3: GoalType.SPEED,
            4: GoalType.CALORIES,
            5: GoalType.SUGAR,
            6: GoalType.WATER,
            7: GoalType.ALCOHOL
        }
        # Set date is set when initialising
        # End date is set when user choosing date in calendar
        self.currentGoal.goalType = switcher.get(index)
        self.currentGoal.goalValue = self.GoalValueSpin.value()
        self.currentGoal.currentValue = self.CurrentValueSpin.value()


    def getCurrentGoal(self):
        return self.currentGoal

    def grayOutName(self):
        self.GoalNameText.setEnabled(False)


def PrintMsg(msgText):
    msg = QMessageBox()
    msg.setIcon(QMessageBox.Information)
    msg.setText("Message: " + msgText)
    msg.setStandardButtons(QMessageBox.Ok)
    msg.exec_()

