# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'signup_basic_info_2.ui'
#
# Created by: PyQt5 UI code generator 5.14.1
#
# WARNING! All changes made in this file will be lost!


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_Dialog(object):
    def setupUi(self, Dialog):
        Dialog.setObjectName("Dialog")
        Dialog.resize(425, 200)
        Dialog.setMaximumSize(QtCore.QSize(425, 200))
        Dialog.setLayoutDirection(QtCore.Qt.LeftToRight)
        Dialog.setStyleSheet("QLabel\n"
"{\n"
"color: rgb(255, 255, 255);\n"
"}\n"
"\n"
"QDialog\n"
"{\n"
"    background-color: rgb(76, 76, 76);\n"
"    color: rgb(255, 255, 255);\n"
"font: 75 10pt \"Segoe UI Semibold\";\n"
"font-weight:bold;\n"
"\n"
"\n"
"}\n"
"QPushButton {\n"
"    background-color: rgb(81, 175, 81);\n"
"    padding: 1px;\n"
"    border: 4px solid rgb(81, 175, 81);\n"
"    border-radius: 1px;\n"
"    color: rgb(255, 255, 255);\n"
"font: 63 8pt \"Segoe UI Semibold\";\n"
"}\n"
"\n"
"\n"
"\n"
"QPushButton:Pressed{\n"
"    background-color: rgb(55, 140, 55);\n"
"    border-color: rgb(55, 140, 55);\n"
"}")
        self.gridLayout = QtWidgets.QGridLayout(Dialog)
        self.gridLayout.setObjectName("gridLayout")
        self.frame_2 = QtWidgets.QFrame(Dialog)
        self.frame_2.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.frame_2.setFrameShadow(QtWidgets.QFrame.Raised)
        self.frame_2.setObjectName("frame_2")
        self.verticalLayout = QtWidgets.QVBoxLayout(self.frame_2)
        self.verticalLayout.setObjectName("verticalLayout")
        self.label = QtWidgets.QLabel(self.frame_2)
        font = QtGui.QFont()
        font.setFamily("Segoe UI Semibold")
        font.setPointSize(10)
        font.setBold(True)
        font.setItalic(False)
        font.setWeight(75)
        self.label.setFont(font)
        self.label.setStyleSheet("color: rgb(255, 255, 255);\n"
"font: 75 10pt \"Segoe UI Semibold\";\n"
"font-weight:bold;")
        self.label.setTextFormat(QtCore.Qt.RichText)
        self.label.setAlignment(QtCore.Qt.AlignBottom|QtCore.Qt.AlignLeading|QtCore.Qt.AlignLeft)
        self.label.setObjectName("label")
        self.verticalLayout.addWidget(self.label)
        self.frame = QtWidgets.QFrame(self.frame_2)
        self.frame.setMaximumSize(QtCore.QSize(16777215, 20))
        self.frame.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.frame.setFrameShadow(QtWidgets.QFrame.Raised)
        self.frame.setObjectName("frame")
        self.horizontalLayout = QtWidgets.QHBoxLayout(self.frame)
        self.horizontalLayout.setContentsMargins(0, 0, 0, 0)
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.firstname = QtWidgets.QLineEdit(self.frame)
        self.firstname.setMinimumSize(QtCore.QSize(0, 20))
        self.firstname.setStyleSheet("background-color: rgb(225, 225, 225);\n"
"font: 63 8pt \"Segoe UI Semibold\";")
        self.firstname.setFrame(False)
        self.firstname.setObjectName("firstname")
        self.horizontalLayout.addWidget(self.firstname)
        self.surname = QtWidgets.QLineEdit(self.frame)
        self.surname.setMinimumSize(QtCore.QSize(0, 20))
        self.surname.setStyleSheet("background-color: rgb(225, 225, 225);\n"
"font: 63 8pt \"Segoe UI Semibold\";")
        self.surname.setInputMethodHints(QtCore.Qt.ImhNone)
        self.surname.setText("")
        self.surname.setFrame(False)
        self.surname.setEchoMode(QtWidgets.QLineEdit.Normal)
        self.surname.setObjectName("surname")
        self.horizontalLayout.addWidget(self.surname)
        self.verticalLayout.addWidget(self.frame)
        self.email = QtWidgets.QLineEdit(self.frame_2)
        self.email.setMinimumSize(QtCore.QSize(0, 20))
        self.email.setStyleSheet("background-color: rgb(225, 225, 225);\n"
"font: 63 8pt \"Segoe UI Semibold\";")
        self.email.setInputMethodHints(QtCore.Qt.ImhNone)
        self.email.setText("")
        self.email.setFrame(False)
        self.email.setEchoMode(QtWidgets.QLineEdit.Normal)
        self.email.setObjectName("email")
        self.verticalLayout.addWidget(self.email)
        self.password_1 = QtWidgets.QLineEdit(self.frame_2)
        self.password_1.setMinimumSize(QtCore.QSize(0, 20))
        self.password_1.setStyleSheet("background-color: rgb(225, 225, 225);\n"
"font: 63 8pt \"Segoe UI Semibold\";")
        self.password_1.setInputMethodHints(QtCore.Qt.ImhHiddenText|QtCore.Qt.ImhNoAutoUppercase|QtCore.Qt.ImhNoPredictiveText|QtCore.Qt.ImhSensitiveData)
        self.password_1.setText("")
        self.password_1.setFrame(False)
        self.password_1.setEchoMode(QtWidgets.QLineEdit.Password)
        self.password_1.setObjectName("password_1")
        self.verticalLayout.addWidget(self.password_1)
        self.password_2 = QtWidgets.QLineEdit(self.frame_2)
        self.password_2.setMinimumSize(QtCore.QSize(0, 20))
        self.password_2.setStyleSheet("background-color: rgb(225, 225, 225);\n"
"font: 63 8pt \"Segoe UI Semibold\";")
        self.password_2.setInputMethodHints(QtCore.Qt.ImhHiddenText|QtCore.Qt.ImhNoAutoUppercase|QtCore.Qt.ImhNoPredictiveText|QtCore.Qt.ImhSensitiveData)
        self.password_2.setText("")
        self.password_2.setFrame(False)
        self.password_2.setEchoMode(QtWidgets.QLineEdit.Password)
        self.password_2.setObjectName("password_2")
        self.verticalLayout.addWidget(self.password_2)
        spacerItem = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.verticalLayout.addItem(spacerItem)
        self.buttonBox = QtWidgets.QDialogButtonBox(self.frame_2)
        self.buttonBox.setStyleSheet("QPushButton {\n"
"    background-color: rgb(81, 175, 81);\n"
"    padding: 2px;\n"
"    border: 4px solid rgb(81, 175, 81);\n"
"    border-radius: 1px;\n"
"    color: rgb(255, 255, 255);\n"
"    padding: 0 8px 0 8px;\n"
"    min-width: 50px;\n"
"font: 63 8pt \"Segoe UI Semibold\";\n"
"}\n"
"\n"
"\n"
"\n"
"QPushButton:Pressed{\n"
"    background-color: rgb(55, 140, 55);\n"
"    border-color: rgb(55, 140, 55);\n"
"}")
        self.buttonBox.setOrientation(QtCore.Qt.Horizontal)
        self.buttonBox.setStandardButtons(QtWidgets.QDialogButtonBox.Cancel|QtWidgets.QDialogButtonBox.Ok)
        self.buttonBox.setObjectName("buttonBox")
        self.verticalLayout.addWidget(self.buttonBox)
        self.gridLayout.addWidget(self.frame_2, 0, 0, 1, 1)

        self.retranslateUi(Dialog)
        self.buttonBox.accepted.connect(Dialog.accept)
        self.buttonBox.rejected.connect(Dialog.reject)
        QtCore.QMetaObject.connectSlotsByName(Dialog)

    def retranslateUi(self, Dialog):
        _translate = QtCore.QCoreApplication.translate
        Dialog.setWindowTitle(_translate("Dialog", "Dialog"))
        self.label.setText(_translate("Dialog", "<html><head/><body><p>Please enter your details into the form:</p></body></html>"))
        self.firstname.setPlaceholderText(_translate("Dialog", "First Name"))
        self.surname.setPlaceholderText(_translate("Dialog", "Surname"))
        self.email.setPlaceholderText(_translate("Dialog", "Email"))
        self.password_1.setPlaceholderText(_translate("Dialog", "Password"))
        self.password_2.setPlaceholderText(_translate("Dialog", "Please enter password again"))
