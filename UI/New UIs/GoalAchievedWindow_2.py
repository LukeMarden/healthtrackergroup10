# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'GoalAchievedWindow_2.ui'
#
# Created by: PyQt5 UI code generator 5.14.1
#
# WARNING! All changes made in this file will be lost!


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_Dialog(object):
    def setupUi(self, Dialog):
        Dialog.setObjectName("Dialog")
        Dialog.resize(313, 460)
        Dialog.setMinimumSize(QtCore.QSize(313, 460))
        Dialog.setMaximumSize(QtCore.QSize(313, 10000))
        Dialog.setStyleSheet("background-color: rgb(76, 76, 76);")
        self.verticalLayout = QtWidgets.QVBoxLayout(Dialog)
        self.verticalLayout.setObjectName("verticalLayout")
        self.DoneGoalsList = QtWidgets.QListWidget(Dialog)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.DoneGoalsList.sizePolicy().hasHeightForWidth())
        self.DoneGoalsList.setSizePolicy(sizePolicy)
        self.DoneGoalsList.setStyleSheet("background-color: rgb(255, 255, 255);\n"
"background-color: rgb(225, 225, 225);")
        self.DoneGoalsList.setObjectName("DoneGoalsList")
        self.verticalLayout.addWidget(self.DoneGoalsList)
        self.buttonBox = QtWidgets.QDialogButtonBox(Dialog)
        self.buttonBox.setStyleSheet("QPushButton {\n"
"    background-color: rgb(81, 175, 81);\n"
"    padding: 2px;\n"
"    border: 4px solid rgb(81, 175, 81);\n"
"    border-radius: 1px;\n"
"    color: rgb(255, 255, 255);\n"
"    padding: 0 8px 0 8px;\n"
"    min-width: 50px;\n"
"font: 63 8pt \"Segoe UI Semibold\";\n"
"}\n"
"\n"
"\n"
"\n"
"QPushButton:Pressed{\n"
"    background-color: rgb(55, 140, 55);\n"
"    border-color: rgb(55, 140, 55);\n"
"}")
        self.buttonBox.setStandardButtons(QtWidgets.QDialogButtonBox.Ok)
        self.buttonBox.setCenterButtons(True)
        self.buttonBox.setObjectName("buttonBox")
        self.verticalLayout.addWidget(self.buttonBox)

        self.retranslateUi(Dialog)
        QtCore.QMetaObject.connectSlotsByName(Dialog)

    def retranslateUi(self, Dialog):
        _translate = QtCore.QCoreApplication.translate
        Dialog.setWindowTitle(_translate("Dialog", "Dialog"))
