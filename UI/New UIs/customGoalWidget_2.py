# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'customGoalWidget_2.ui'
#
# Created by: PyQt5 UI code generator 5.14.1
#
# WARNING! All changes made in this file will be lost!


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_Form(object):
    def setupUi(self, Form):
        Form.setObjectName("Form")
        Form.resize(400, 300)
        self.frame = QtWidgets.QFrame(Form)
        self.frame.setGeometry(QtCore.QRect(30, 40, 251, 121))
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.frame.sizePolicy().hasHeightForWidth())
        self.frame.setSizePolicy(sizePolicy)
        self.frame.setMinimumSize(QtCore.QSize(251, 121))
        self.frame.setStyleSheet("\n"
"QFrame#frame\n"
"{\n"
"border: 1px solid rgb(76, 76, 76);\n"
"    background-color: rgb(255, 255, 255);\n"
"}\n"
"")
        self.frame.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.frame.setFrameShadow(QtWidgets.QFrame.Raised)
        self.frame.setObjectName("frame")
        self.verticalLayout = QtWidgets.QVBoxLayout(self.frame)
        self.verticalLayout.setObjectName("verticalLayout")
        self.GoalNameText = QtWidgets.QLabel(self.frame)
        self.GoalNameText.setMinimumSize(QtCore.QSize(0, 20))
        font = QtGui.QFont()
        font.setFamily("Segoe UI Semibold")
        font.setPointSize(15)
        font.setBold(True)
        font.setItalic(False)
        font.setWeight(75)
        self.GoalNameText.setFont(font)
        self.GoalNameText.setStyleSheet("font: 63 15pt \"Segoe UI Semibold\";\n"
"font-weight:bold;\n"
"background-color: rgb(255, 255, 255);")
        self.GoalNameText.setAlignment(QtCore.Qt.AlignCenter)
        self.GoalNameText.setObjectName("GoalNameText")
        self.verticalLayout.addWidget(self.GoalNameText)
        self.GoalTypeText = QtWidgets.QLabel(self.frame)
        self.GoalTypeText.setStyleSheet("font: 63 10pt \"Segoe UI Semibold\";\n"
"font-weight:bold;\n"
"background-color: rgb(255, 255, 255);")
        self.GoalTypeText.setObjectName("GoalTypeText")
        self.verticalLayout.addWidget(self.GoalTypeText)
        self.EndDateText = QtWidgets.QLabel(self.frame)
        self.EndDateText.setStyleSheet("font: 63 10pt \"Segoe UI Semibold\";\n"
"font-weight:bold;\n"
"background-color: rgb(255, 255, 255);")
        self.EndDateText.setObjectName("EndDateText")
        self.verticalLayout.addWidget(self.EndDateText)
        self.EndDateText_2 = QtWidgets.QLabel(self.frame)
        self.EndDateText_2.setStyleSheet("font: 63 10pt \"Segoe UI Semibold\";\n"
"font-weight:bold;\n"
"background-color: rgb(255, 255, 255);")
        self.EndDateText_2.setObjectName("EndDateText_2")
        self.verticalLayout.addWidget(self.EndDateText_2)
        self.GoalProgressBar = QtWidgets.QProgressBar(self.frame)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.GoalProgressBar.sizePolicy().hasHeightForWidth())
        self.GoalProgressBar.setSizePolicy(sizePolicy)
        self.GoalProgressBar.setMinimumSize(QtCore.QSize(0, 20))
        self.GoalProgressBar.setStyleSheet("\n"
"QProgressBar {\n"
"    border: 2px solid grey;\n"
"    font: 63 10pt \"Segoe UI Semibold\";\n"
"    font-weight:bold;\n"
"    text-align: center;\n"
"background-color: rgb(255, 255, 255);\n"
"}\n"
"\n"
"QProgressBar::chunk {\n"
"    background-color: rgb(81, 175, 81);\n"
"    width: 20px;\n"
"\n"
"}")
        self.GoalProgressBar.setProperty("value", 2)
        self.GoalProgressBar.setOrientation(QtCore.Qt.Horizontal)
        self.GoalProgressBar.setInvertedAppearance(False)
        self.GoalProgressBar.setObjectName("GoalProgressBar")
        self.verticalLayout.addWidget(self.GoalProgressBar)

        self.retranslateUi(Form)
        QtCore.QMetaObject.connectSlotsByName(Form)

    def retranslateUi(self, Form):
        _translate = QtCore.QCoreApplication.translate
        Form.setWindowTitle(_translate("Form", "Form"))
        self.GoalNameText.setText(_translate("Form", "Goal Name"))
        self.GoalTypeText.setText(_translate("Form", "Goal Type"))
        self.EndDateText.setText(_translate("Form", "End Date"))
        self.EndDateText_2.setText(_translate("Form", "Progress:"))
