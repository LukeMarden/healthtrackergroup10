# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'UserGroup_2.ui'
#
# Created by: PyQt5 UI code generator 5.14.1
#
# WARNING! All changes made in this file will be lost!


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.setEnabled(True)
        MainWindow.resize(550, 300)
        MainWindow.setMaximumSize(QtCore.QSize(550, 300))
        MainWindow.setAutoFillBackground(False)
        MainWindow.setStyleSheet("QLabel\n"
"{\n"
"color: rgb(255, 255, 255);\n"
"}\n"
"\n"
"QDialog\n"
"{\n"
"background-color: rgb(255, 255, 255);\n"
"\n"
"\n"
"    color: rgb(255, 255, 255);\n"
"font: 75 10pt \"Segoe UI Semibold\";\n"
"font-weight:bold;\n"
"\n"
"\n"
"}\n"
"\n"
"background-color: rgb(76, 76, 76);    ")
        self.gridLayout = QtWidgets.QGridLayout(MainWindow)
        self.gridLayout.setContentsMargins(0, 0, 0, 0)
        self.gridLayout.setObjectName("gridLayout")
        self.h_frame = QtWidgets.QFrame(MainWindow)
        self.h_frame.setAutoFillBackground(False)
        self.h_frame.setObjectName("h_frame")
        self.horizontalLayout = QtWidgets.QHBoxLayout(self.h_frame)
        self.horizontalLayout.setContentsMargins(0, 0, 0, 0)
        self.horizontalLayout.setSpacing(0)
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.frame = QtWidgets.QFrame(self.h_frame)
        self.frame.setStyleSheet("background-color: rgb(208, 208, 208);\n"
"background-color: rgb(158, 158, 158);\n"
"background-color: rgb(226, 226, 226);\n"
"background-color: rgb(98, 98, 98);\n"
"background-color: rgb(76, 76, 76);")
        self.frame.setObjectName("frame")
        self.verticalLayout_2 = QtWidgets.QVBoxLayout(self.frame)
        self.verticalLayout_2.setContentsMargins(34, 12, 34, 12)
        self.verticalLayout_2.setSpacing(15)
        self.verticalLayout_2.setObjectName("verticalLayout_2")
        spacerItem = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.verticalLayout_2.addItem(spacerItem)
        self.AddGroupButton = QtWidgets.QPushButton(self.frame)
        self.AddGroupButton.setStyleSheet("QPushButton {\n"
"    background-color: rgb(81, 175, 81);\n"
"    padding: 3px;\n"
"    border: 4px solid rgb(81, 175, 81);\n"
"    border-radius: 1px;\n"
"    color: rgb(255, 255, 255);\n"
"font: 63 10pt \"Segoe UI Semibold\";\n"
"}\n"
"\n"
"\n"
"\n"
"QPushButton:Pressed{\n"
"    background-color: rgb(55, 140, 55);\n"
"    border-color: rgb(55, 140, 55);\n"
"}")
        self.AddGroupButton.setAutoDefault(True)
        self.AddGroupButton.setFlat(False)
        self.AddGroupButton.setObjectName("AddGroupButton")
        self.verticalLayout_2.addWidget(self.AddGroupButton)
        self.EditGroupButton = QtWidgets.QPushButton(self.frame)
        self.EditGroupButton.setStyleSheet("QPushButton {\n"
"    background-color: rgb(81, 175, 81);\n"
"    padding: 3px;\n"
"    border: 4px solid rgb(81, 175, 81);\n"
"    border-radius: 1px;\n"
"    color: rgb(255, 255, 255);\n"
"font: 63 10pt \"Segoe UI Semibold\";\n"
"}\n"
"\n"
"\n"
"\n"
"QPushButton:Pressed{\n"
"    background-color: rgb(55, 140, 55);\n"
"    border-color: rgb(55, 140, 55);\n"
"}")
        self.EditGroupButton.setObjectName("EditGroupButton")
        self.verticalLayout_2.addWidget(self.EditGroupButton)
        self.RemoveGroupButton = QtWidgets.QPushButton(self.frame)
        self.RemoveGroupButton.setStyleSheet("QPushButton {\n"
"    background-color: rgb(81, 175, 81);\n"
"    padding: 3px;\n"
"    border: 4px solid rgb(81, 175, 81);\n"
"    border-radius: 1px;\n"
"    color: rgb(255, 255, 255);\n"
"font: 63 10pt \"Segoe UI Semibold\";\n"
"}\n"
"\n"
"\n"
"\n"
"QPushButton:Pressed{\n"
"    background-color: rgb(55, 140, 55);\n"
"    border-color: rgb(55, 140, 55);\n"
"}")
        self.RemoveGroupButton.setObjectName("RemoveGroupButton")
        self.verticalLayout_2.addWidget(self.RemoveGroupButton)
        spacerItem1 = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.verticalLayout_2.addItem(spacerItem1)
        self.horizontalLayout.addWidget(self.frame)
        self.frame_2 = QtWidgets.QFrame(self.h_frame)
        self.frame_2.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.frame_2.setFrameShadow(QtWidgets.QFrame.Raised)
        self.frame_2.setObjectName("frame_2")
        self.gridLayout_2 = QtWidgets.QGridLayout(self.frame_2)
        self.gridLayout_2.setContentsMargins(20, 20, 20, 20)
        self.gridLayout_2.setObjectName("gridLayout_2")
        self.UserGroupList = QtWidgets.QListWidget(self.frame_2)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.UserGroupList.sizePolicy().hasHeightForWidth())
        self.UserGroupList.setSizePolicy(sizePolicy)
        self.UserGroupList.setStyleSheet("background-color: rgb(255, 255, 255);\n"
"background-color: rgb(225, 225, 225);")
        self.UserGroupList.setObjectName("UserGroupList")
        self.gridLayout_2.addWidget(self.UserGroupList, 0, 0, 1, 1)
        self.horizontalLayout.addWidget(self.frame_2)
        self.horizontalLayout.setStretch(0, 3)
        self.horizontalLayout.setStretch(1, 5)
        self.gridLayout.addWidget(self.h_frame, 0, 0, 1, 1)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "MainWindow"))
        self.AddGroupButton.setText(_translate("MainWindow", "+ Make Group"))
        self.EditGroupButton.setText(_translate("MainWindow", "O Edit Group"))
        self.RemoveGroupButton.setText(_translate("MainWindow", "x Remove Group"))
