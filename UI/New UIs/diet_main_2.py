# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'diet_main_2.ui'
#
# Created by: PyQt5 UI code generator 5.14.1
#
# WARNING! All changes made in this file will be lost!


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_Dialog(object):
    def setupUi(self, Dialog):
        Dialog.setObjectName("Dialog")
        Dialog.resize(431, 466)
        Dialog.setMaximumSize(QtCore.QSize(800, 550))
        Dialog.setStyleSheet("background-color: rgb(255, 255, 255);")
        self.gridLayout = QtWidgets.QGridLayout(Dialog)
        self.gridLayout.setContentsMargins(0, 0, 0, 0)
        self.gridLayout.setObjectName("gridLayout")
        self.frame_2 = QtWidgets.QFrame(Dialog)
        self.frame_2.setFrameShape(QtWidgets.QFrame.NoFrame)
        self.frame_2.setFrameShadow(QtWidgets.QFrame.Raised)
        self.frame_2.setLineWidth(1)
        self.frame_2.setObjectName("frame_2")
        self.verticalLayout = QtWidgets.QVBoxLayout(self.frame_2)
        self.verticalLayout.setContentsMargins(0, 20, 0, 0)
        self.verticalLayout.setSpacing(6)
        self.verticalLayout.setObjectName("verticalLayout")
        self.frame_3 = QtWidgets.QFrame(self.frame_2)
        self.frame_3.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.frame_3.setFrameShadow(QtWidgets.QFrame.Raised)
        self.frame_3.setObjectName("frame_3")
        self.gridLayout_3 = QtWidgets.QGridLayout(self.frame_3)
        self.gridLayout_3.setContentsMargins(15, -1, 15, -1)
        self.gridLayout_3.setObjectName("gridLayout_3")
        self.dateEdit = QtWidgets.QDateEdit(self.frame_3)
        self.dateEdit.setMinimumSize(QtCore.QSize(0, 20))
        self.dateEdit.setMaximumSize(QtCore.QSize(16777215, 50))
        self.dateEdit.setStyleSheet("QDateEdit{\n"
"   background-color: rgb(225, 225, 225);\n"
"\n"
"    \n"
"    font: 63 8pt \"Segoe UI Semibold\";\n"
"\n"
"}\n"
"\n"
"QDateEdit:up-button{\n"
"    background-color: rgb(81, 175, 81);\n"
"    padding: 1px;\n"
"    border: 4px solid rgb(81, 175, 81);\n"
"    color: rgb(255, 255, 255);\n"
"}\n"
"\n"
"QDateEdit:up-button:Pressed{\n"
"    background-color: rgb(55, 140, 55);\n"
"    border-color: rgb(55, 140, 55);\n"
"}\n"
"\n"
"QDateEdit:down-button{\n"
"    background-color: rgb(81, 175, 81);\n"
"    padding: 1px;\n"
"    border: 4px solid rgb(81, 175, 81);\n"
"    color: rgb(255, 255, 255);\n"
"}\n"
"\n"
"QDateEdit:down-button:Pressed{\n"
"    background-color: rgb(55, 140, 55);\n"
"    border-color: rgb(55, 140, 55);\n"
"}\n"
"\n"
"\n"
"\n"
"QDateEdit:up-arrow {\n"
"width: 0; \n"
"  height: 0; \n"
"  border-left: 4px solid rgb(81, 175, 81);\n"
"  border-right: 4px solid rgb(81, 175, 81);\n"
"  border-bottom: 4px solid  rgb(255, 255, 255);\n"
"}\n"
"\n"
"QDateEdit:down-arrow {\n"
"width: 0; \n"
"  height: 0; \n"
"  border-left: 4px solid rgb(81, 175, 81);\n"
"  border-right: 4px solid rgb(81, 175, 81);\n"
"  border-top: 4px solid  rgb(255, 255, 255);\n"
"}\n"
"")
        self.dateEdit.setFrame(False)
        self.dateEdit.setObjectName("dateEdit")
        self.gridLayout_3.addWidget(self.dateEdit, 0, 1, 1, 1)
        self.dateLabel = QtWidgets.QLabel(self.frame_3)
        self.dateLabel.setMinimumSize(QtCore.QSize(0, 20))
        self.dateLabel.setMaximumSize(QtCore.QSize(16777215, 50))
        self.dateLabel.setStyleSheet("font: 63 10pt \"Segoe UI Semibold\";\n"
"")
        self.dateLabel.setObjectName("dateLabel")
        self.gridLayout_3.addWidget(self.dateLabel, 0, 0, 1, 1)
        self.verticalLayout.addWidget(self.frame_3)
        self.scrollArea = QtWidgets.QScrollArea(self.frame_2)
        self.scrollArea.setFrameShape(QtWidgets.QFrame.NoFrame)
        self.scrollArea.setWidgetResizable(True)
        self.scrollArea.setObjectName("scrollArea")
        self.scrollAreaWidgetContents = QtWidgets.QWidget()
        self.scrollAreaWidgetContents.setGeometry(QtCore.QRect(0, 0, 916, 287))
        self.scrollAreaWidgetContents.setObjectName("scrollAreaWidgetContents")
        self.horizontalLayout = QtWidgets.QHBoxLayout(self.scrollAreaWidgetContents)
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.frame = QtWidgets.QFrame(self.scrollAreaWidgetContents)
        self.frame.setMinimumSize(QtCore.QSize(0, 0))
        self.frame.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.frame.setFrameShadow(QtWidgets.QFrame.Raised)
        self.frame.setObjectName("frame")
        self.gridLayout_4 = QtWidgets.QGridLayout(self.frame)
        self.gridLayout_4.setObjectName("gridLayout_4")
        self.breakfastLabel = QtWidgets.QLabel(self.frame)
        self.breakfastLabel.setMaximumSize(QtCore.QSize(16777215, 15))
        self.breakfastLabel.setStyleSheet("font: 63 10pt \"Segoe UI Semibold\";\n"
"")
        self.breakfastLabel.setObjectName("breakfastLabel")
        self.gridLayout_4.addWidget(self.breakfastLabel, 0, 0, 1, 1)
        self.breakfastList = QtWidgets.QListWidget(self.frame)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.breakfastList.sizePolicy().hasHeightForWidth())
        self.breakfastList.setSizePolicy(sizePolicy)
        self.breakfastList.setMinimumSize(QtCore.QSize(200, 200))
        self.breakfastList.setStyleSheet("background-color: rgb(255, 255, 255);\n"
"background-color: rgb(225, 225, 225);")
        self.breakfastList.setObjectName("breakfastList")
        self.gridLayout_4.addWidget(self.breakfastList, 1, 0, 1, 1)
        self.horizontalLayout.addWidget(self.frame)
        self.frame_4 = QtWidgets.QFrame(self.scrollAreaWidgetContents)
        self.frame_4.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.frame_4.setFrameShadow(QtWidgets.QFrame.Raised)
        self.frame_4.setObjectName("frame_4")
        self.gridLayout_8 = QtWidgets.QGridLayout(self.frame_4)
        self.gridLayout_8.setObjectName("gridLayout_8")
        self.lunchLabel = QtWidgets.QLabel(self.frame_4)
        self.lunchLabel.setMaximumSize(QtCore.QSize(16777215, 15))
        self.lunchLabel.setStyleSheet("font: 63 10pt \"Segoe UI Semibold\";\n"
"")
        self.lunchLabel.setObjectName("lunchLabel")
        self.gridLayout_8.addWidget(self.lunchLabel, 0, 0, 1, 1)
        self.lunchList = QtWidgets.QListWidget(self.frame_4)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.lunchList.sizePolicy().hasHeightForWidth())
        self.lunchList.setSizePolicy(sizePolicy)
        self.lunchList.setMinimumSize(QtCore.QSize(200, 200))
        self.lunchList.setStyleSheet("background-color: rgb(255, 255, 255);\n"
"background-color: rgb(225, 225, 225);")
        self.lunchList.setObjectName("lunchList")
        self.gridLayout_8.addWidget(self.lunchList, 1, 0, 1, 1)
        self.horizontalLayout.addWidget(self.frame_4)
        self.frame_5 = QtWidgets.QFrame(self.scrollAreaWidgetContents)
        self.frame_5.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.frame_5.setFrameShadow(QtWidgets.QFrame.Raised)
        self.frame_5.setObjectName("frame_5")
        self.gridLayout_5 = QtWidgets.QGridLayout(self.frame_5)
        self.gridLayout_5.setObjectName("gridLayout_5")
        self.dinnerList = QtWidgets.QListWidget(self.frame_5)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.dinnerList.sizePolicy().hasHeightForWidth())
        self.dinnerList.setSizePolicy(sizePolicy)
        self.dinnerList.setMinimumSize(QtCore.QSize(200, 200))
        self.dinnerList.setStyleSheet("background-color: rgb(255, 255, 255);\n"
"background-color: rgb(225, 225, 225);")
        self.dinnerList.setObjectName("dinnerList")
        self.gridLayout_5.addWidget(self.dinnerList, 1, 0, 1, 1)
        self.dinnerLabel = QtWidgets.QLabel(self.frame_5)
        self.dinnerLabel.setMaximumSize(QtCore.QSize(16777215, 15))
        self.dinnerLabel.setStyleSheet("font: 63 10pt \"Segoe UI Semibold\";\n"
"")
        self.dinnerLabel.setObjectName("dinnerLabel")
        self.gridLayout_5.addWidget(self.dinnerLabel, 0, 0, 1, 1)
        self.horizontalLayout.addWidget(self.frame_5)
        self.frame_6 = QtWidgets.QFrame(self.scrollAreaWidgetContents)
        self.frame_6.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.frame_6.setFrameShadow(QtWidgets.QFrame.Raised)
        self.frame_6.setObjectName("frame_6")
        self.gridLayout_6 = QtWidgets.QGridLayout(self.frame_6)
        self.gridLayout_6.setObjectName("gridLayout_6")
        self.snackLabel = QtWidgets.QLabel(self.frame_6)
        self.snackLabel.setMaximumSize(QtCore.QSize(16777215, 15))
        self.snackLabel.setStyleSheet("font: 63 10pt \"Segoe UI Semibold\";")
        self.snackLabel.setObjectName("snackLabel")
        self.gridLayout_6.addWidget(self.snackLabel, 0, 0, 1, 1)
        self.snackList = QtWidgets.QListWidget(self.frame_6)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.snackList.sizePolicy().hasHeightForWidth())
        self.snackList.setSizePolicy(sizePolicy)
        self.snackList.setMinimumSize(QtCore.QSize(200, 200))
        self.snackList.setStyleSheet("background-color: rgb(255, 255, 255);\n"
"background-color: rgb(225, 225, 225);")
        self.snackList.setObjectName("snackList")
        self.gridLayout_6.addWidget(self.snackList, 1, 0, 1, 1)
        self.horizontalLayout.addWidget(self.frame_6)
        self.scrollArea.setWidget(self.scrollAreaWidgetContents)
        self.verticalLayout.addWidget(self.scrollArea)
        self.frame_8 = QtWidgets.QFrame(self.frame_2)
        self.frame_8.setMinimumSize(QtCore.QSize(0, 90))
        self.frame_8.setStyleSheet("background-color: rgb(76, 76, 76);")
        self.frame_8.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.frame_8.setFrameShadow(QtWidgets.QFrame.Raised)
        self.frame_8.setObjectName("frame_8")
        self.gridLayout_7 = QtWidgets.QGridLayout(self.frame_8)
        self.gridLayout_7.setContentsMargins(10, -1, 10, -1)
        self.gridLayout_7.setHorizontalSpacing(20)
        self.gridLayout_7.setObjectName("gridLayout_7")
        self.editItem = QtWidgets.QPushButton(self.frame_8)
        self.editItem.setStyleSheet("QPushButton {\n"
"    background-color: rgb(81, 175, 81);\n"
"    padding: 3px;\n"
"    border: 4px solid rgb(81, 175, 81);\n"
"    border-radius: 1px;\n"
"    color: rgb(255, 255, 255);\n"
"font: 63 10pt \"Segoe UI Semibold\";\n"
"}\n"
"\n"
"\n"
"\n"
"QPushButton:Pressed{\n"
"    background-color: rgb(55, 140, 55);\n"
"    border-color: rgb(55, 140, 55);\n"
"}")
        self.editItem.setObjectName("editItem")
        self.gridLayout_7.addWidget(self.editItem, 2, 1, 1, 1)
        self.removeItem = QtWidgets.QPushButton(self.frame_8)
        self.removeItem.setStyleSheet("QPushButton {\n"
"    background-color: rgb(81, 175, 81);\n"
"    padding: 3px;\n"
"    border: 4px solid rgb(81, 175, 81);\n"
"    border-radius: 1px;\n"
"    color: rgb(255, 255, 255);\n"
"font: 63 10pt \"Segoe UI Semibold\";\n"
"}\n"
"\n"
"\n"
"\n"
"QPushButton:Pressed{\n"
"    background-color: rgb(55, 140, 55);\n"
"    border-color: rgb(55, 140, 55);\n"
"}")
        self.removeItem.setObjectName("removeItem")
        self.gridLayout_7.addWidget(self.removeItem, 2, 2, 1, 1)
        self.addItem = QtWidgets.QPushButton(self.frame_8)
        self.addItem.setStyleSheet("QPushButton {\n"
"    background-color: rgb(81, 175, 81);\n"
"    padding: 3px;\n"
"    border: 4px solid rgb(81, 175, 81);\n"
"    border-radius: 1px;\n"
"    color: rgb(255, 255, 255);\n"
"font: 63 10pt \"Segoe UI Semibold\";\n"
"}\n"
"\n"
"\n"
"\n"
"QPushButton:Pressed{\n"
"    background-color: rgb(55, 140, 55);\n"
"    border-color: rgb(55, 140, 55);\n"
"}")
        self.addItem.setObjectName("addItem")
        self.gridLayout_7.addWidget(self.addItem, 2, 0, 1, 1)
        self.verticalLayout.addWidget(self.frame_8)
        self.gridLayout.addWidget(self.frame_2, 0, 0, 1, 1)

        self.retranslateUi(Dialog)
        QtCore.QMetaObject.connectSlotsByName(Dialog)

    def retranslateUi(self, Dialog):
        _translate = QtCore.QCoreApplication.translate
        Dialog.setWindowTitle(_translate("Dialog", "Dialog"))
        self.dateLabel.setText(_translate("Dialog", "Date:"))
        self.breakfastLabel.setText(_translate("Dialog", "Breakfast"))
        self.lunchLabel.setText(_translate("Dialog", "Lunch"))
        self.dinnerLabel.setText(_translate("Dialog", "Dinner"))
        self.snackLabel.setText(_translate("Dialog", "Snack"))
        self.editItem.setText(_translate("Dialog", "O Edit Item"))
        self.removeItem.setText(_translate("Dialog", "x Remove Item"))
        self.addItem.setText(_translate("Dialog", "+ Add Item"))
