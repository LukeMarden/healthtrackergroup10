# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'ui/customDoneGoalWidget.ui'
#
# Created by: PyQt5 UI code generator 5.14.1
#
# WARNING! All changes made in this file will be lost!


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_Form(object):
    def setupUi(self, Form):
        Form.setObjectName("Form")
        Form.resize(400, 300)
        self.frame = QtWidgets.QFrame(Form)
        self.frame.setGeometry(QtCore.QRect(20, 30, 311, 181))
        self.frame.setMinimumSize(QtCore.QSize(211, 121))
        self.frame.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.frame.setFrameShadow(QtWidgets.QFrame.Raised)
        self.frame.setObjectName("frame")
        self.verticalLayout = QtWidgets.QVBoxLayout(self.frame)
        self.verticalLayout.setObjectName("verticalLayout")
        self.GoalNameText = QtWidgets.QLabel(self.frame)
        font = QtGui.QFont()
        font.setPointSize(11)
        self.GoalNameText.setFont(font)
        self.GoalNameText.setAlignment(QtCore.Qt.AlignCenter)
        self.GoalNameText.setObjectName("GoalNameText")
        self.verticalLayout.addWidget(self.GoalNameText)
        self.GoalTypeText = QtWidgets.QLabel(self.frame)
        self.GoalTypeText.setObjectName("GoalTypeText")
        self.verticalLayout.addWidget(self.GoalTypeText)
        self.SetDateText = QtWidgets.QLabel(self.frame)
        self.SetDateText.setObjectName("SetDateText")
        self.verticalLayout.addWidget(self.SetDateText)
        self.EndDateText = QtWidgets.QLabel(self.frame)
        self.EndDateText.setObjectName("EndDateText")
        self.verticalLayout.addWidget(self.EndDateText)
        self.StartValueText = QtWidgets.QLabel(self.frame)
        self.StartValueText.setObjectName("StartValueText")
        self.verticalLayout.addWidget(self.StartValueText)
        self.GoalValueText = QtWidgets.QLabel(self.frame)
        self.GoalValueText.setObjectName("GoalValueText")
        self.verticalLayout.addWidget(self.GoalValueText)

        self.retranslateUi(Form)
        QtCore.QMetaObject.connectSlotsByName(Form)

    def retranslateUi(self, Form):
        _translate = QtCore.QCoreApplication.translate
        Form.setWindowTitle(_translate("Form", "Form"))
        self.GoalNameText.setText(_translate("Form", "Goal Name"))
        self.GoalTypeText.setText(_translate("Form", "Goal Type"))
        self.SetDateText.setText(_translate("Form", "Set Date"))
        self.EndDateText.setText(_translate("Form", "End Date"))
        self.StartValueText.setText(_translate("Form", "Start Value:"))
        self.GoalValueText.setText(_translate("Form", "Goal Value:"))
