# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'userInput.ui'
#
# Created by: PyQt5 UI code generator 5.9.2
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_MainWindow(object):
    def input(self):
        height  = self.insert_Height.text()
        
        weight = self.insert_Weight.text()
        
        gender = self.genderComboBox.currentText()
        
        if(not height or not weight ):
            print("Please enter the data required")
            _translate = QtCore.QCoreApplication.translate
            self.ForDisplayingMessage.setText(_translate("MainWindow", "Please leave no line blank "))
            return
        try:
            height1 = float(height)
        except Exception:
            print("Please only insert numbers for height")
            _translate = QtCore.QCoreApplication.translate
            self.ForDisplayingMessage.setText(_translate("MainWindow", "Please only insert numbers for height"))
            return 
        try:
            weight2 = float(weight)
        except Exception:
            print("Please only insert numbers for weight")
            _translate = QtCore.QCoreApplication.translate
            self.ForDisplayingMessage.setText(_translate("MainWindow", "Please only insert numbers for weight"))
            return 
        print("height is "+height + " ,weight is "+ weight+" gender is "+gender)
        _translate = QtCore.QCoreApplication.translate
        #self.ForDisplayingMessage.setText(_translate("MainWindow", "height is "+height + " ,weight is "+ weight+" gender is "+gender))
        bmi = weight2/(height1*height1)
        bmi2 = str(bmi)
        print("Your BMI is "+ bmi2)
        if(bmi<18.5):
            warning="underwieght"
        if(bmi>29):
            warning="overweight"
        self.ForDisplayingMessage.setText(_translate("MainWindow", "Your BMI is "+bmi2+" which is "+warning))
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(800, 600)
        font = QtGui.QFont()
        font.setPointSize(9)
        MainWindow.setFont(font)
        MainWindow.setMouseTracking(True)
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.heightHeader = QtWidgets.QLabel(self.centralwidget)
        self.heightHeader.setGeometry(QtCore.QRect(150, 160, 151, 21))
        font = QtGui.QFont()
        font.setPointSize(10)
        self.heightHeader.setFont(font)
        self.heightHeader.setObjectName("heightHeader")
        self.weightHeader = QtWidgets.QLabel(self.centralwidget)
        self.weightHeader.setGeometry(QtCore.QRect(150, 85, 151, 21))
        font = QtGui.QFont()
        font.setPointSize(10)
        self.weightHeader.setFont(font)
        self.weightHeader.setScaledContents(True)
        self.weightHeader.setObjectName("weightHeader")
        self.submitButton = QtWidgets.QPushButton(self.centralwidget)
        self.submitButton.setGeometry(QtCore.QRect(150, 340, 211, 91))
        font = QtGui.QFont()
        font.setPointSize(11)
        self.submitButton.setFont(font)
        self.submitButton.setMouseTracking(True)
        self.submitButton.setObjectName("submitButton")
        self.genderComboBox = QtWidgets.QComboBox(self.centralwidget)
        self.genderComboBox.setGeometry(QtCore.QRect(150, 280, 151, 22))
        self.genderComboBox.setObjectName("genderComboBox")
        self.genderComboBox.addItem("")
        self.genderComboBox.addItem("")
        self.genderComboBox.addItem("")
        self.genderHeader = QtWidgets.QLabel(self.centralwidget)
        self.genderHeader.setGeometry(QtCore.QRect(150, 240, 151, 21))
        font = QtGui.QFont()
        font.setPointSize(10)
        self.genderHeader.setFont(font)
        self.genderHeader.setObjectName("genderHeader")
        self.mainWindowHeader = QtWidgets.QLabel(self.centralwidget)
        self.mainWindowHeader.setGeometry(QtCore.QRect(160, 10, 501, 61))
        font = QtGui.QFont()
        font.setPointSize(18)
        self.mainWindowHeader.setFont(font)
        self.mainWindowHeader.setTextFormat(QtCore.Qt.PlainText)
        self.mainWindowHeader.setScaledContents(True)
        self.mainWindowHeader.setAlignment(QtCore.Qt.AlignCenter)
        self.mainWindowHeader.setWordWrap(False)
        self.mainWindowHeader.setObjectName("mainWindowHeader")
        self.insert_Weight = QtWidgets.QLineEdit(self.centralwidget)
        self.insert_Weight.setGeometry(QtCore.QRect(150, 120, 113, 22))
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.insert_Weight.sizePolicy().hasHeightForWidth())
        self.insert_Weight.setSizePolicy(sizePolicy)
        self.insert_Weight.setInputMethodHints(QtCore.Qt.ImhDigitsOnly|QtCore.Qt.ImhNoAutoUppercase|QtCore.Qt.ImhNoPredictiveText|QtCore.Qt.ImhPreferNumbers)
        self.insert_Weight.setObjectName("insert_Weight")
        self.insert_Height = QtWidgets.QLineEdit(self.centralwidget)
        self.insert_Height.setGeometry(QtCore.QRect(150, 200, 113, 22))
        self.insert_Height.setInputMethodHints(QtCore.Qt.ImhDigitsOnly|QtCore.Qt.ImhNoAutoUppercase|QtCore.Qt.ImhNoPredictiveText|QtCore.Qt.ImhPreferNumbers)
        self.insert_Height.setObjectName("insert_Height")
        self.ForDisplayingMessage = QtWidgets.QLabel(self.centralwidget)
        self.ForDisplayingMessage.setGeometry(QtCore.QRect(380, 140, 391, 101))
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.ForDisplayingMessage.sizePolicy().hasHeightForWidth())
        self.ForDisplayingMessage.setSizePolicy(sizePolicy)
        self.ForDisplayingMessage.setObjectName("ForDisplayingMessage")
        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QtWidgets.QMenuBar(MainWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 800, 26))
        self.menubar.setObjectName("menubar")
        MainWindow.setMenuBar(self.menubar)
        self.statusbar = QtWidgets.QStatusBar(MainWindow)
        self.statusbar.setObjectName("statusbar")
        MainWindow.setStatusBar(self.statusbar)
        self.submitButton.clicked.connect(self.input)
        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "MainWindow"))
        self.heightHeader.setText(_translate("MainWindow", "Insert Height(m)"))
        self.weightHeader.setText(_translate("MainWindow", "Insert Weight in Kg"))
        self.submitButton.setText(_translate("MainWindow", "Submit Data "))
        self.genderComboBox.setItemText(0, _translate("MainWindow", "Male "))
        self.genderComboBox.setItemText(1, _translate("MainWindow", "Female"))
        self.genderComboBox.setItemText(2, _translate("MainWindow", "Rather not say "))
        self.genderHeader.setText(_translate("MainWindow", "What is your gender"))
        self.mainWindowHeader.setText(_translate("MainWindow", "Please Enter Your Information"))
        


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    MainWindow = QtWidgets.QMainWindow()
    ui = Ui_MainWindow()
    ui.setupUi(MainWindow)
    MainWindow.show()
    sys.exit(app.exec_())

