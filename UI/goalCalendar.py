# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'goal_calendar.ui'
#
# Created by: PyQt5 UI code generator 5.14.1
#
# WARNING! All changes made in this file will be lost!


from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtCore import QDate


class SetDateDialog(object):


    def __init__(self):
        self.selectedDate = QDate()

    def setupUi(self, Dialog):
        Dialog.setObjectName("Dialog")
        Dialog.resize(350, 250)
        Dialog.setMaximumSize(QtCore.QSize(350, 250))
        Dialog.setStyleSheet("background-color: rgb(76, 76, 76);")
        self.gridLayout = QtWidgets.QGridLayout(Dialog)
        self.gridLayout.setObjectName("gridLayout")
        self.frame = QtWidgets.QFrame(Dialog)
        self.frame.setObjectName("frame")
        self.verticalLayout = QtWidgets.QVBoxLayout(self.frame)
        self.verticalLayout.setSpacing(20)
        self.verticalLayout.setObjectName("verticalLayout")
        self.CalendarSelect = QtWidgets.QCalendarWidget(self.frame)
        self.CalendarSelect.setStyleSheet("background-color: rgb(225, 225, 225);\n"
                                          "color: rgb(0, 0, 0);\n"
                                          "font: 63 8pt \"Segoe UI Semibold\";\n"
                                          "\n"
                                          "")
        self.CalendarSelect.setGridVisible(True)
        self.CalendarSelect.setObjectName("CalendarSelect")
        self.verticalLayout.addWidget(self.CalendarSelect)
        self.buttonBox = QtWidgets.QDialogButtonBox(self.frame)
        self.buttonBox.setStyleSheet("QPushButton {\n"
                                     "    background-color: rgb(81, 175, 81);\n"
                                     "    padding: 2px;\n"
                                     "    border: 4px solid rgb(81, 175, 81);\n"
                                     "    border-radius: 1px;\n"
                                     "    color: rgb(255, 255, 255);\n"
                                     "    padding: 0 8px 0 8px;\n"
                                     "    min-width: 50px;\n"
                                     "font: 63 8pt \"Segoe UI Semibold\";\n"
                                     "}\n"
                                     "\n"
                                     "\n"
                                     "\n"
                                     "QPushButton:Pressed{\n"
                                     "    background-color: rgb(55, 140, 55);\n"
                                     "    border-color: rgb(55, 140, 55);\n"
                                     "}")
        self.buttonBox.setStandardButtons(QtWidgets.QDialogButtonBox.Cancel | QtWidgets.QDialogButtonBox.Ok)
        self.buttonBox.setCenterButtons(True)
        self.buttonBox.setObjectName("buttonBox")
        self.verticalLayout.addWidget(self.buttonBox)
        self.gridLayout.addWidget(self.frame, 0, 0, 1, 1)

        self.retranslateUi(Dialog)
        QtCore.QMetaObject.connectSlotsByName(Dialog)

        #UI init
        self.buttonBox.accepted.connect(Dialog.accept)
        self.buttonBox.rejected.connect(Dialog.reject)
        self.buttonBox.accepted.connect(self.onDateSelected)



    def retranslateUi(self, Dialog):
        _translate = QtCore.QCoreApplication.translate
        Dialog.setWindowTitle(_translate("Dialog", "Dialog"))

    def setSelectedDateToUI(self, setDate):
        self.CalendarSelect.setSelectedDate(QDate.fromString(setDate, "dd-MM-yyyy"))
        self.onDateSelected()

    def getSelectedDate(self):
        return self.selectedDate

    def onDateSelected(self):
        self.selectedDate = self.CalendarSelect.selectedDate()