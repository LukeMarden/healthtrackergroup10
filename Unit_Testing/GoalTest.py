
class GoalTest(object):
    def goalAddTest(self, goalController):
        from Model.Goal import Goal
        from Model.GoalType import GoalType
        goalController.onAddGoal(Goal(-1, "Test Name", "02-03-2020", GoalType.WEIGHT, "06-03-2020", 6, 3, 2))

    def goalRemoveTest(self, goalController, index):
        goalController.onRemoveGoal(index)