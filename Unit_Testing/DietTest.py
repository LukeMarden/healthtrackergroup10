
class DietTest(object):
    def dietAddTest(self, dietHistoryGUI):
        from Model.DietHistory import DietHistory
        dietHistoryGUI.controller.onAddItem(DietHistory(0, "dietName", "01-05-2020", "Breakfast", "Food",  1, 2, 3, 4, 5, 6, 7))

    def dietRemoveTest(self, dietHistoryGUI, id):
        dietHistoryGUI.controller.onRemoveItem(id)
        dietHistoryGUI.controller.breakfastList.pop(dietHistoryGUI.breakfastList.currentRow())
        dietHistoryGUI.updateList()