from PyQt5 import QtWidgets
from PyQt5.QtWidgets import QMessageBox
import psycopg2, psycopg2.extras
from View.AddUserGUI import AddUserGUI
class AddUserController(QtWidgets.QDialog, AddUserGUI):
    def __init__(self, groupID, userID, parent=None):
        self.DBusername = "postgres"
        self.DBpassword = "123456"
        self.host = "localhost"
        self.database = "HealthTracker"
        connection = psycopg2.connect(user=self.DBusername, password=self.DBpassword, host=self.host, database=self.database)
        manager = connection.cursor()
        self.groupID = groupID

        QtWidgets.QDialog.__init__(self, parent=parent)
        self.setupUi(self, groupID, userID)
        manager.close()
        connection.close()

    def getUserID(self):
        return self.id

    def addUser(self, email):
        connection = psycopg2.connect(user=self.DBusername, password=self.DBpassword, host=self.host, database=self.database)
        manager = connection.cursor()
        query = manager.execute("SELECT id FROM users, groupassignments WHERE email = %s "
                                     "AND users.id = groupassignments.userid AND groupassignments.groupid = %s", [str(email), self.groupID])
        account = manager.fetchone()
        query = manager.execute("SELECT email FROM users WHERE email = %s", [str(email)])
        emailVerification = manager.fetchone()
        if (emailVerification is None):
            QMessageBox.warning(self, "Warning", "User doesn't exist.")
            return
        elif (account is None):
            query = manager.execute("SELECT id FROM users WHERE email = %s", [str(email)])
            userid = manager.fetchone()[0]
            query = manager.execute("INSERT INTO groupassignments(groupid, userid) VALUES (%s, %s) ",
                                         [self.groupID, userid])
        else:
            QMessageBox.warning(self, "Warning", "User is already in the group.")
        connection.commit()
        manager.close()
        connection.close()