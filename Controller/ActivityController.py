from PyQt5 import QtCore, QtGui, QtWidgets
from datetime import datetime
import psycopg2

from View.ActivityHistoryGUI import activityHistoryGUI
from View.ActivityInputs import activityInputs
from View.AddNewActivity import newActivity


class ActivityController(QtWidgets.QDialog, activityHistoryGUI):


    def __init__(self, ID, launchFrom, parent=None):
        # test userID
        self.userID = ID
        # test database credentials
        self.dbUserName = "postgres"
        self.dbPassword = "123456"
        self.dbHost = "localhost"
        self.dbName = "HealthTracker"
        self.dbport = "5433"
        self.ActivityHistory(launchFrom)

    # set ui to the page that displays activity history
    def ActivityHistory(self, launchFrom):
        self.UI = activityHistoryGUI()
        dialog = QtWidgets.QDialog()
        self.UI.setupUi(dialog, self, launchFrom)
        dialog.exec_()

    # sets ui to page that takes input to enter a new activity
    def addActivityLogWindow(self):
        activities = self.getActivities()
        self.UI = activityInputs()
        dialog = QtWidgets.QDialog()
        self.UI.setupUi(dialog, self, activities)
        dialog.exec_()

    # sets ui to page for adding new activity types to the database
    def addNewActivityWindow(self):
        self.UI = newActivity()
        dialog = QtWidgets.QDialog()
        self.UI.setupUi(dialog, self)
        dialog.exec_()

    # method for when backing out of activity history page
    # closes app for now
    def closeActivities(self):
        #app.quit()
        self.UI.reject()
        pass

    # method to get the activities available in the database so they can be picked when in the add activity page
    def getActivities(self):
        # connection = psycopg2.connect(user=self.dbUserName, password=self.dbPassword, host=self.dbHost, port=self.dbport,
        #                               database=self.dbName)

        connection = psycopg2.connect(user=self.dbUserName, password=self.dbPassword, host=self.dbHost, database=self.dbName)

        cur = connection.cursor()
        cur.execute("SELECT activityid, activityname FROM activities")
        activities = cur.fetchall()
        connection.close()
        return activities

    # method to add the data taken from the ui to the data base
    def addActivityLog(self, activityID, activityLength):
        # connection = psycopg2.connect(user=self.dbUserName, password=self.dbPassword, host=self.dbHost, port=self.dbport,
        #                               database=self.dbName)

        connection = psycopg2.connect(user=self.dbUserName, password=self.dbPassword, host=self.dbHost,
                                      database=self.dbName)
        cur = connection.cursor()
        cur.execute("INSERT INTO userActivities (userid, activityid, activitylength, datedone, timedone)" +
                    "VALUES( {0}, {1}, {2}, CURRENT_DATE, LOCALTIME(0))".format(self.userID, activityID, activityLength))
        connection.commit()
        connection.close()

    # method to get the users activity data to display in the table
    def getActivityData(self):
        # connection = psycopg2.connect(user=self.dbUserName, password=self.dbPassword, host=self.dbHost, port=self.dbport,
        #                               database=self.dbName)

        connection = psycopg2.connect(user=self.dbUserName, password=self.dbPassword, host=self.dbHost,
                                      database=self.dbName)
        cur = connection.cursor()
        cur.execute("SELECT activityName, activitylength, kcalsburned, datedone, timedone FROM userview \
                    WHERE userid = %s", [self.userID])
        userActivities = cur.fetchall()
        connection.close()
        return userActivities

    # method to get the column names of the activities table
    def getActivityDataNames(self):
        # connection = psycopg2.connect(user=self.dbUserName, password=self.dbPassword, host=self.dbHost, port=self.dbport,
        #                               database=self.dbName)

        connection = psycopg2.connect(user=self.dbUserName, password=self.dbPassword, host=self.dbHost,
                                      database=self.dbName)
        cur = connection.cursor()
        cur.execute("SELECT activityName, activitylength, kcalsburned, datedone, timedone FROM userview")
        columnNames = [desc[0] for desc in cur.description]
        connection.close()
        return columnNames

    # method to get data points for the graph
    def getActivityDailyCalsBurned(self):
        # connection = psycopg2.connect(user=self.dbUserName, password=self.dbPassword, host=self.dbHost, port=self.dbport,
        #                               database=self.dbName)

        connection = psycopg2.connect(user=self.dbUserName, password=self.dbPassword, host=self.dbHost, database=self.dbName)
        cur = connection.cursor()
        cur.execute("SELECT SUM(kcalsburned) FROM userview WHERE userid = %s GROUP BY datedone", [self.userID])
        data = cur.fetchall()
        connection.close()
        calsBurned = [0]
        for x in range(len(data)):
            calsBurned.append(data[x][0])
        return calsBurned

    # method used when adding new activity type to the database to check if the new activity is reasonable and
    # not already in the database
    def activityCheckInvalid(self, activityName):
        temp = activityName
        temp = temp.replace(" ", "")
        if not temp.isalpha() or len(activityName) <= 4 or len(activityName) >= 15:
            return True
        # connection = psycopg2.connect(user=self.dbUserName, password=self.dbPassword, host=self.dbHost, port=self.dbport,
        #                               database=self.dbName)

        connection = psycopg2.connect(user=self.dbUserName, password=self.dbPassword, host=self.dbHost,
                                      database=self.dbName)
        cur = connection.cursor()
        cur.execute("SELECT activityname FROM activities WHERE activityname = %s", [activityName])
        results = cur.fetchall()
        connection.close()
        for result in results:
            if result[0] == activityName:
                return True
        return False

    # method to add new activity type to the database
    def addNewActivity(self, activityName, activityIntensity):
        # connection = psycopg2.connect(user=self.dbUserName, password=self.dbPassword, host=self.dbHost, port=self.dbport,
        #                               database=self.dbName)

        connection = psycopg2.connect(user=self.dbUserName, password=self.dbPassword, host=self.dbHost,
                                      database=self.dbName)
        cur = connection.cursor()
        cur.execute("INSERT INTO activities (activityname, kcalspermin) VALUES (%s, %s)",
                    [activityName, activityIntensity])
        connection.commit()
        connection.close()
        self.addActivityLogWindow()





if __name__ == "__main__":
    import sys

    app = QtWidgets.QApplication(sys.argv)
    auth = ActivityController(1)
    auth.exec_()
