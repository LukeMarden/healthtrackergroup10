import psycopg2
import psycopg2.extras
from PyQt5 import QtWidgets
from View.SignupExtendedInfoGUI import SignupExtendedInfoGUI
class SignupExtendedInfoController(QtWidgets.QDialog, SignupExtendedInfoGUI):
    def __init__(self, ID, parent=None):
        QtWidgets.QDialog.__init__(self, parent=parent)
        self.setupUi(self, ID)
        self.DBusername = "postgres"
        self.DBpassword = "123456"
        self.host = "localhost"
        self.database = "HealthTracker"
    #     load all values using that id from the database

    def addData(self, weight, height, dob, gender, ID):
        connection = psycopg2.connect(user=self.DBusername, password=self.DBpassword, host=self.host, database=self.database)
        manager = connection.cursor()
        self.query = manager.execute("UPDATE users SET dob = TO_DATE(%s, 'dd/mm/yyyy'), weight = %s, height = %s , gender = %s "
                                     "WHERE id = %s",
                                     [dob, weight, height, gender, ID])
        connection.commit()
        manager.close()
        connection.close()
        from Controller.UserController import UserController
        self.hide()
        auth = UserController(ID)
        auth.exec_()