from PyQt5 import QtWidgets
from PyQt5.QtWidgets import QMessageBox
import psycopg2, psycopg2.extras
from View.EditGroupGUI import EditGroupGUI
from Model.StandardUser import StandardUser
class EditGroupController(QtWidgets.QDialog, EditGroupGUI):
    def __init__(self, groupID, userID, parent=None):
        self.DBusername = "postgres"
        self.DBpassword = "123456"
        self.host = "localhost"
        self.database = "HealthTracker"
        connection = psycopg2.connect(user=self.DBusername, password=self.DBpassword, host=self.host, database=self.database)
        manager = connection.cursor()
        self.userID = userID
        self.groupID = groupID
        self.users = []
        query = manager.execute("SELECT userid FROM groupassignments WHERE groupid = %s", [self.groupID])
        users = manager.fetchall()
        for user in users:
            self.users.append(StandardUser(user[0]))
        QtWidgets.QDialog.__init__(self, parent=parent)
        self.setupUi(self, self.groupID, self.userID)
        manager.close()
        connection.close()


    # def getUserID(self):
    #     return self.id

    def getGroupID(self):
        return self.groupID

    def getGroupName(self):
        connection = psycopg2.connect(user=self.DBusername, password=self.DBpassword, host=self.host, database=self.database)
        manager = connection.cursor()
        query = manager.execute("SELECT groupname FROM groups WHERE groupid = %s", [self.groupID])
        groupName = manager.fetchone()[0]
        manager.close()
        connection.close()
        return groupName

    def getUsers(self):
        return self.users

    def removeUser(self, ID):
        connection = psycopg2.connect(user=self.DBusername, password=self.DBpassword, host=self.host, database=self.database)
        manager = connection.cursor()
        query = manager.execute("SELECT adminid FROM groups WHERE groupid = %s", [self.groupID])
        adminID = manager.fetchone()[0]
        if (self.userID is ID and adminID is not ID):
            query = manager.execute("DELETE FROM groupassignments WHERE userid = %s", [ID])
        elif (self.userID is ID and adminID is ID):
            QMessageBox.warning(self, "Warning", "You cannot leave as you are the group admin.")
            return
        elif (adminID is self.userID):
            query = manager.execute("DELETE FROM groupassignments WHERE userid = %s", [ID])
        else:
            QMessageBox.warning(self, "Warning", "You cannot remove other users as you are not the admin")
            return
        connection.commit()
        manager.close()
        connection.close()



