from PyQt5 import QtWidgets
from PyQt5.QtWidgets import QMessageBox
import psycopg2
import psycopg2.extras
from View.SignupBasicInfoGUI import SignupBasicInfoGUI


class SignupBasicInfoController(QtWidgets.QDialog, SignupBasicInfoGUI):

    def __init__(self, parent=None):
        QtWidgets.QDialog.__init__(self, parent=parent)
        self.setupUi(self)
        self.DBusername = "postgres"
        self.DBpassword = "123456"
        self.host = "localhost"
        self.database = "HealthTracker"

    def addData(self):
        firstName = self.firstname.text()
        surname = self.surname.text()
        email = self.email.text()
        password = self.password_1.text()
        if (not firstName or not surname or not email or not password):
            QMessageBox.warning(self, "Warning", "Please leave no boxes empty.")
            return
        if (password != self.password_2.text()):
            QMessageBox.warning(self, "Warning", "Passwords don't match")
            return
        if ("@" not in email):
            QMessageBox.warning(self, "Warning", "Not a Valid Email")
            return
        connection = psycopg2.connect(user=self.DBusername, password=self.DBpassword, host=self.host, database=self.database)
        manager = connection.cursor()
        self.query = manager.execute("SELECT EXISTS (SELECT users.email FROM users WHERE users.email = %s)", [email])
        if (manager.fetchone()[0] is True):
            QMessageBox.warning(self, "Warning", "'" + email + "'" + " is already registered to an account")
            return
        self.query = manager.execute("INSERT INTO users(firstname, surname, email, password) VALUES (%s, %s, %s, %s)",
                                     [firstName, surname, email, password])
        connection.commit()
        manager.close()
        connection.close()
        self.hide()
