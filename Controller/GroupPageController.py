from PyQt5 import QtWidgets
import psycopg2, psycopg2.extras
from View.GroupPageGUI import GroupPageGUI
from Model.StandardUser import StandardUser
class GroupPageController(QtWidgets.QDialog, GroupPageGUI):

    def __init__(self, groupid, parent=None):
        self.DBusername = "postgres"
        self.DBpassword = "123456"
        self.host = "localhost"
        self.database = "HealthTracker"
        connection = psycopg2.connect(user=self.DBusername, password=self.DBpassword, host=self.host, database=self.database)
        manager = connection.cursor()
        self.id = groupid
        self.users = []
        query = manager.execute("SELECT userid FROM groupassignments WHERE groupid = %s", [self.id])
        users = manager.fetchall()
        for user in users:
            self.users.append(StandardUser(user[0]))

        QtWidgets.QDialog.__init__(self, parent=parent)
        self.setupUi(self)
        manager.close()
        connection.close()

    def getUsers(self):
        return self.users

    def getGroupName(self):
        connection = psycopg2.connect(user=self.DBusername, password=self.DBpassword, host=self.host, database=self.database)
        manager = connection.cursor()
        query = manager.execute("SELECT groupname FROM groups WHERE groupid = %s", [self.id])
        groupName = manager.fetchone()[0]
        manager.close()
        connection.close()
        return groupName