from datetime import datetime, date

import psycopg2
from PyQt5 import QtWidgets
import sys

from PyQt5.QtWidgets import QMessageBox

from Model.Goal import Goal
from Model.GoalType import GoalType
from View.GoalsGUI import GoalMainUI


class GoalsController(object):
    'Controller class for Goal UI and Goal Logic'

    def __init__(self, userID, selfRunning, launchFrom):

        self.userID = userID
        self.goalsList = []
        self.doneGoalsList = []
        self.goalMainWindow = GoalMainUI()
        self.goalMainWindow.onGoalAddedEvent += self.onAddGoal
        self.goalMainWindow.onGoalEditedEvent += self.onEditGoal
        self.goalMainWindow.onGoalRemovedEvent += self.onRemoveGoal
        self.goalMainWindow.onGoalDetailRequestEvent += self.returnGoalDetail
        self.goalMainWindow.onGoalsDoneRequestEvent += self.returnDoneGoals
        self.dbUserName = "postgres"
        self.dbPassword = "123456"
        self.dbHost = "localhost"
        self.dbName = "HealthTracker"

        if selfRunning:
            app = QtWidgets.QApplication(sys.argv)

        dialog = QtWidgets.QDialog()
        self.goalMainWindow.setupUi(dialog, launchFrom)

        if selfRunning:
            dialog.show()
        self.initialiseGoalListUI()

        if not selfRunning:
            dialog.exec_()
        else:
            sys.exit(app.exec_())


    def initialiseGoalListUI(self):
        # COnnect to database
        connection = psycopg2.connect(user=self.dbUserName, password=self.dbPassword, host=self.dbHost,
                                      database=self.dbName)
        cur = connection.cursor()
        cur.execute('SELECT * FROM goals WHERE userID = %s', [self.userID])
        goalsRawData = cur.fetchall()
        for row in goalsRawData:
            addGoal = Goal.createEmptyGoal()
            addGoal.goalID = row[0]
            addGoal.setDate = row[2].strftime('%d-%m-%Y')
            addGoal.goalName = row[3]
            addGoal.endDate = row[4].strftime('%d-%m-%Y')
            addGoal.startValue = float(row[5])
            addGoal.currentValue = float(row[6])
            addGoal.goalValue = float(row[7])
            addGoal.goalType = GoalType(row[8])
            if addGoal.getProgress() == 100 or datetime.strptime(addGoal.endDate, '%d-%m-%Y').date() < date.today():
                self.doneGoalsList.append(addGoal)
                self.goalMainWindow.updateGoalsAchievedCount(len(self.doneGoalsList))
            else:
                self.goalsList.append(addGoal)
                self.goalMainWindow.addItemToUIList(addGoal)

        self.goalMainWindow.updateGoalsAchievedCount(len(self.doneGoalsList))
        #connection.commit()
        cur.close()
        connection.close()


    def onAddGoal(self, goal):
        #Validation
        if datetime.strptime(goal.endDate, '%d-%m-%Y').date() < date.today():
            PrintMsg("Invalid end date")
            return

        if goal.goalName == "":
            PrintMsg("Name cannot be empty")
            return

        if goal.goalValue == goal.currentValue:
            PrintMsg("Goal value and current value cannot be the same")
            return

        #Goal adding
        goal.setDate = date.today().strftime("%d-%m-%Y")
        goal.startValue = goal.currentValue

        # Connect to database
        connection = psycopg2.connect(user=self.dbUserName, password=self.dbPassword, host=self.dbHost, database=self.dbName)
        cur = connection.cursor()
        cur.execute("INSERT INTO goals(userID, setDate, goalName, endDate, StartValue, CurrentValue, goalValue, "
                    "goalType) "
                    "VALUES (%s, TO_DATE(%s, 'dd-mm-yyyy'), %s, TO_DATE(%s, 'dd-mm-yyyy'), %s, %s, %s, %s);",
            [self.userID, goal.setDate, goal.goalName, goal.endDate, goal.startValue, goal.currentValue, goal.goalValue, goal.goalType.value])
        connection.commit()

        cur.execute("SELECT max(id) FROM goals")
        rawID = cur.fetchone()
        goal.goalID = int(rawID[0])
        self.goalsList.append(goal)
        self.goalMainWindow.addItemToUIList(goal)
        connection.close()


    def returnGoalDetail(self, index):
        self.goalMainWindow.invokeGoalDetailWindow(self.goalsList[index], False)

    def returnDoneGoals(self):
        self.goalMainWindow.showGoalsDoneWindow(self.doneGoalsList)

    def onEditGoal(self, index, goal):
        if goal.getProgress() == 100 or datetime.strptime(goal.endDate, '%d-%m-%Y').date() < date.today():
            self.goalsList.pop(index)
            self.goalMainWindow.removeItemFromUIList(index)
            self.doneGoalsList.append(goal)
            self.goalMainWindow.updateGoalsAchievedCount(len(self.doneGoalsList))
            goal.endDate = date.today().strftime("%d-%m-%Y")
        else:
            self.goalsList[index] = goal
            self.goalMainWindow.changeItemFromUIList(index, goal)

        # Connect to database
        connection = psycopg2.connect(user=self.dbUserName, password=self.dbPassword, host=self.dbHost,
                                      database=self.dbName)
        cur = connection.cursor()
        cur.execute("UPDATE goals SET endDate = TO_DATE(%s, 'dd-mm-yyyy'), goalValue = %s, goalType = %s, CurrentValue = %s, goalName = %s WHERE id = %s;",
                    [goal.endDate, goal.goalValue, goal.goalType.value, goal.currentValue, goal.goalName, goal.goalID])
        connection.commit()
        cur.close()
        connection.close()


    def onRemoveGoal(self, index):
        removeGoal = self.goalsList[index]
        del self.goalsList[index]
        self.goalMainWindow.removeItemFromUIList(index)

        # COnnect to database
        connection = psycopg2.connect(user=self.dbUserName, password=self.dbPassword, host=self.dbHost,
                                      database=self.dbName)
        cur = connection.cursor()
        cur.execute('DELETE FROM goals WHERE id = %s',
                    [removeGoal.goalID])
        connection.commit()
        cur.close()
        connection.close()

def PrintMsg(msgText):
    msg = QMessageBox()
    msg.setIcon(QMessageBox.Information)
    msg.setText("Message: " + msgText)
    msg.setStandardButtons(QMessageBox.Ok)
    msg.exec_()


if __name__ == "__main__":
    GoalsController("1", True)