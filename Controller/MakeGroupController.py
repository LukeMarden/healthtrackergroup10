from PyQt5 import QtWidgets
import psycopg2, psycopg2.extras
from View.MakeGroupGUI import MakeGroupGUI
class MakeGroupController(QtWidgets.QDialog, MakeGroupGUI):
    def __init__(self, ID, parent=None):
        self.id = ID
        self.DBusername = "postgres"
        self.DBpassword = "123456"
        self.host = "localhost"
        self.database = "HealthTracker"

        QtWidgets.QDialog.__init__(self, parent=parent)
        self.setupUi(self)

    def getUserID(self):
        return self.id

    def makeGroup(self, groupName):

        connection = psycopg2.connect(user=self.DBusername, password=self.DBpassword, host=self.host, database=self.database)
        manager = connection.cursor()
        query = manager.execute("INSERT INTO groups(groupname, adminid) VALUES (%s, %s) "
                                     "RETURNING groupid", [str(groupName), self.id])
        groupid = int(manager.fetchone()[0])
        query = manager.execute("INSERT INTO groupassignments(groupid, userid) VALUES (%s, %s)",
                                     [groupid, self.id])
        connection.commit()
        manager.close()
        connection.close()
