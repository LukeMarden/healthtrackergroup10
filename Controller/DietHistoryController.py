import psycopg2
import psycopg2.extras
from PyQt5 import QtWidgets
from PyQt5.QtWidgets import QMessageBox

from Model.DietHistory import DietHistory
from View.DietHistoryGUI import DietHistoryGUI


class DietHistoryController(object):
    """Controller class for DietHistory"""

    def __init__(self, userID, launchFrom):
        self.userID = userID
        self.breakfastList = []
        self.lunchList = []
        self.dinnerList = []
        self.snackList = []
        self.dietMainWindow = DietHistoryGUI(self)
        self.userName = "postgres"
        self.password = "123456"
        self.host = "localhost"
        self.database = "HealthTracker"

        window = QtWidgets.QDialog()
        self.dietMainWindow.setupUi(window, launchFrom)

        self.initializeItemList()

        # window.show()
        window.exec_()

    def initializeItemList(self):
        self.breakfastList = []
        self.lunchList = []
        self.dinnerList = []
        self.snackList = []

        # connects to database
        connection = psycopg2.connect(user=self.userName, password=self.password, host=self.host,
                                      database=self.database)
        cur = connection.cursor()
        print("UI date: " + self.dietMainWindow.dateEdit.date().toString("dd/MM/yyyy"))
        cur.execute("SELECT * FROM diets WHERE userID = %s AND dietdate = TO_DATE(%s, 'dd/mm/yyyy')",
                    [self.userID, self.dietMainWindow.dateEdit.date().toString("dd/MM/yyyy")])
        dietsRawData = cur.fetchall()
        for row in dietsRawData:
            addDiet = DietHistory.createEmptyDiet()
            addDiet.dietID = row[1]
            addDiet.dietName = row[2]
            addDiet.dietDate = row[3].strftime('%m-%d-%Y')
            addDiet.dietTime = row[4]
            addDiet.dietType = row[5]
            addDiet.dietWeight = row[6]
            addDiet.dietVolume = row[7]
            addDiet.dietPercentage = row[8]
            addDiet.dietCalories = row[9]
            addDiet.dietFat = row[10]
            addDiet.dietCarbs = row[11]
            addDiet.dietPro = row[12]
            print("diet: " + addDiet.toString())

            if addDiet.dietTime == "Breakfast":
                self.breakfastList.append(addDiet)
            elif addDiet.dietTime == "Lunch":
                self.lunchList.append(addDiet)
            elif addDiet.dietTime == "Dinner":
                self.dinnerList.append(addDiet)
            else:
                self.snackList.append(addDiet)

        cur.close()
        connection.close()
        self.dietMainWindow.updateList()

    def onAddItem(self, diet):

        if diet.dietName == "":
            PrintMsg("Name cannot be empty")
            return

        # Connect to database

        connection = psycopg2.connect(user=self.userName, password=self.password, host=self.host,
                                      database=self.database)
        cur = connection.cursor()

        cur.execute(
            "INSERT INTO diets(userid, dietname, dietdate, diettime, diettype, dietweight, dietvolume, dietpercentage, dietcalories, dietfat, dietcarbs, dietpro) VALUES (%s, %s, TO_DATE(%s, 'mm/dd/yyyy'), %s, %s, %s, %s, %s, %s, %s, %s, %s)",
            [self.userID, diet.dietName, diet.dietDate, diet.dietTime, diet.dietType, diet.dietWeight,
             diet.dietVolume, diet.dietPercentage, diet.dietCalories, diet.dietFat, diet.dietCarbs, diet.dietPro])

        cur.close()
        connection.commit()
        connection.close()
        self.initializeItemList()
        self.dietMainWindow.updateList()
        print("added")

    def onEditItem(self, diet):

        connection = psycopg2.connect(user=self.userName, password=self.password, host=self.host,
                                      database=self.database)

        cur = connection.cursor()
        cur.execute("UPDATE diets SET dietdate = TO_DATE(%s, 'mm/dd/yyyy'), diettime = %s, diettype = %s, dietweight = "
                    "%s, dietvolume = %s, dietpercentage = %s, dietcalories = %s, dietfat = %s, dietcarbs = %s, "
                    "dietpro = %s "
                    "WHERE dietID = %s;",
                    [diet.dietDate, diet.dietTime, diet.dietType, diet.dietWeight,
                     diet.dietVolume, diet.dietPercentage, diet.dietCalories, diet.dietFat, diet.dietCarbs,
                     diet.dietPro, diet.dietID])

        connection.commit()
        cur.close()
        connection.close()
        self.initializeItemList()
        self.dietMainWindow.updateList()

    def onRemoveItem(self, dietID):
        connection = psycopg2.connect(user=self.userName, password=self.password, host=self.host,
                                      database=self.database)
        cur = connection.cursor()

        cur.execute("DELETE FROM diets WHERE dietID = %s",
                    [dietID])

        connection.commit()
        connection.close()


def PrintMsg(msgText):
    msg = QMessageBox()
    msg.setIcon(QMessageBox.Information)
    msg.setText("Message: " + msgText)
    msg.setStandardButtons(QMessageBox.Ok)
    msg.exec_()


if __name__ == "__main__":
    import sys

    app = QtWidgets.QApplication(sys.argv)
    auth = DietHistoryController(2)
