from PyQt5 import QtWidgets
from PyQt5.QtWidgets import QMessageBox
import psycopg2, psycopg2.extras
from View.UserGroupGUI import UserGroupGUI
from Model.UserGroupModel import UserGroupModel
class UserGroupController(QtWidgets.QDialog, UserGroupGUI):
    def __init__(self, ID, parent=None):
        # self.user = StandardUser(ID)
        self.DBusername = "postgres"
        self.DBpassword = "123456"
        self.host = "localhost"
        self.database = "HealthTracker"
        connection = psycopg2.connect(user=self.DBusername, password=self.DBpassword, host=self.host, database=self.database)
        manager = connection.cursor()
        self.id = ID
        self.groups = []
        query = manager.execute("SELECT groupid FROM groupassignments WHERE userid = %s", [self.id])
        groups = manager.fetchall()
        for group in groups:
            self.groups.append(UserGroupModel(group[0]))

        QtWidgets.QDialog.__init__(self, parent=parent)
        self.setupUi(self, ID)
        manager.close()
        connection.close()


    def getGroups(self):
        return self.groups

    def getUserID(self):
        return self.id

    def addGroup(self, groupname):
        connection = psycopg2.connect(user=self.DBusername, password=self.DBpassword, host=self.host, database=self.database)
        manager = connection.cursor()
        query = manager.execute("INSERT INTO groups(groupname, adminid) VALUES %s, %s", [groupname, self.id])
        groupid = manager.execute("SELECT groupid FROM groups WHERE groupname = %s" [groupname])
        query = manager.execute("INSERT INTO groupassignments(groupid, userid) VALUES %s, %s", [groupid, self.id])
        manager.close()
        connection.close()

    def removeGroup(self, groupid):
        connection = psycopg2.connect(user=self.DBusername, password=self.DBpassword, host=self.host, database=self.database)
        manager = connection.cursor()
        query = manager.execute("SELECT adminid FROM groups WHERE adminid = %s AND groupid = %s",
                                     [self.id, groupid])
        array_row = manager.fetchone()
        if (array_row == None):
            QMessageBox.warning(self, "Warning", "You are not the group admin.")
            return
        else:
            query = manager.execute("DELETE FROM groupassignments WHERE groupid = %s", [groupid])
            query = manager.execute("DELETE FROM groups WHERE groupid = %s", [groupid])
            connection.commit()
        manager.close()
        connection.close()

    def openGroup(self, groupid):
        connection = psycopg2.connect(user=self.DBusername, password=self.DBpassword, host=self.host, database=self.database)
        manager = connection.cursor()
        query = manager.execute("SELECT userid FROM groupassignments WHERE groupid = %s", [groupid])
        group = manager.fetchone()
        manager.close()
        connection.close()
        return group

if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    auth = UserGroupController(4)
    auth.exec_()