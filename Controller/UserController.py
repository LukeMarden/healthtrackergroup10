from PyQt5 import QtWidgets
import psycopg2, psycopg2.extras
from View.UserGUI import UserGUI
from Model.StandardUser import StandardUser
class UserController(QtWidgets.QDialog, UserGUI):
    def __init__(self, ID, parent=None):
        self.user = StandardUser(ID)
        self.DBusername = "postgres"
        self.DBpassword = "123456"
        self.host = "localhost"
        self.database = "HealthTracker"
        connection = psycopg2.connect(user=self.DBusername, password=self.DBpassword, host=self.host, database=self.database)
        manager = connection.cursor()


        QtWidgets.QDialog.__init__(self, parent=parent)
        self.setupUi(self)

        manager.close()
        connection.close()

        self.updateGoalsGraph(self.user.id, self.DBusername, self.DBpassword, self.host, self.database)
        self.updateDietGraph(self.user.id, self.DBusername, self.DBpassword, self.host, self.database)
        self.updateActivityGraph(self.user.id, self.DBusername, self.DBpassword, self.host, self.database)


    def get_id(self):
        return self.user.id
    def get_fname(self):
        return self.user.fname

    def get_sname(self):
        return self.user.sname

    def get_email(self):
        return self.user.email

    def get_dob(self):
        return self.user.dob

    def get_weight(self):
        return self.user.weight

    def get_height(self):
        return self.user.height

    def get_gender(self):
        return self.user.gender

    def get_bmi(self):
        return self.user.generateBMI()






if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    auth = UserController(19)
    auth.exec_()