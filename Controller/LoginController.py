from PyQt5 import QtWidgets
from PyQt5.QtWidgets import QMessageBox
import psycopg2
import psycopg2.extras

from View.LoginGUI import LoginGUI
class LoginController(QtWidgets.QDialog, LoginGUI):

    def __init__(self, parent=None):
        QtWidgets.QDialog.__init__(self, parent=parent)
        self.setupUi(self)
        self.DBusername = "postgres"
        self.DBpassword = "123456"
        self.host = "localhost"
        self.database = "HealthTracker"

    def login(self):
        username = self.username.text()
        password = self.password.text()

        connection = psycopg2.connect(user=self.DBusername, password=self.DBpassword, host=self.host, database=self.database)
        manager = connection.cursor()
        self.query = manager.execute("SELECT email, password FROM users WHERE email = %s AND password = %s",
                                     [username, password])
        array_row = manager.fetchone()
        if (array_row == None):
            QMessageBox.warning(self, "Warning", "Login failed")
            return
        else:
            getUsername = array_row[0]
            getPassword = array_row[1]

        if getUsername == username and getPassword == password:
            self.query = manager.execute("SELECT id FROM users WHERE email = %s", [username])
            ID = manager.fetchone()[0]
            self.query = manager.execute("SELECT weight, height, dob FROM users WHERE id = %s", [ID])
            array_row = manager.fetchone()
            if (not array_row[0] or not array_row[1] or not array_row[2]):
                self.hide()
                self.launchExtendedSignUp(ID)
                return
            else:
                from Controller.UserController import UserController
                auth = UserController(ID)
                self.hide()
                auth.exec_()
                return

        #     this is where the users page will load
        else:
            QMessageBox.warning(self, "Warning", "Login failed")
        manager.close()
        connection.close()

if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    auth = LoginController()
    auth.exec_()
